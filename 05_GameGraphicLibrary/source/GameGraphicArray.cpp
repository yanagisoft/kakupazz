//=================================================================================================
//
// GameGraphicArray ソースファイル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "GameGraphicArray.h"


//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//@info
	//		SetSpritePostion ( VEC3 ) での設定で
	//		スプライトが順番によってアルファ値を完全に透明にしてしまうことの、
	//		原因追究と解決

	// ->
	//		DirectX の仕様
	//		透過情報(α値)のあるオブジェクトをレンダリングするときには、
	//		後(Z値の大きい)のオブジェクトをすべてレンダリングしなければならない


	//Z値で降順ソートされた位置に挿入
	void GameGraphicArray::InsertByZ ( shared_ptr < GameGraphicBase > pTask )
	{
		//一つも無いとき通常の追加
		if ( 0 == GetSize () ) { AddTask ( pTask ); return; }

		//Z値をチェックして指定位置に挿入
		float z = pTask->GetZ ();

		shared_ptr < vector < P_Task > > pvpVec = GetpvpTask ();

		int i = 0;
		for ( auto p : (*pvpVec) )
		{
			shared_ptr < GameGraphicBase > pg = dynamic_pointer_cast < GameGraphicBase > ( p );
			float gz = pg->GetZ ();

			//Z値が対象より大きいとき、その前に挿入して終了
			if ( z > gz )
			{
				InsertTask ( pTask, i );
				return;
			}
			++i;
		}

#if 0
		UINT max = GetMax ();
		for ( UINT i = 0; i < max; ++i )
		{
			GameTask* pt = GetTask ( i );
			GameGraphicBase * g = dynamic_cast < GameGraphicBase * > ( pt );
			float gz = g->GetZ ();

			//Z値が対象より大きいとき、その前に挿入して終了
			if ( z > gz )
			{
				InsertTask ( pTask, i );
				return;
			}
		}
#endif // 0

		//すべての値より小さい場合、末尾に追加
		AddTask ( pTask );
	}


}	//namespace GAME

