﻿//=================================================================================================
//
// キャラ ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "Chara.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//配置
	enum CHARA_ALIGN
	{
		CA_P1_X = 215, CA_P1_Y = 330,
		CA_P2_X = 300, CA_P2_Y = 330,
		CA_TIMER = 14,
		CA_TX_NUM = 4,

		//SINGLE
		CA_S_X = 100, CA_S_Y = 330, 
		CA_S_BACK_X = 130, CA_S_BACK_Y = 45, CA_S_BACK_W = 70, CA_S_BACK_H = 400, 
	};


	Chara::Chara ()
		: m_name ( CN_VERNA )
		, m_timer ( 0 ), m_index ( 0 )
	{
		//main
		m_grpMain = make_shared < GrpAcv > ();
		AddTask ( m_grpMain );
	}

	Chara::~Chara ()
	{
	}

	void Chara::ParamInit ()
	{
		//キャラクタによるイメージの選択
		switch ( m_name )
		{
		case CN_VERNA: 
			m_grpMain->AddTexture ( _T("dot_ver0.png") );
			m_grpMain->AddTexture ( _T("dot_ver1.png") );
			m_grpMain->AddTexture ( _T("dot_ver2.png") );
			m_grpMain->AddTexture ( _T("dot_ver3.png") );
		break;
		case CN_ESTA: 
			m_grpMain->AddTexture ( _T("dot_est0.png") );
			m_grpMain->AddTexture ( _T("dot_est1.png") );
			m_grpMain->AddTexture ( _T("dot_est2.png") );
			m_grpMain->AddTexture ( _T("dot_est3.png") );
		break;
		case CN_AUTUM: 
			m_grpMain->AddTexture ( _T("dot_aut0.png") );
			m_grpMain->AddTexture ( _T("dot_aut1.png") );
			m_grpMain->AddTexture ( _T("dot_aut2.png") );
			m_grpMain->AddTexture ( _T("dot_aut3.png") );
		break;
		case CN_HIEMS: 
			m_grpMain->AddTexture ( _T("dot_hie0.png") );
			m_grpMain->AddTexture ( _T("dot_hie1.png") );
			m_grpMain->AddTexture ( _T("dot_hie2.png") );
			m_grpMain->AddTexture ( _T("dot_hie3.png") );
		break;
		default: break;
		}
	}

	void Chara::Move()
	{
		//メイングラフィック アニメーション
		if ( ++ m_timer >= CA_TIMER ) 
		{
			m_timer = 0;
			if ( ++m_index >= CA_TX_NUM ) { m_index = 0; }
			m_grpMain->SetIndexTexture ( m_index );
		}

		TASK_VEC::Move ();
	}

	void Chara::SetPosSingle ()
	{
		m_grpMain->SetPos ( CA_S_X, CA_S_Y );
	}

	void Chara::SetPosDouble ()
	{
		//プレイヤ別の位置初期化
		if ( PLAYER1 == m_playerNum )
		{
			m_grpMain->SetPos ( CA_P1_X, CA_P1_Y );
		}
		else
		{
			m_grpMain->SetPos ( CA_P2_X, CA_P2_Y );
		}
	}


}	//namespace GAME

