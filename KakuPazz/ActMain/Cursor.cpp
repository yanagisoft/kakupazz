﻿//=================================================================================================
//
//		カーソル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Cursor.h"
#include "../GameMain/Sound.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//-------------------------------------------------------------------------------------------------
	// 定数宣言
	enum CURSOR_CONST_VALUE
	{
		CURSOR_LIMIT_TOP = 12,				//カーソルが移動できる上限
//		CURSOR_START_POS_X = 0,				//開始時カーソル位置x
		CURSOR_START_POS_X = 0,				//開始時カーソル位置x
//		CURSOR_START_POS_Y = 12 + 6,		//開始時カーソル位置y
		CURSOR_START_POS_Y = 12 + 2,		//開始時カーソル位置y
		CURSOR_SCALINGCENTER_X = 32,		//拡大中心X
		CURSOR_SCALINGCENTER_Y = 16,		//拡大中心Y
		INPUT_STAY_TIME = 10,				//押しっぱなし判定時間
		CURSOR_PALSE = 10,					//振動時間
	};

	const KEY_NAME Cursor::m_input[_PLAYER_NUM][INPUT_NUM] = 
	{
		{
			P1_UP, P1_DOWN, P1_LEFT, P1_RIGHT, 
			P1_BUTTON1, P1_BUTTON2,	P1_BUTTON3,	P1_BUTTON4,
		} ,
		{
			P2_UP, P2_DOWN, P2_LEFT, P2_RIGHT, 
			P2_BUTTON1, P2_BUTTON2,	P2_BUTTON3,	P2_BUTTON4,
		}
	};

	//-------------------------------------------------------------------------------------------------
	void CursorInput::Input ()
	{
		//入力チェック
		if ( IS_NET_KEY ( m_targetKey ) )
		{
			//タイマ初期値の場合
			if ( 0 == m_inputTimer )
			{
				DoMove ();		//１つ移動してから
				m_inputTimer = 1;	//押しっぱなし判定計測開始
			}
			//計測終了、その後維持
			else if ( INPUT_STAY_TIME == m_inputTimer )
			{
				DoMove ();		//押しっぱなし移動
			}
			//計測中の場合
			else
			{
				m_inputTimer++;	//待機時間は移動しない
			}
		}
		//入力が無い場合
		else
		{
			m_inputTimer = 0;	//押しっぱなし判定計測終了
		}
	}

	void CursorInput::DoMove ()
	{
		//可動範囲内なら
		if ( Condition () )
		{
//			SOUND->Play ( SE_CURSOR_MOVE );
			Do ();	//移動
		}
	}

	//-------------------------------------------------------------------------------------------------
	bool CursorInputUp::Condition () { return ( CURSOR_LIMIT_TOP < GetPt().y ); }
	bool CursorInputDown::Condition () { return ( STAGE_SIZE_Y - 2 > GetPt().y ); }
	bool CursorInputLeft::Condition () { return ( 0 < GetPt().x ); }
	bool CursorInputRight::Condition () { return ( STAGE_SIZE_X - 2  > GetPt().x ); }

	void CursorInputUp::Do () { --(GetpPt()->y); }
	void CursorInputDown::Do () { ++(GetpPt()->y); }
	void CursorInputLeft::Do () { --(GetpPt()->x); }
	void CursorInputRight::Do () { ++(GetpPt()->x); }

	//-------------------------------------------------------------------------------------------------
	Cursor::Cursor()
		: m_mode(SINGLE), m_playerNum ( PLAYER1 )
	{
		//拡大の中心
		SetScalingCenter ( VEC2 ( CURSOR_SCALINGCENTER_X, CURSOR_SCALINGCENTER_Y ) );

		//位置
		m_point = make_shared < POINT > ();
		m_point->x = CURSOR_START_POS_X;
		m_point->y = CURSOR_START_POS_Y;

		//テクスチャ
		AddTexture ( _T("cursor.png") );

		//カーソル入力補助に位置のポインタを設定
		m_inputUp.SetpPoint ( m_point );
		m_inputDown.SetpPoint ( m_point );
		m_inputLeft.SetpPoint ( m_point );
		m_inputRight.SetpPoint ( m_point );
	}

	Cursor::~Cursor()
	{
	}

	void Cursor::Init()
	{
		//ゲーム基準位置からグラフィック表示位置を設定
		float x = m_base.x + PANEL_SIZE_X * m_point->x;
		float y = m_base.y + PANEL_SIZE_Y * m_point->y;
		SetPos ( x, y ); 

		m_upRevise = 0;
		m_pulseTimer = 0;
		
		GrpAcv::Init();
	}

	//----------------------------------------------------------------------------
	//	動作
	//----------------------------------------------------------------------------
	void Cursor::Move()
	{
		//カーソル入力補助による押しっぱなし判定
		m_inputUp.Input ();
		m_inputDown.Input ();
		m_inputLeft.Input ();
		m_inputRight.Input ();

		//ゲーム座標からの表示位置計算
		VEC2 pos;
		pos.x = (float)(m_base.x + PANEL_SIZE_X * m_point->x);
		pos.y = (float)(m_base.y + PANEL_SIZE_Y * m_point->y);
		pos.y += m_upRevise;		//上昇位置補正
		SetPos( pos ); 

		//振動
		if ( ++ m_pulseTimer >= CURSOR_PALSE )
		{
			m_pulse = ! m_pulse;
			if ( m_pulse ) { SetScaling ( VEC2( 1.125f, 1.125f ) ); }
			else { SetScaling ( VEC2( 1, 1 ) ); }
			m_pulseTimer = 0;
		}

		GrpAcv::Move ();
	}

	bool Cursor::CallSwap ()
	{
		return PUSH_NET_KEY ( m_input[m_playerNum][BUTTON1] );
	}

	bool Cursor::CallUp ()
	{
		return IS_NET_KEY ( m_input[m_playerNum][BUTTON2] );
	}

	bool Cursor::CallDebugSpoit ()
	{
		return PUSH_NET_KEY ( m_input[m_playerNum][BUTTON3] );
	}

	bool Cursor::CallDebugPaste ()
	{
		return PUSH_NET_KEY ( m_input[m_playerNum][BUTTON4] );
	}

	void Cursor::Down ()
	{
		if ( m_point->y < STAGE_SIZE_Y ) { m_point->y++; }
	}

	void Cursor::Up ()
	{
		if ( m_point->y > CURSOR_LIMIT_TOP ) { m_point->y--; }
	}


}	//namespace GAME

