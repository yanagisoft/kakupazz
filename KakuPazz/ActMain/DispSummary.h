﻿//=================================================================================================
//
// DispSummary ヘッダ
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/GameCommon.h"
#include "../GameMain/SceneCommon.h"
#include "Chara.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	class DispSummary : public TASK_VEC
	{
		P_Param		m_param;		//ゲームパラメータ
		PLAYER_NUM	m_playerNum;

		P_GrpAcv	m_grpBack;
		P_GrpAcv	m_grpGaugeFrame;
		P_GrpAcv	m_grpGauge;
		P_GrpAcv	m_grpAttack;
		P_GrpAcv	m_star;
		P_GrpAcv	m_starNum;

		P_Chara		m_chara;

	public:
		DispSummary ();
		DispSummary ( const DispSummary & rhs ) = delete;	//コピー禁止
		~DispSummary ();

		void ParamInit ( P_Param p );
		void Move ();

		void SetPlayerNum ( PLAYER_NUM playerNum ) { m_playerNum = playerNum; }

	private:
		void _SetPosSingle ();
		void _SetPosDouble ();
	};

	using P_DispSummary = shared_ptr < DispSummary >;


}	//namespace GAME

