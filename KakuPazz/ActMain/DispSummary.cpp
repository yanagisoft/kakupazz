﻿//=================================================================================================
//
// DispSummary ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "DispSummary.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//配置
	enum CHARA_ALIGN
	{
		CA_BACK_P1_X = 245, CA_BACK_P1_Y = 45, CA_BACK_P1_W = 70, CA_BACK_P1_H = 400,
		CA_BACK_P2_X = 325, CA_BACK_P2_Y = 45, CA_BACK_P2_W = 70, CA_BACK_P2_H = 400,
		CA_GAUGE_FRAME_P1_X = 265, CA_GAUGE_FRAME_P1_Y = 100,
		CA_GAUGE_FRAME_P2_X = 345, CA_GAUGE_FRAME_P2_Y = 100,
		CA_GAUGE_P1_X = 265, CA_GAUGE_P1_Y = 100,
		CA_GAUGE_P2_X = 345, CA_GAUGE_P2_Y = 100,
		CA_STAR_P1_X = 265, CA_STAR_P1_Y = 260,
		CA_STAR_P2_X = 345, CA_STAR_P2_Y = 260,
		CA_STAR_NUM_P1_X = 265, CA_STAR_NUM_P1_Y = 290,
		CA_STAR_NUM_P2_X = 345, CA_STAR_NUM_P2_Y = 290,
		CA_ATTACK_P1_X = 265, CA_ATTACK_P1_Y = 50,
		CA_ATTACK_P2_X = 345, CA_ATTACK_P2_Y = 50,
	};


	DispSummary::DispSummary ()
	{
		//back
		m_grpBack = make_shared < GrpAcv > ();
		m_grpBack->AddTexture ( _T("chara_sys_back.png") );
		AddTask ( m_grpBack );

		//main
		m_chara = make_shared < Chara > ();
		AddTask ( m_chara );

		//gaugeFrame
		m_grpGaugeFrame = make_shared < GrpAcv > ();
		m_grpGaugeFrame->AddTexture ( _T("gaugeFrame.png") );
		AddTask ( m_grpGaugeFrame );

		//attack
		m_grpAttack = make_shared < GrpAcv > ();
		AddTask ( m_grpAttack );

		//gauge
		m_grpGauge = make_shared < GrpAcv > ();
		m_grpGauge->AddTexture ( _T("gauge.png") );
		AddTask ( m_grpGauge );

		//star
		m_star = make_shared < GrpAcv > ();
		m_star->AddTexture ( _T("star.png") );
		AddTask ( m_star );

		//starNum
		m_starNum = make_shared < GrpAcv > ();
		m_starNum->AddTexture ( _T("starnum.png") );
		AddTask ( m_starNum );
	}

	DispSummary::~DispSummary ()
	{
	}

	void DispSummary::ParamInit ( P_Param p )
	{
		m_param = p;

		 //キャラクタによるイメージの選択
		if ( PLAYER1 == m_playerNum )
		{
			m_chara->SetName ( m_param->GetCharaName1p () );
		}
		else
		{
			m_chara->SetName ( m_param->GetCharaName2p () );
		}
		m_chara->SetPlayerNum ( m_playerNum );
		m_chara->ParamInit ();

		//モードによる表示位置の初期化
		switch ( p->GetMode () )
		{
		case GAME::SINGLE: _SetPosSingle (); break;
		case GAME::DOUBLE_1P_CPU: _SetPosDouble (); break;
		case GAME::DOUBLE_1P_2P : _SetPosDouble (); break;
		case GAME::DOUBLE_CPU_CPU: _SetPosDouble (); break;
		case GAME::NETWORK_SERVER_1P: _SetPosDouble (); break;
		case GAME::NETWORK_SERVER_2P: _SetPosDouble (); break;
		case GAME::NETWORK_CLIENT_1P: _SetPosDouble (); break;
		case GAME::NETWORK_CLIENT_2P: _SetPosDouble (); break;
		default: break;
		}
	}

	void DispSummary::Move()
	{
		TASK_VEC::Move ();
	}

	void DispSummary::_SetPosSingle ()
	{
		m_chara->SetPosSingle ();
		m_grpBack->SetValid ( false );
		m_grpGaugeFrame->SetValid ( false );
		m_grpGauge->SetValid ( false );
		m_star->SetValid ( false );
		m_starNum->SetValid ( false );
		m_grpAttack->SetValid ( false );
	}

	void DispSummary::_SetPosDouble ()
	{
		//プレイヤ別の位置初期化
		if ( PLAYER1 == m_playerNum )
		{
			m_grpBack->SetPos ( CA_BACK_P1_X, CA_BACK_P1_Y );
			m_grpAttack->AddTexture ( _T("arrow_1p_g.png") );
			m_grpAttack->AddTexture ( _T("arrow_1p_c.png") );
			m_grpAttack->SetPos ( CA_ATTACK_P1_X, CA_ATTACK_P1_Y );
			m_chara->SetPosDouble ();
			m_grpGaugeFrame->SetPos ( CA_GAUGE_FRAME_P1_X, CA_GAUGE_FRAME_P1_Y );
			m_grpGauge->SetPos ( CA_GAUGE_P1_X, CA_GAUGE_P1_Y );
			m_star->SetPos ( CA_STAR_P1_X, CA_STAR_P1_Y );
			m_starNum->SetPos ( CA_STAR_NUM_P1_X, CA_STAR_NUM_P1_Y );
		}
		else
		{
			m_grpBack->SetPos ( CA_BACK_P2_X, CA_BACK_P2_Y );
			m_grpAttack->AddTexture ( _T("arrow_2p_g.png") );
			m_grpAttack->AddTexture ( _T("arrow_2p_c.png") );
			m_grpAttack->SetPos ( CA_ATTACK_P2_X, CA_ATTACK_P2_Y );
			m_chara->SetPosDouble ();
			m_grpGaugeFrame->SetPos ( CA_GAUGE_FRAME_P2_X, CA_GAUGE_FRAME_P2_Y );
			m_grpGauge->SetPos ( CA_GAUGE_P2_X, CA_GAUGE_P2_Y );
			m_star->SetPos ( CA_STAR_P2_X, CA_STAR_P2_Y );
			m_starNum->SetPos ( CA_STAR_NUM_P2_X, CA_STAR_NUM_P2_Y );
		}
	}


}	//namespace GAME

