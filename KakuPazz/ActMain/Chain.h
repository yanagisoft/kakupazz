﻿//=================================================================================================
//
// 連鎖
//
//=================================================================================================


#ifndef		__CHAIN_HEADER_INCLUDE__
#define		__CHAIN_HEADER_INCLUDE__


//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "../GameMain/Game.h"

#include "PanelCommon.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class Chain	: public GameTaskArray
	{
		enum CHAIN_PARAM
		{
			CHAIN_PARAM_TIMER = 30,
		};

		//連鎖表示
		UINT		m_nChain;		//連鎖数
		GameGraphicFromArchive		m_rensa;		//"れんさ"表示
		GameGraphicFromArchive		m_chainNum;		//連鎖数表示
		GameMatrix			m_matrix;		//連鎖数表示のマトリックス

		UINT		m_timer;		//表示時間

	public:
		Chain ();
		Chain ( const Chain& rhs );
		~Chain ();

		void Init ();
		void Move ();

		void IncrimentChain () { m_nChain++; }
		void ResetChain ();
	};


}	//namespace GAME


#endif		//__CHAIN_HEADER_INCLUDE__

