﻿//=================================================================================================
//
// ゲーム進行表示		
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "Panel/PanelCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	
	class DispProcedure : public TASK_VEC
	{
		P_Game_Proc		m_procedure;	//ゲーム進行

		DWORD		m_timer;	//タイマー
		DWORD		m_index;	//テクスチャインデックス
		float		m_scale;	//拡大率

		P_GrpAcv	m_countDown;		//数字のカウントダウン
		P_GrpAcv	m_pGameOver;	//ゲームオーバー表示
		P_GrpAcv	m_grp1pWin;		//1p勝
		P_GrpAcv	m_grp2pWin;		//2p勝
		VEC2		m_vel;			//速度

		shared_ptr < Fade >		m_fade;		//フェード


	public:
		DispProcedure( P_Game_Proc procedure );
		DispProcedure( const DispProcedure& rhs ) = delete;
		~DispProcedure();

		void Init();
		void Move();

		void Start ();
		void Count ();
		void Play ();
		void Over ();
		void Win1p ();
		void Win2p ();

		void SetProcedure ( P_Game_Proc p ) { m_procedure = p; }
	};

	using P_DispProcedure = shared_ptr < DispProcedure >;


}	//namespace GAME

