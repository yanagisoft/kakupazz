﻿//=================================================================================================
//
// 連鎖表示
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "DispChain.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	enum DISP_PROCEDURE_PARAM
	{
		DC_DIGIT_X = 430,
		DC_DIGIT_Y = 50,
		DC_RENSA_X = 490,
		DC_RENSA_Y = 50,
		DC_TIME = 60,
	};

	DispChain::DispChain ()
		: m_timer ( 0 ), m_bActive ( false )
	{
		//数字
		m_digit = make_shared < GrpAcv > ();
		m_digit->AddTexture ( _T("chain0.png") );
		m_digit->AddTexture ( _T("chain1.png") );
		m_digit->AddTexture ( _T("chain2.png") );
		m_digit->AddTexture ( _T("chain3.png") );
		m_digit->AddTexture ( _T("chain4.png") );
		m_digit->AddTexture ( _T("chain5.png") );
		m_digit->AddTexture ( _T("chain6.png") );
		m_digit->AddTexture ( _T("chain7.png") );
		m_digit->AddTexture ( _T("chain8.png") );
		m_digit->AddTexture ( _T("chain9.png") );
		m_digit->SetPos ( DC_DIGIT_X , DC_DIGIT_Y );
		m_digit->SetValid ( false );
		AddTask ( m_digit );

		//れんさ表示
		m_rensa = make_shared < GrpAcv > ();
		m_rensa->AddTexture ( _T("rensa.png") );
		m_rensa->SetPos ( DC_RENSA_X , DC_RENSA_Y );
		m_rensa->SetValid ( false );
		AddTask ( m_rensa );
	}

	DispChain::~DispChain ()
	{
	}

	void DispChain::Move ()
	{
		//稼働中
		if ( 0 < m_timer )
		{
			TRACE_F ( _T("%d\n"), m_timer );
			//終了
			if ( 0 == -- m_timer )
			{
				m_digit->SetValid ( false );
				m_rensa->SetValid ( false );
				m_bActive = false;
			}
		}
		TASK_VEC::Move ();
	}

	void DispChain::On ( UINT chainNum )
	{
		if ( 0 == chainNum ) { return; }
		if ( chainNum < 10 - 1 )
		{
			m_digit->SetIndexTexture ( chainNum + 1 );
			m_digit->SetValid ( true );
			m_rensa->SetValid ( true );
		}
		m_bActive = true;
	}

	void DispChain::Off ()
	{
		if ( m_timer == 0 )
		{
			m_timer = DC_TIME;
		}
	}

}	//namespace GAME

