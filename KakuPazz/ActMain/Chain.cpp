﻿//=================================================================================================
//
// 連鎖
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
//#include "StdAfx.h"
#include "Chain.h"


//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	Chain::Chain ()
	{
//		SetMax ( 2 );
		m_nChain = 0;
		m_timer = 0;
	}

	Chain::~Chain()
	{
	}

	void Chain::Init()
	{
		//「れんさ」表示
		m_rensa.AddTexture ( TEXT("rensa.tga") );
		m_rensa.GetpMatrix()->SetPos ( 80, 300 );
		m_rensa.SetValid ( false );
		AddTask ( &m_rensa );

		//連鎖数表示
//		m_chainNum.SetMaxTexture ( 10 );
		m_chainNum.AddTexture ( TEXT("chain1.png") );
		m_chainNum.AddTexture ( TEXT("chain2.png") );
		m_chainNum.AddTexture ( TEXT("chain3.png") );
		m_chainNum.AddTexture ( TEXT("chain4.png") );
		m_chainNum.AddTexture ( TEXT("chain5.png") );
		m_chainNum.AddTexture ( TEXT("chain6.png") );
		m_chainNum.AddTexture ( TEXT("chain7.png") );
		m_chainNum.AddTexture ( TEXT("chain8.png") );
		m_chainNum.AddTexture ( TEXT("chain9.png") );
		m_chainNum.AddTexture ( TEXT("chain0.png") );

//		m_chainNum.SetMaxMatrix ( 1 );
		m_chainNum.AddMatrix ();
		m_chainNum.GetpMatrix()->SetPos ( 20, 300 );
		m_chainNum.GetpMatrix()->SetValid ( false );
//		m_chainNum.SetGameMatrix ( &m_matrix );

		AddTask ( &m_chainNum );


		GameTaskArray::Init();
	}

	void Chain::Move()
	{
		if ( KeyInput::instance()->PushKey ( P1_BUTTON4 ) )
		{
			IncrimentChain();
		}

		//タイマーで表示の消去
		if ( m_timer > 0 )
		{
			m_timer++;
			if ( m_timer > CHAIN_PARAM_TIMER )
			{
				m_timer = 0;
				m_nChain = 0;
				m_matrix.SetValid ( false );
				m_rensa.SetValid ( false );			
			}
		}

		if ( m_nChain > 9 ) m_nChain = 0;
		
		if ( m_nChain > 0 )
		{
			m_matrix.SetValid ( true );
			m_rensa.SetValid ( true );
		}

		m_chainNum.SetIndexTexture ( m_nChain );
//		m_matrix.SetTextureIndex ( m_nChain );
		GameTaskArray::Move();
	}

	void Chain::ResetChain ()
	{
		if ( m_nChain > 0 )
		{
			m_timer++;
		}
//		m_nChain = 0;
	}


}	//namespace GAME

