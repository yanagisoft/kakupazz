﻿//=================================================================================================
//
// 連鎖表示
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "Panel/PanelCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	
	class DispChain : public TASK_VEC
	{
		DWORD		m_timer;	//タイマー
		DWORD		m_index;	//テクスチャインデックス
		P_GrpAcv	m_digit;	//数字
		P_GrpAcv	m_rensa;	//れんさ表示
		bool		m_bActive;

	public:
		DispChain();
		DispChain( const DispChain & rhs ) = delete;
		~DispChain();

		void Move ();
		
		bool IsActive () const { return m_bActive; }

		void On ( UINT chainNum );
		void Off ();
	};

	using P_DispChain = shared_ptr < DispChain >;


}	//namespace GAME

