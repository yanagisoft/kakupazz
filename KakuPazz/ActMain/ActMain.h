﻿//=================================================================================================
//
// アクトメイン ヘッダ
//		ゲームにおける動作の主要部分
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/GameCommon.h"
#include "../GameMain/Scene.h"
#include "../Network/NetWaiting.h"
#include "../Network/NetworkSwitch.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	class ActMain : public Scene
	{
		UINT		m_frame;	//共通フレーム

		//StageとNetworkWaitingを切り替える
		shared_ptr < NetworkSwitch > m_networkSwitch;

	public:
		ActMain ();
		ActMain ( const ActMain & rhs ) = delete;	//コピー禁止
		~ActMain ();

		void ParamInit ();
		void Move ();

		//GameSceneManagerで扱うため、戻値はSceneではなくGameScene
		P_GameScene Transit ();
	};

}	//namespace GAME

