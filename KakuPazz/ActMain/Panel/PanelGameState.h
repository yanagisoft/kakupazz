//=================================================================================================
//
// PanelGameState ヘッダファイル
//	GameStateにの継承先によって振る舞いを変えるパターン
//
//	PanelGameState	状態の基本
//	PGS_Default		通常状態
//	PGS_Moving		移動状態
//	PGS_Drop		落下状態
//	PGS_Getdown		着地状態
//	PGS_EraseWait	消去待機状態
//	PGS_Erase		消去状態
//	PGS_GameStart	ゲームスタート状態
//	PGS_GameOver	ゲームオーバー状態
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "PanelCommon.h"
#include "PanelState.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class PanelObject;
	using SP_PnOb = shared_ptr < PanelObject >;
	using WP_PnObject = weak_ptr < PanelObject >;

	//-------------------------------------------
	//状態の基本
	class PanelGameState
	{
	protected:	//内部計算用
		UINT		m_blinkTimer;	//点滅時間
		VEC2		m_movingPos;	//現在移動量
		VEC2		m_distance;		//移動距離
		P_PnState	m_target;		//移動先の状態を一時保存(移動後に入替)
		UINT		m_moveFrame;	//移動時間
		UINT		m_moveTimer;	//移動タイマ
		UINT		m_getdownTimer;	//着地時間
		VEC2		m_scaling;		//拡大縮小

		//テクスチャ名テーブル
		const static TEXTURE_NAME	m_textureName[PANEL_STATE_NUM][PANEL_KIND_NUM];

		//親パネルオブジェクト操作用
		WP_PnObject	m_wpOb;

	public:
		PanelGameState () = delete;
		PanelGameState ( WP_PnObject p );
		PanelGameState ( const PanelGameState & rhs) = delete;
		virtual ~PanelGameState () {}

		void SetwpPnOb ( SP_PnOb p ) { m_wpOb = p; }

		virtual void Init () {}
		virtual void Act () {}
		virtual TEXTURE_NAME SelectTexture ( PANEL_KIND kind );
		virtual _CLR SelectClr ();
	
		//状態確認
		virtual bool IsLocked () const { return true; }
		virtual bool CanMove () const { return false; }
		virtual bool CanErase () const { return false; }
		virtual bool CanDrop () const { return false; }
		virtual bool CanBeDropped () const { return false; }

		//移動値取得
		VEC2 GetMoving () const { return m_movingPos; }

		//拡大縮小値取得
		VEC2 GetScaling () const { return m_scaling; }
	};

	using PGS = PanelGameState;
	using P_PGS = shared_ptr < PGS >;
	using WP_PGS = weak_ptr < PGS >;


	//-------------------------------------------
	//通常状態
	class PGS_Default : public PGS
	{
	public:
		PGS_Default () = delete;
		PGS_Default ( WP_PnObject p ) : PGS ( p ) {}
		~PGS_Default () {}

		void Init ();

		bool IsLocked () const { return false; }
		bool CanMove () const { return true; }
		bool CanDrop () const { return true; }
		bool CanBeDropped () const { return true; }
		bool CanErase () const { return true; }
	};
	using P_PGS_Default = shared_ptr < PGS_Default >;

	//-------------------------------------------
	//移動状態
	class PGS_Moving : public PGS
	{
	public:
		PGS_Moving () = delete;
		PGS_Moving ( WP_PnObject p ) : PGS ( p ) {}

		void Act ();
		void Set ( P_PnState pTarget, VEC2 distance, UINT frame );
	};
	using P_PGS_Moving = shared_ptr < PGS_Moving >;

	//-------------------------------------------
	//落下状態
	class PGS_Drop : public PGS
	{
	public:
		PGS_Drop () = delete;
		PGS_Drop ( WP_PnObject p ) : PGS ( p ) {}
		
		_CLR SelectClr ();
		void Act ();
		void Set ( P_PnState pTarget, VEC2 distance, UINT frame );
		void Swap ();
	};
	using P_PGS_Drop = shared_ptr < PGS_Drop >;

	//-------------------------------------------
	//着地状態
	class PGS_Getdown : public PGS
	{
	public:
		PGS_Getdown () = delete;
		PGS_Getdown ( WP_PnObject p ) : PGS ( p ) {}

		_CLR SelectClr ();
		void Init ();
		void Act ();
		bool CanErase () const { return true; }
	};
	using P_PGS_Getdown = shared_ptr < PGS_Getdown >;

	//-------------------------------------------
	//消去待機状態
	class PGS_EraseWait : public PGS
	{
	public:
		PGS_EraseWait () = delete;
		PGS_EraseWait ( WP_PnObject p ) : PGS ( p ) {}

		TEXTURE_NAME SelectTexture ( PANEL_KIND kind );
		void Act ();
	};
	using P_PGS_EraseWait = shared_ptr < PGS_EraseWait >;

	//-------------------------------------------
	//消去状態
	class PGS_Erase : public PGS
	{
	public:
		PGS_Erase () = delete;
		PGS_Erase ( WP_PnObject p ) : PGS ( p ) {}

		void Start ();
	};
	using P_PGS_Erase = shared_ptr < PGS_Erase >;

	//-------------------------------------------
	//おじゃま消去状態
	class PGS_EraseHamper : public PGS
	{
	public:
		PGS_EraseHamper () = delete;
		PGS_EraseHamper ( WP_PnObject p ) : PGS ( p ) {}

		void Start ();
	};
	using P_PGS_EraseHamper = shared_ptr < PGS_EraseHamper >;

	//-------------------------------------------
	//ゲームスタート状態
	class PGS_GameStart : public PGS
	{
	public:
		PGS_GameStart () = delete;
		PGS_GameStart ( WP_PnObject p ) : PGS ( p ) {}
	};
	using P_PGS_GameStart = shared_ptr < PGS_GameStart >;

	//-------------------------------------------
	//ゲームオーバー状態
	class PGS_GameOver : public PGS
	{
	public:
		PGS_GameOver () = delete;
		PGS_GameOver ( WP_PnObject p ) : PGS ( p ) {}

		TEXTURE_NAME SelectTexture ( PANEL_KIND kind );
	};
	using P_PGS_GameOver = shared_ptr < PGS_GameOver >;


}	//namespace GAME


