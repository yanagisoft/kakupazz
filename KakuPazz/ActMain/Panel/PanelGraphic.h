//=================================================================================================
//
// PanelGraphic ヘッダファイル
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
//#include "Const.h"
#include "Game.h"
#include "PanelArray.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class PanelGraphic : public GrpAcv
	{
	public:
		PanelGraphic () = delete;
		PanelGraphic ( P_PanelArray panelArray );
		PanelGraphic ( const PanelGraphic & rhs ) = delete;
		~PanelGraphic () = default;

		void Draw ();
		void Printf ();

	private:
		void SetPanelArray ( P_PanelArray panelArray );

	};

	using P_PnGraphic = shared_ptr < PanelGraphic >;


}	//namespace GAME


