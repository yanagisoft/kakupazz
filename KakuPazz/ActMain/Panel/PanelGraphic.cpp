//=================================================================================================
//
// PanelGraphic ソースファイル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelGraphic.h"


//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	PanelGraphic::PanelGraphic ( P_PanelArray panelArray )
	{
		SetPanelArray ( panelArray );

		//テクスチャの設定
		AddTexture ( _T( "panel1.png" ) );
		AddTexture ( _T( "panel2.png" ) );
		AddTexture ( _T( "panel3.png" ) );
		AddTexture ( _T( "panel4.png" ) );
		AddTexture ( _T( "panel5.png" ) );
		AddTexture ( _T( "panel6.png" ) );

		AddTexture ( _T( "panel1_light.png" ) );
		AddTexture ( _T( "panel2_light.png" ) );
		AddTexture ( _T( "panel3_light.png" ) );
		AddTexture ( _T( "panel4_light.png" ) );
		AddTexture ( _T( "panel5_light.png" ) );
		AddTexture ( _T( "panel6_light.png" ) );

		AddTexture ( _T( "panel1_face.png" ) );
		AddTexture ( _T( "panel2_face.png" ) );
		AddTexture ( _T( "panel3_face.png" ) );
		AddTexture ( _T( "panel4_face.png" ) );
		AddTexture ( _T( "panel5_face.png" ) );
		AddTexture ( _T( "panel6_face.png" ) );

		AddTexture ( _T( "blank.png" ) );

		AddTexture ( _T( "hamperBlockL.png" ) );		//おじゃま左
		AddTexture ( _T( "hamperBlockC.png" ) );		//おじゃま中
		AddTexture ( _T( "hamperBlockR.png" ) );		//おじゃま右

		AddTexture ( _T( "hamperBlockUL.png" ) );		//おじゃま上左
		AddTexture ( _T( "hamperBlockUC.png" ) );		//おじゃま上中
		AddTexture ( _T( "hamperBlockUR.png" ) );		//おじゃま上右
		AddTexture ( _T( "hamperBlockML.png" ) );		//おじゃま中左
		AddTexture ( _T( "hamperBlockMC.png" ) );		//おじゃま中中
		AddTexture ( _T( "hamperBlockMR.png" ) );		//おじゃま中右
		AddTexture ( _T( "hamperBlockBL.png" ) );		//おじゃま下左
		AddTexture ( _T( "hamperBlockBC.png" ) );		//おじゃま下中
		AddTexture ( _T( "hamperBlockBR.png" ) );		//おじゃま下右
	}

	void PanelGraphic::SetPanelArray ( P_PanelArray panelArray )
	{
		ClearObject ();
		VSP_PnOb & vp_pnOb = panelArray->GetrVP_PnObject ();
		for ( auto p : vp_pnOb )
		{
			AddpObject ( p );
		}
	}

	void PanelGraphic::Draw ()
	{
		GrpAcv::Draw ();
	}

	void PanelGraphic::Printf ()
	{
		DBGOUT_FL_F ( _T("PnGraphic\n") );
		int index = 0;
		for ( auto p : (*GetpvpObject()) )
		{
			if ( 0 == index % STAGE_SIZE_X )
			{
				DBGOUT_FL_F ( _T("\n") );
			}
			VEC2 v = p->GetPos ();
			bool b = p->GetValid ();
			DBGOUT_FL_F ( _T(" [%d](%d, %d), %d "), index, (int)v.x, (int)v.y, b ? 1: 0 );
			++index;
		}
		DBGOUT_FL_F ( _T(" \n") );
	}

}	//namespace GAME

