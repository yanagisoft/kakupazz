//=================================================================================================
//
//	パネルオブジェクトの配列を操作する
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "PanelCommon.h"
#include "PanelObject.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//前方宣言
	class PanelArray;
	using P_PanelArray = shared_ptr < PanelArray > ;

	//------
	class OperatePanel
	{
		P_PanelArray	m_pPnAry;

	public:
		OperatePanel ();
		OperatePanel ( const OperatePanel & rhs ) = delete;
		~OperatePanel ();

		//パネル配列設定
		void Setp ( P_PanelArray pPnAry );
		
		//各種操作
		void SwapPanel ( POINT pt0, POINT pt1 );//パネル即時交換
		void SwapPanel_V ( LONG x, LONG y );		//パネル即時交換_縦


		void SetUpRevise ( float upRevise );		//位置補正反映
		void UpPanel ();	//せりあげ

		void MovePanel ( POINT pt );		//条件確認後、パネル左右交換移動
//		void DropPanel ( LONG x, LONG y );		//パネル落下

		void SetVibration ();		//振動開始
		void StopVibration ();		//振動終了
	};




}	//namespace GAME

