//=================================================================================================
//
// パネルオブジェクト配列 ヘッダ
//		パネルオブジェクトの配列処理
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "PanelCommon.h"
#include "PanelObject.h"
#include "PanelState.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class PanelArray
	{
		SPVSP_PnOb		m_spvspPanelOb;	//パネルオブジェクト配列
		VP_PnState		m_vpPanelSt;	//パネル状態配列

		//-------------------------
		//内部関数
		//パネル生成
		void _GeneratePanel ();

	public:
		PanelArray ();
		PanelArray ( const PanelArray & rhs ) = delete;
		~PanelArray ();

		//パネルオブジェクト配列を取得
		SPVSP_PnOb GetpVP_PnOb () { return m_spvspPanelOb; }
		VSP_PnOb & GetrVP_PnObject () { return (*m_spvspPanelOb); }

		//パネルオブジェクトを取得
		SP_PnOb GetpPnOb ( POINT pt );
		SP_PnOb GetpPnOb ( UINT x, UINT y );
		SP_PnOb GetpPnOb ( UINT index );
		const SP_PnOb GetpPnOb ( POINT pt ) const;
		const SP_PnOb GetpPnOb ( UINT x, UINT y ) const;
		const SP_PnOb GetpPnOb ( UINT index ) const;

		//パネルステート配列を取得
		VP_PnState & GetrVP_PnState () { return m_vpPanelSt; }

		//パネルステートを取得
		P_PnState GetpPnSt ( POINT pt );
		P_PnState GetpPnSt ( UINT x, UINT y );
		P_PnState GetpPnSt ( UINT index );
		const P_PnState GetpPnSt ( POINT pt ) const;
		const P_PnState GetpPnSt ( UINT x, UINT y ) const;
		const P_PnState GetpPnSt ( UINT index ) const;

		//状態設定
		void SetBase ( VEC2  & base );		//基準位置設定
		void SetGameStart ();		//パネル開始設定
		void SetOver ();		//ゲーム終了処理

		//状態チェック
		bool IsTopPanel() const;		//最上位置にパネルがあるかどうか
		bool IsPanelLocked() const;		//ロック中のパネルが一つでもあるかどうか
		LONG GetMaxPanelPos () const;		//パネル最上位置
		bool IsDropStart ( LONG x, LONG y ) const;
		bool CanDrop ( LONG x, LONG y ) const;
		bool CanBeDropped ( LONG x, LONG y ) const;
		bool CanBeGetdown ( LONG x, LONG y ) const;
		bool IsChain () const;	//１つでも連鎖状態かどうか

		//添字から位置に変換
		POINT IndexToPt ( UINT index ) const;

	};

	using P_PanelArray = shared_ptr < PanelArray > ;


}	//namespace GAME


