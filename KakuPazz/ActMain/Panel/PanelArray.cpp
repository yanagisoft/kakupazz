//=================================================================================================
//
// パネルオブジェクト配列 ヘッダ
//		パネルオブジェクトの配列処理
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelArray.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	PanelArray::PanelArray ()
	{
		//パネルオブジェクト配列を確保
		m_spvspPanelOb = make_shared < VSP_PnOb > ();
		_GeneratePanel ();		//パネル生成
	}

	PanelArray::~PanelArray ()
	{
	}

	//パネル生成
	void PanelArray::_GeneratePanel ()
	{
		for ( UINT y = 0; y < STAGE_SIZE_Y; y++ )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; x++ )
			{
				//パネルオブジェクトの生成と登録
				SP_PnOb pOb = make_shared < PanelObject > ( x, y );
				pOb->SetThis ();
				m_spvspPanelOb->push_back ( pOb );

				//パネル状態の生成と登録
				P_PnState pSt = make_shared < PanelState > ();
				m_vpPanelSt.push_back ( pSt );

				//オブジェクトに状態を登録
				pOb->SetpPanelState ( pSt );
			}	
		}
	}

	//パネルオブジェクトを取得
	SP_PnOb PanelArray::GetpPnOb ( POINT pt )
	{
		return GetpPnOb ( (UINT) pt.x, (UINT) pt.y );
	}
	SP_PnOb PanelArray::GetpPnOb ( UINT x, UINT y )
	{
		UINT index = y * STAGE_SIZE_X + x;
		return GetpPnOb ( index );
	}
	SP_PnOb PanelArray::GetpPnOb ( UINT index )
	{
		if ( m_spvspPanelOb->size () <= index ) { return nullptr; }
		return (*m_spvspPanelOb)[index]; 
	}
	const SP_PnOb PanelArray::GetpPnOb ( POINT pt ) const
	{
		return GetpPnOb ( (UINT) pt.x, (UINT) pt.y );
	}
	const SP_PnOb PanelArray::GetpPnOb ( UINT x, UINT y ) const
	{
		UINT index = y * STAGE_SIZE_X + x;
		return GetpPnOb ( index );
	}
	const SP_PnOb PanelArray::GetpPnOb ( UINT index ) const
	{
		if ( m_spvspPanelOb->size () <= index ) { return nullptr; }
		return (*m_spvspPanelOb)[index]; 
	}

	//パネルステートを取得
	P_PnState PanelArray::GetpPnSt ( POINT pt )
	{
		return GetpPnSt ( (UINT) pt.x, (UINT) pt.y );
	}
	P_PnState PanelArray::GetpPnSt ( UINT x, UINT y )
	{
		UINT index = y * STAGE_SIZE_X + x;
		return GetpPnSt ( index );
	}
	P_PnState PanelArray::GetpPnSt ( UINT index )
	{
		if ( m_vpPanelSt.size () <= index ) { return nullptr; }
		return (*m_spvspPanelOb)[index]->GetpPanelState (); 
	}
	const P_PnState PanelArray::GetpPnSt ( POINT pt ) const 
	{
		return GetpPnSt ( (UINT) pt.x, (UINT) pt.y );
	}
	const P_PnState PanelArray::GetpPnSt ( UINT x, UINT y ) const 
	{
		UINT index = y * STAGE_SIZE_X + x;
		return GetpPnSt ( index );
	}
	const P_PnState PanelArray::GetpPnSt ( UINT index ) const 
	{
		if ( m_vpPanelSt.size () <= index ) { return nullptr; }
		return (*m_spvspPanelOb)[index]->GetpPanelState (); 
	}

	//基準位置設定
	void PanelArray::SetBase ( VEC2 & base )
	{
		for ( SP_PnOb pOb : (*m_spvspPanelOb) )
		{
			pOb->SetBase ( base );
		}
	}

	//パネル開始設定
	void PanelArray::SetGameStart ()
	{
		UINT index = 0;
		for ( SP_PnOb pOb : (*m_spvspPanelOb) )
		{
			//状態をGAMESTARTにする
//@temp			pOb->SetGameStart ();
			pOb->SetDefault ();

			//位置から初期色指定
			POINT pt = IndexToPt ( index ++ );
			
			//上左の種類を取得
			PANEL_KIND kind0 = PN_BLANK;
			PANEL_KIND kind1 = PN_BLANK;
			if ( 0 < pt.x ) { kind0 = GetpPnSt ( pt.x - 1, pt.y )->GetKind (); }
			if ( 0 < pt.y ) { kind1 = GetpPnSt ( pt.x, pt.y - 1 )->GetKind (); }
			
			if ( pt.y < PANEL_START_TOP )	//上半分は空白にする
			{
				pOb->SetBlank ();

				//@temp 落下テストのための段差初期配置
#if 0
				if ( pt.x < 1 && pt.y > 13 )
				{
					pOb->SetOtherKind ( kind0, kind1 );
				}
#endif // 0
			}
			else	//残りは基本色から左と上と異なるランダムな種類
			{
				pOb->SetOtherKind ( kind0, kind1 );
			}
		}
	}

	POINT PanelArray::IndexToPt ( UINT index ) const
	{
		LONG ln = index;
		POINT pt = { ln % STAGE_SIZE_X, ln / STAGE_SIZE_X  };
		return pt;
	}

	//ゲーム終了処理
	void PanelArray::SetOver ()
	{
		for ( SP_PnOb pOb : (*m_spvspPanelOb) )
		{
			pOb->SetGameOver ();
		}
	}

	//ゲームオーバー高さ位置にパネルがあるかどうか
	bool PanelArray::IsTopPanel () const
	{
		//一番上に一つでも空白以外のパネルがあるとき
		for ( UINT x = 0; x < STAGE_SIZE_X; ++x )
		{
			const SP_PnOb pOb = GetpPnOb ( x, PANEL_GAMEOVER_Y );
			if ( ! pOb->IsBlank () ) { return true; }
		}
		return false;
	}

	//ロック中のパネルが一つでもあるかどうか
	bool PanelArray::IsPanelLocked () const
	{
		//ロック状態のパネルが一つでもあるとき
		for ( SP_PnOb pOb : (*m_spvspPanelOb) )
		{
			if ( pOb->IsLocked () ) { return true; }
		}
		return false;
	}

	//パネル最上位置取得
	LONG PanelArray::GetMaxPanelPos () const
	{
		LONG ret = 0;
		bool end = false;	//終了フラグ
		LONG x = 0;
		LONG y = 0;

		//上から走査
		while ( y < STAGE_SIZE_Y )
		{
			x = 0;
			while ( x < STAGE_SIZE_X )
			{
				UINT index = y * STAGE_SIZE_X + x;

				//一つでもパネルが存在したらその段を返す
				if ( ! (*m_spvspPanelOb)[index]->IsBlank () )
				{
					ret = y;
					end = true;
					break;
				}
				x++;
			}
			if ( end ) { break; }
			y++;
		}
		return ret;
	}


	//自身の状態と、下のパネル列から見て、この位置が落下開始起点かどうか
	bool PanelArray::IsDropStart ( LONG x, LONG y ) const
	{
		//自身が落下可能かどうか
		bool bSelf = CanDrop ( x, y );

		//下のパネルから見て上が落下可能かどうか
		SP_PnOb pPnObBottom = GetpPnOb ( x, y + 1 );
		bool bBottom = pPnObBottom->CanBeDropped ();

		//両方満たしてTRUE
		return bSelf && bBottom;
	}

	//自身の状態が落下可能かどうか
	bool PanelArray::CanDrop ( LONG x, LONG y ) const
	{
		SP_PnOb pPnOb = GetpPnOb ( x, y );
		if ( nullptr == pPnOb ) { return false; }
		return pPnOb->CanDrop ();
	}

	//この場所に落下可能か
	bool PanelArray::CanBeDropped ( LONG x, LONG y ) const
	{
		SP_PnOb pPnOb = GetpPnOb ( x, y );
		if ( nullptr == pPnOb ) { return false; }
		return pPnOb->CanBeDropped ();
	}

	//この場所に着地可能か
	bool PanelArray::CanBeGetdown ( LONG x, LONG y ) const
	{
		SP_PnOb pPnOb = GetpPnOb ( x, y );
		if ( nullptr == pPnOb ) { return false; }
		return pPnOb->CanBeGetdown ();
	}


	//１つでも連鎖状態かどうか
	bool PanelArray::IsChain () const
	{
		for ( auto pOb : (*m_spvspPanelOb) )
		{
			if ( pOb->GetChain () ) { return true; }
		}
		return false;
	}

}	//namespace GAME

