//=================================================================================================
//
// PanelGameState ヘッダファイル
//	GameStateにの継承先によって振る舞いを変えるパターン
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelGameState.h"
#include "PanelObject.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//-------------------------------------------------------------------------------------------------
	// 定数宣言
	//-------------------------------------------------------------------------------------------------

	//テクスチャ名 配列
	const TEXTURE_NAME
	PGS::m_textureName[PANEL_STATE_NUM][PANEL_KIND_NUM] =
	{
		{ PANEL0, PANEL1, PANEL2, PANEL3, PANEL4, PANEL5, TX_BLANK },
		{ PANEL_LIGHT0, PANEL_LIGHT1, PANEL_LIGHT2, PANEL_LIGHT3, PANEL_LIGHT4, PANEL_LIGHT5, TX_BLANK },
		{ PANEL_FACE0, PANEL_FACE1, PANEL_FACE2, PANEL_FACE3, PANEL_FACE4, PANEL_FACE5, TX_BLANK }
	};

	//調整パラメータ
	enum PANEL_PARAM
	{
		PANEL_ERASE_WAIT_TIME = 10,		//消去点滅時間
		PANEL_GETDOWN_WAIT_TIME = 8,	//着地時間
	};


	//===========================================================
	//[基本]
	PanelGameState::PanelGameState ( WP_PnObject p )
		: m_wpOb ( p )
		, m_blinkTimer ( 0 ), m_movingPos ( 0, 0 ), m_distance ( 0, 0 )
		, m_moveFrame ( 0 ), m_moveTimer ( 0 ), m_getdownTimer ( 0 )
		, m_scaling ( 1.f, 1.f )
	{
	}

	//テクスチャの選択
	TEXTURE_NAME PanelGameState::SelectTexture ( PANEL_KIND kind )
	{
		return m_textureName [ PS_DEFAULT ] [ kind ];
	}

	_CLR PanelGameState::SelectClr ()
	{
		_CLR color = 0xffffffff;
		return  color;
	}


	//===========================================================
	//[通常] 初期化
	void PGS_Default::Init ()
	{
		m_movingPos.x = 0;
		m_blinkTimer = 0;
		m_scaling = VEC2 ( 1.f, 1.f );
	}

	//===========================================================
	//[移動] 設定
	void PGS_Moving::Set ( P_PnState pTarget, VEC2 distance, UINT frame )
	{
		m_target = pTarget;
		m_distance = distance;
		m_moveFrame = frame;
	}

	//[移動] 動作
	void PGS_Moving::Act ()
	{
		//位置計算
		m_movingPos += m_distance / (FLOAT)m_moveFrame;	// (距離)/(時間)
		
		//タイマ
		++m_moveTimer;

		//移動時間満了時
		if ( m_moveFrame == m_moveTimer )
		{
			//初期化
			m_movingPos.x = 0;
			m_movingPos.y = 0;
			m_moveTimer = 0;

			m_wpOb.lock()->SetpPanelState ( m_target );	//入替	
			m_target = nullptr;
			m_wpOb.lock()->SetDefault ();	//通常状態に戻る
		}
	}


	//===========================================================
	//[落下] 設定
	void PGS_Drop::Set ( P_PnState pTarget, VEC2 distance, UINT frame )
	{
		m_target = pTarget;
		m_distance = distance;
		m_moveFrame = frame;
	}

	_CLR PGS_Drop::SelectClr ()
	{
//@test		return 0xff8080ff;//着地時に青色
		return 0xffffffff;
	}

	//[落下] 動作
	void PGS_Drop::Act ()
	{
		//落下の入替(遷移先指定)は同時に落下する縦パネル１列を循環させるので、
		//DropSequenceが同時に行う

		//位置計算
		m_movingPos += m_distance / (FLOAT)m_moveFrame;	// (距離)/(時間)
		
		//タイマ
		++ m_moveTimer;

		//終了の処理はDropSequenceから行う
	}

	//[落下] 終了
	void PGS_Drop::Swap ()
	{
		m_moveTimer = 0;
		m_movingPos.x = 0;
		m_movingPos.y = 0;
			
		m_wpOb.lock()->SetpPanelState ( m_target );	//入替
		m_target = nullptr;
	}

	//===========================================================
	//[着地] 初期化
	void PGS_Getdown::Init ()
	{
		//着地モーション
		m_scaling = VEC2 ( 1.f, 0.5f );
		m_getdownTimer = 0;
	}

	_CLR PGS_Getdown::SelectClr ()
	{
//@test		return 0xffff8080;//着地時に赤色
		return 0xffffffff;
	}

	//[着地] 動作
	void PGS_Getdown::Act ()
	{
		++ m_getdownTimer;
		
		//着地完了時
		if ( PANEL_GETDOWN_WAIT_TIME < m_getdownTimer )
		{
			m_getdownTimer = 0;

			//通常状態に戻る
			m_wpOb.lock()->SetDefault ();
		}
	}


	//===========================================================
	//[消去待機] 動作
	void PGS_EraseWait::Act ()
	{
		//点滅カウント
		if ( PANEL_ERASE_WAIT_TIME > ++ m_blinkTimer ) { return; }

		//カウント終了時
		m_blinkTimer = 0;
	}

	//[消去待機] テクスチャの選択
	TEXTURE_NAME PGS_EraseWait::SelectTexture ( PANEL_KIND kind )
	{
		//点滅によってテクスチャを遷移する
		return m_textureName [ m_blinkTimer % PANEL_STATE_NUM ] [ kind ];
	}


	//===========================================================
	//[消去] 開始
	void PGS_Erase::Start ()
	{
		m_wpOb.lock()->StartEffect ();		//エフェクトの開始
		m_wpOb.lock()->SetBlank ();		//状態は消去中だが、表示を空白へ
	}

	//===========================================================
	//[おじゃま消去] 開始
	void PGS_EraseHamper::Start ()
	{
		m_wpOb.lock()->StartEffect ();		//エフェクトの開始
		m_wpOb.lock()->SetStart ();		//状態は消去中だが、表示を種類付き通常パネルへ
	}

	//===========================================================
	//[開始]

	//===========================================================
	//[終了] テクスチャの選択
	TEXTURE_NAME	PGS_GameOver::SelectTexture ( PANEL_KIND kind )
	{
		return m_textureName[PS_FACE][kind];
	}


}	//namespace GAME

