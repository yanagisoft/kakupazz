//=================================================================================================
//
// PanelGraphic ヘッダファイル
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
//#include "Const.h"
#include "Game.h"
#include "PanelArray.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class DebugPanelGraphic : public GrpAcv
	{
	public:
		DebugPanelGraphic ();
		DebugPanelGraphic ( const DebugPanelGraphic & rhs ) = delete;
		~DebugPanelGraphic () = default;

	private:
		void SetPnOb ( P_PanelArray panelArray );

	};

	using P_DbgPnGrp = shared_ptr < DebugPanelGraphic >;


}	//namespace GAME


