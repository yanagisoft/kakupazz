//=================================================================================================
//
// PanelObject ソースファイル
//		パネルの位置を表すオブジェクト
//		表示はPanelGraphicで行う
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelObject.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//-------------------------------------------------------------------------------------------------
	//調整パラメータ

	//-------------------------------------------------------------------------------------------------
	//コンストラクタ
	PanelObject::PanelObject ( LONG x, LONG y )
		: m_point ( POINT { x, y } ), m_base ( VEC2 ( 0, 0 ) )
	{
		//拡大縮小の基準点
		SetScalingCenter ( VEC2 ( PANEL_SIZE_X / 2, PANEL_SIZE_Y ) );
		
		//エフェクトオブジェクト
		m_pEfObject = make_shared < EfObject > ( x, y );
		
		//--------------------------------------------------
		m_upRevise = 0;
		m_vibration = 0;
	}

	PanelObject::~PanelObject ()
	{
	}

	//thisを設定する
	void PanelObject::SetThis ()
	{
		//--------------------------------------------------
		//状態
		//State: 遷移のときに内部で確保して返す
		//Strategy: 確保しておいて遷移時に切替る

		//thisを変換
		//@info コンストラクタ内ではshared_from_thisは使えない
		shared_ptr < PanelObject > p = shared_from_this ();

		//各状態を確保しつつ設定
		m_pgs_Default = make_shared < PGS_Default > ( p );
		m_pgs_Moving = make_shared < PGS_Moving > ( p );
		m_pgs_Drop = make_shared < PGS_Drop > ( p );
		m_pgs_Getdown = make_shared < PGS_Getdown > ( p );
		m_pgs_EraseWait = make_shared < PGS_EraseWait > ( p );
		m_pgs_Erase = make_shared < PGS_Erase > ( p );
		m_pgs_EraseHamper = make_shared < PGS_EraseHamper > ( p );
		m_pgs_GameStart = make_shared < PGS_GameStart > ( p );
		m_pgs_GameOver = make_shared < PGS_GameOver > ( p);

		//最初の状態
		mwp_pgs = m_pgs_Default;
	}

	//動作
	void PanelObject::Move ()
	{
		//状態で分岐
		mwp_pgs.lock()->Act ();		//状態による動作

		//随時処理(表示関連など)
		_Position ();		//位置計算
		_SelectTexture ();	//テクスチャ選択
		_SelectColor ();	//カラー選択

		//エフェクトマトリックスの更新
		m_pEfObject->Move ();

		GameObject::Move ();
	}

	//値のみコピー
	void PanelObject::Copy ( const PanelObject & rhs )
	{
		m_state->SetKind ( rhs.GetKind () );
	}

	//基準位置の設定
	void PanelObject::SetBase ( VEC2 & vec )
	{
		m_base = vec; 
		m_pEfObject->SetBase ( vec );
	}


	//--------------------------------------------------------------------
	//内部関数

	//位置計算
	void PanelObject::_Position ()
	{
		//振動
		if ( 0 != m_vibration )
		{
			m_vibration *= -1.f;
		}

		//パネル位置計算
		VEC2 pos = m_base;
		pos.x += (float)(m_point.x * PANEL_SIZE_X);
		pos.y += (float)(m_point.y * PANEL_SIZE_Y) + m_upRevise;

		pos += mwp_pgs.lock()->GetMoving ();
		pos.x += m_vibration;
		SetPos ( pos );

		//エフェクトマトリックスの位置計算
		m_pEfObject->Position ( mwp_pgs.lock()->GetMoving (), m_upRevise );
	}

	//テクスチャ選択
	void PanelObject::_SelectTexture ()
	{
		//種類と状態からテクスチャを取得
		TEXTURE_NAME t;
		if ( HAMPER_NUM != m_state->GetHamper () )
		{
			t = TX_HAMPER_C;
		}
		else
		{
			t = mwp_pgs.lock()->SelectTexture ( m_state->GetKind () );
		}
		SetIndexTexture ( t );

		//拡大縮小
		VEC2 scaling = mwp_pgs.lock()->GetScaling ();
		SetScaling ( scaling );

		//おじゃま
#if 0
		if ( m_state->GetHamper () )
		{
			if ( m_point.x == 0 )
			{
				SetIndexTexture ( TX_HAMPER_L );
			}
			else if ( m_point.x == STAGE_SIZE_X - 1 )
			{
				SetIndexTexture ( TX_HAMPER_R );
			}
			else
			{
				SetIndexTexture ( TX_HAMPER_C );
			}
		}
#endif // 0
	}

	//カラー選択
	void PanelObject::_SelectColor ()
	{
		//最下段のとき暗化
		if ( STAGE_SIZE_Y - 1 == m_point.y ) 
		{
			SetColor ( 0xff808080 );
		}
		//連鎖フラグ
		else if ( m_state->GetChain () )
		{
			SetColor ( 0xff80ff80 );
		}
		else
		{
			SetColor ( mwp_pgs.lock ()->SelectClr () );
		}
	}

	void PanelObject::SetOtherKind ( PANEL_KIND k0, PANEL_KIND k1 )
	{
		m_state->SetOtherKind ( k0, k1 );
	}

	//基本状態
	void PanelObject::SetDefault ()
	{
		mwp_pgs = m_pgs_Default;
		m_state->SetDefault ();
	}

	//開始状態
	void PanelObject::SetStart ()
	{
		mwp_pgs = m_pgs_Default;
		m_state->Start ();
	}

	//-------------------------------------------
	//移動
	bool PanelObject::CanMove () const
	{
		if ( m_state->IsHamper () ) { return false; }	//おじゃまのとき不可
		return mwp_pgs.lock()->CanMove ();
	}

	//移動開始
	//引数	P_PnState		pTarget		交換対象
	//		D3DXVECTOR2		distance	距離
	//		UINT			frame		移動完了時間
	void PanelObject::StartMove ( P_PnState pTarget, VEC2 distance, UINT frame )
	{
		m_pgs_Moving->Set ( pTarget, distance, frame );
		m_state->SetDefault ();
		mwp_pgs = m_pgs_Moving;
	}

	//-------------------------------------------
	//自身が落下可能か
	bool PanelObject::CanDrop () const
	{
		if ( IsBlank () ) { return false; }	//空白のとき不可
		return mwp_pgs.lock()->CanDrop ();
	}

	//一つ上が落下可能か
	bool PanelObject::CanBeDropped () const
	{
		//最下段のとき不可
		if ( STAGE_SIZE_Y == m_point.y ) { return false; }

		//動作中でなく、かつ空白である
		//空白でも入替などの動作中の可能性がある
		bool b0 = mwp_pgs.lock()->CanBeDropped ();
		bool b1 = IsBlank ();
		return b0 && b1;
	}

	//一つ上が着地可能か
	bool PanelObject::CanBeGetdown () const
	{
		//最下段のとき不可
		if ( STAGE_SIZE_Y == m_point.y ) { return false; }

		//動作中である、または空白でない
		bool b0 =  mwp_pgs.lock()->IsLocked ();
		bool b1 = ! IsBlank ();
		return b0 || b1;
	}


	//落下開始
	//引数	P_PnState	pTarget		交換対象
	//		VEC2		distance	距離
	void PanelObject::StartDrop ( P_PnState pTarget, VEC2 distance )
	{
		m_pgs_Drop->Set ( pTarget, distance, DROP_TIME );
		m_state->SetDefault ();
		mwp_pgs = m_pgs_Drop;
	}

	void PanelObject::ContinueDrop ()
	{
		m_pgs_Drop->Swap ();
		m_state->SetDefault ();
	}

	void PanelObject::EndDrop ()
	{
		m_pgs_Drop->Swap ();
		SetDefault ();
	}

	//-------------------------------------------
	//着地
	void PanelObject::StartGetdown ()
	{
		m_pgs_Drop->Swap ();
		m_state->SetDefault ();
		m_pgs_Getdown->Init ();
		mwp_pgs = m_pgs_Getdown;
	}

	//---------------------------

	//消去可能か
	bool PanelObject::CanErase () const
	{
		if ( IsBlank () ) { return false; }	//空白のとき不可
		if ( m_state->IsHamper () ) { return false; }	//おじゃまのとき不可
		return mwp_pgs.lock()->CanErase ();
	}

	//消去待機
	void PanelObject::EraseWait ()
	{
		mwp_pgs = m_pgs_EraseWait;
	}

	//消去開始
	void PanelObject::StartErase ()
	{
		m_pgs_Erase->Start ();
		mwp_pgs = m_pgs_Erase;
	}

	//エフェクトの開始
	void PanelObject::StartEffect ()
	{
		GetpEfObject ()->SetValid ( true );
	}


	//おじゃま指定
	void PanelObject::SetHamper ( HAMPER hmp )
	{
		m_state->SetKind ( PN_HAMPER );
		m_state->SetHamper ( hmp );
	}

	void PanelObject::StartEraseHamper ()
	{
		m_pgs_EraseHamper->Start ();
		mwp_pgs = m_pgs_EraseHamper;
	}


}	//namespace GAME

