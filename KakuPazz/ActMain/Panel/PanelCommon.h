﻿//=================================================================================================
//
//		パネル共通ヘッダ
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

//==============================================================================
//		定数宣言
//==============================================================================

	//配置されるパネルの個数
	enum STAGE_SIZE
	{
		STAGE_SIZE_X = 6,		//ステージ上のパネル個数：横
		STAGE_SIZE_Y = 24,		//ステージ上のパネル個数：縦(おじゃま込、表示は１２)
		STAGE_PANEL_NUM = STAGE_SIZE_X * STAGE_SIZE_Y,	//パネル個数
	};

	//種類
	enum PANEL_KIND
	{
		GREEN = 0,
		ORENGE = 1,
		BLUE = 2,
		RED = 3,
		WATER = 4,
		PURPLE = 5,
		PN_BLANK = 6,
		PN_HAMPER = 7,
		PANEL_KIND_NUM
	};

	//配置
	//上から下へインクリメント
	//0 □→□
	//	□→□ N
	enum ALIGN
	{
		PANEL_SIZE_X = 32,					//パネル一辺
		PANEL_SIZE_Y = 32,
		PANEL_BASE_X = 224,					//パネル位置原点
		PANEL_BASE_Y = 48 + PANEL_SIZE_Y - 384,		//一段下まで
		PANEL_BASE_1P_X = 48,				//パネル位置原点1p
		PANEL_BASE_1P_Y = PANEL_BASE_Y,
		PANEL_BASE_2P_X = 400,				//パネル位置原点2p
		PANEL_BASE_2P_Y = PANEL_BASE_Y,

		PANEL_BOTTOM = PANEL_BASE_Y + PANEL_SIZE_Y * STAGE_SIZE_Y - PANEL_SIZE_Y,	//パネル下位置隠し背景

		EF_SIZE_X = 48,
		EF_SIZE_Y = 48,

		STAGE_WIDTH = STAGE_SIZE_X * PANEL_SIZE_X,
		STAGE_HEIGHT = STAGE_SIZE_Y * PANEL_SIZE_Y,
	};

//---------------------------------------------------------------------------------------------
	enum PANEL_ARRAY_PARAM
	{
		PANEL_START_TOP = 6 + 12,	//パネルの初期高さ
		PANEL_GAMEOVER_Y = 12,		//ゲームオーバー高さ
		PANEL_MOVE_SPEED = 5,		//パネル入替速度[f]
		PANEL_DROP_SPEED = 60,		//パネル落下速度[f]
	};
	enum PANEL_OBJECT_PARAM
	{
		DROP_TIME = 8,
	};


	enum CHARA_GRAPHIC
	{
		STAND_1P_X = PANEL_BASE_1P_X,
		STAND_2P_X = PANEL_BASE_2P_X,
		STAND_1P_Y = 48,//PANEL_BASE_1P_Y - PANEL_SIZE_Y - 384,
		STAND_2P_Y = 48,//PANEL_BASE_2P_Y - PANEL_SIZE_Y - 384,

		STAND_1P_L = 176,
		STAND_1P_T = 64,
		STAND_1P_R = STAND_1P_L + STAGE_WIDTH,
		STAND_1P_B = STAND_1P_T + PANEL_SIZE_Y * 12,

		STAND_2P_L = 176,
		STAND_2P_T = 64,
		STAND_2P_R = STAND_2P_L + STAGE_WIDTH,
		STAND_2P_B = STAND_2P_T + PANEL_SIZE_Y * 12,
	};



#if 0
	//パネルゲーム状態
	enum PANEL_GAME_STATE
	{
		PGS_GAMESTART,		//開始
//		PGS_BLANK,			//空白
		PGS_DEFAULT,		//基本
		PGS_MOVE,			//移動
		PGS_DROPRESERVE,	//落下予約
		PGS_DROP,			//落下中
		PGS_DROPEXPAND,		//落下継続
		PGS_GETDOWN,		//着地
		PGS_LOCK,			//ロック
		PGS_ERASERESERVE,	//消去予約
		PGS_ERASE,			//消去
		PGS_GAMEOVER,		//終了
	};
#endif // 0

	//ゲームアクト進行（スタート、カウント、プレイ、オーバー、タイトルへ移項）
	//(1p勝, 2p勝, 選択)
	enum GAME_PROCEDURE
	{
		GAME_PROCEDURE_START,
		GAME_PROCEDURE_COUNT,
		GAME_PROCEDURE_PLAY,
		GAME_PROCEDURE_OVER,
		GAME_PROCEDURE_1P_WIN,
		GAME_PROCEDURE_2P_WIN,
		GAME_PROCEDURE_SELECT,
		GAME_PROCEDURE_TITLE,
	};
	using P_Game_Proc = shared_ptr < GAME_PROCEDURE >;

	//プレイヤーアクト進行（スタート、プレイ、ピンチ, オーバー、タイトルへ移項）
	enum PLAYER_PROCEDURE
	{
		PLAYER_PROCEDURE_START,
		PLAYER_PROCEDURE_PLAY,
		PLAYER_PROCEDURE_PINCH,
		PLAYER_PROCEDURE_OVER,
		PLAYER_PROCEDURE_TITLE,
	};

	//----------------------------------------------------------
	//パネルテクスチャ状態
	enum PANEL_STATE
	{
		PS_DEFAULT,
		PS_BLINK,
		PS_FACE,
		PANEL_STATE_NUM
	};

	//テクスチャ名
	enum TEXTURE_NAME
	{
		PANEL0, PANEL1, PANEL2, PANEL3, PANEL4, PANEL5,
		PANEL_LIGHT0, PANEL_LIGHT1, PANEL_LIGHT2, PANEL_LIGHT3, PANEL_LIGHT4, PANEL_LIGHT5,
		PANEL_FACE0, PANEL_FACE1, PANEL_FACE2, PANEL_FACE3, PANEL_FACE4, PANEL_FACE5,
		TX_BLANK,
		//１列（左端、中央、右端）
		TX_HAMPER_L, TX_HAMPER_C, TX_HAMPER_R,
		//複数列(上:U, 中:M, 下:B)
		TX_HAMPER_UL, TX_HAMPER_UC, TX_HAMPER_UR,
		TX_HAMPER_ML, TX_HAMPER_MC, TX_HAMPER_MR,
		TX_HAMPER_BL, TX_HAMPER_BC, TX_HAMPER_BR,
	};

	//おじゃま
	enum HAMPER
	{
		//１列（左端、中央、右端）
		HAMPER_L, HAMPER_C, HAMPER_R,
		//複数列(上:U, 中:M, 下:B)
		HAMPER_UL, HAMPER_UC, HAMPER_UR,
		HAMPER_ML, HAMPER_MC, HAMPER_MR,
		HAMPER_BL, HAMPER_BC, HAMPER_BR,
		//個数(おじゃまでない値)
		HAMPER_NUM,
	};


}	//namespace GAME

