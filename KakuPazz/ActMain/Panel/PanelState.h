﻿//=================================================================================================
//
//	パネル状態
//		入替時に保持するパラメータ
//		パネル種類と各種フラグ
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "PanelCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//パネル状態(移動時、落下時に入れ替えるパラメータ)
	class PanelState
	{
		PANEL_KIND	m_kind;			//種類
		bool		m_dropStart;	//落下起点フラグ
		bool		m_dropAble;		//落下可能フラグ
		bool		m_drop;			//落下フラグ
		bool		m_dropCheck;	//落下確定フラグ
		bool		m_getdown;		//着地フラグ
		bool		m_erase;		//消去フラグ
		bool		m_chain;		//連鎖フラグ
		HAMPER		m_hamper;		//おじゃま
		UINT		m_hamperID;		//おじゃまID

	public:	
		PanelState();
		PanelState( const PanelState& rhs );	//コピー可能
		~PanelState();

		//種類も含めた初期化
		void Start();

		//パネル種類を変えない初期化
		void SetDefault ();

		//パネル種類
		void SetKind ( PANEL_KIND k ) { m_kind = k; }
		void SetRandomKind();
		//上と左と異なる種類を設定
		void SetOtherKind ( PANEL_KIND k0, PANEL_KIND k1 );
		PANEL_KIND GetKind () const { return m_kind; }
		bool IsBlank() const { return ( PN_BLANK == m_kind ); }

		//落下フラグ
		void SetDropAble ( bool b ) { m_dropAble = b; }
		bool GetDropAble () const { return m_dropAble; }
		void SetDropStart ( bool b ) { m_dropStart = b; }
		bool GetDropStart () const { return m_dropStart; }
		void SetDrop ( bool b ) { m_drop = b; }
		bool GetDrop () const { return m_drop; }
		void SetDropCheck ( bool b ) { m_dropCheck = b; }
		bool GetDropCheck () const { return m_dropCheck; }

		//着地フラグ
		void SetGetdown ( bool b ) { m_getdown = b; }
		bool GetGetdown () const { return m_getdown; }

		//消去フラグ
		void SetErase ( bool b ) { m_erase = b; }
		bool GetErase () const { return m_erase; }

		//連鎖フラグ
		void SetChain ( bool b ) { if ( PN_BLANK != m_kind ) { m_chain = b; } }
		bool GetChain () const { return m_chain; }

		//おじゃま(パネル種類はPANEL_KIND, さらにおじゃまの種類をHAMPERで定義する)
		bool IsHamper () { return PN_HAMPER == m_kind; }
		void SetHamper ( HAMPER hmp ) { m_hamper = hmp; }
		HAMPER GetHamper () const { return m_hamper; }

		//おじゃまID
		void SetHamperID ( UINT id ) { m_hamperID = id; }
		UINT GetHamperID () const { return m_hamperID; }
	};

	using P_PnState = shared_ptr < PanelState >;
	using VP_PnState = vector < P_PnState >;

}	//namespace GAME

