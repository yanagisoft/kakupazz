﻿//=================================================================================================
//
// パネルの管理
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelManager.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//----------------------------------------------------------------------------
	//	調整パラメータ
	//----------------------------------------------------------------------------
	enum PANEL_MANAGER_PARAM
	{
		PANEL_UP_VEL_SPEED = 8,		//せりあげ速さ・加速
//@test
		PANEL_UP_VEL_NORMAL = 1,	//せりあげ速さ・通常
//		PANEL_UP_VEL_NORMAL = 0,	//せりあげ速さ・通常
		PANEL_UP_SIZE = 32,			//せりあげ幅
		HAMPER_MAX = 12,			//おじゃま最大列
		PINCH_TIME = 60,			//ピンチ時間(ゲームオーバー猶予)
//		PINCH_TIME = 180,			//ピンチ時間(ゲームオーバー猶予)
	};

	//モードによる基準位置
	const POINT PanelManager::m_basePoint =
	{ PANEL_BASE_X, PANEL_BASE_Y };		//SINGLE
	
	const POINT PanelManager::m_basePointDouble [ _PLAYER_NUM ] =
	{
		{ PANEL_BASE_1P_X, PANEL_BASE_1P_Y },	//1P
		{ PANEL_BASE_2P_X, PANEL_BASE_2P_Y }	//2P
	};

	//----------------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------------
	PanelManager::PanelManager ( P_Game_Proc procedure )
		: m_procedure ( procedure ), m_playerProc ( PLAYER_PROCEDURE_START )
		, m_gameStart ( false ), m_bGrap ( false )
		, m_dispCount ( 0 )
	{
		//パネル配列の初期化
		P_PanelArray pAry = make_shared < PanelArray > ();
		m_panelArray = pAry;

		m_operatePanel.Setp ( pAry );		//パネル操作に設定
		m_pnGrp = make_shared < PanelGraphic > ( pAry );	//パネル表示の初期化
		m_pEfGrp = make_shared < EfGraphic > ( pAry );		//エフェクト表示の初期化
		m_pErase = make_unique < Erase > ( pAry );		//イレース
		m_pDrop = make_unique < Drop > ( pAry );		//ドロップ

		m_hmpMngr = make_shared < HamperManager > ();
		m_hmpMngr->SetpPnAry ( pAry );	//おじゃま
		m_pDrop->SetHmpMngr ( m_hmpMngr );
		m_pErase->SetHmpMngr ( m_hmpMngr );


		//@debug
#if 0
		//パネル種類　入替テスト
		m_dbgPnGrp = make_shared < DebugPanelGraphic > ();
		m_dbgPnGrp->ClearObject ();
		m_tempPnOb = make_shared < PanelObject > ( 0, 0 );
		VEC2 vecBase = VEC2 ( 0, 0 );
		m_tempPnOb->SetBase ( vecBase );
		m_tempPnOb->SetThis ();
		P_PnState pState = make_shared < PanelState > ();
		m_tempPnOb->SetpPanelState ( pState );
		m_dbgPnGrp->AddpObject ( m_tempPnOb );
#endif // 0

		//カーソル
		m_pCursor = make_shared < Cursor > ();

		//連鎖表示
		m_dispChain = make_shared < DispChain > ();


		//タスクのセット
		AddTask ( m_pnGrp );
		AddTask ( m_pEfGrp );
		AddTask ( m_pCursor );
		AddTask ( m_dispChain );

		//@debug　パネル直入替
#if 0
		AddTask ( m_dbgPnGrp );
#endif // 0
	}

	//----------------------------------------------------------------------------
	//		デストラクタ
	//----------------------------------------------------------------------------
	PanelManager::~PanelManager ()
	{
	}

	//----------------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------------
	void PanelManager::ParamInit ()
	{
		//※ゲームパラメータ（モード、プレイヤ）による変数はこのInit()で設定する
		// 表示の基準位置( m_base )など

		//ゲームモードとプレイヤによる基準位置設定
		switch ( m_mode )
		{
		case SINGLE: _SetSingleBase (); break;
		case DOUBLE_1P_CPU: _SetDoubleBase(); break;
		case DOUBLE_1P_2P: _SetDoubleBase(); break;
		case DOUBLE_CPU_CPU: _SetDoubleBase(); break;
		default: break;
		}

		//パネルの基準位置
		m_panelArray->SetBase ( m_base );

		//カーソルの基準位置
		m_pCursor->SetBase ( m_base );
		m_pCursor->SetPlayerNum ( m_playerNum );

		//ゲーム初期化
		_GameInit ();
	}

	void PanelManager::_SetSingleBase ()
	{
		m_base.x = 1.f * m_basePoint.x;
		m_base.y = 1.f * m_basePoint.y;
	}

	void PanelManager::_SetDoubleBase ()
	{
		m_base.x = 1.f * m_basePointDouble[m_playerNum].x;
		m_base.y = 1.f * m_basePointDouble[m_playerNum].y;
	}

	void PanelManager::Init ()
	{
		TASK_VEC::Init ();
	}


	//----------------------------------------------------------------------------
	//		メイン動作
	//----------------------------------------------------------------------------
	void PanelManager::Move ()
	{
		switch ( * m_procedure )
		{
		case GAME_PROCEDURE_START: _Start (); break;
		case GAME_PROCEDURE_PLAY: _Play (); break;
		case GAME_PROCEDURE_OVER: _Over (); break;
		default: break;
		}
		TASK_VEC::Move ();
	}

	//----------------------------------------------------------------------------
	//		ゲームスタート時
	//----------------------------------------------------------------------------
	void PanelManager::_Start ()
	{
		//GAME_PROCEDURE_STARTの中でも初回のみ初期化
		if ( ! m_gameStart ) { _GameInit (); }
	}

	void PanelManager::_GameInit ()
	{
		//ゲーム開始フラグ
		m_gameStart = true;

		//状態
		* m_procedure = GAME_PROCEDURE_START;
//@temp		* m_procedure = GAME_PROCEDURE_PLAY;
		m_playerProc = PLAYER_PROCEDURE_START;

		//各パラメータ
		m_upRevise = 0;		//上昇補正位置
		m_pinchTimer = 0;
		m_vibelation = 0;
		m_hamperNum = 0;
		m_enemyChain = 0;
		m_myChain = 0;

		//パネル設定
		m_panelArray->SetGameStart ();

		//@test
		//おじゃまテスト
#define TEST 0
#if TEST
		m_hmpMngr->GenerateHamper ( 1 );
#endif // TEST

	}


	//----------------------------------------------------------------------------
	//	ゲームプレイ
	//----------------------------------------------------------------------------
	void PanelManager::_Play ()
	{
		_PlayInput ();	//入力
		_PlayEnemy ();	//相手
		_PlayDrop ();	//落下
		_PlayErase ();	//消去
		_PlayUp ();		//全体位置上昇

		//プレイヤー状態で分岐
		switch ( m_playerProc )
		{
		case PLAYER_PROCEDURE_PINCH: _Pinch (); break;
		default: break;
		}
	}


	//----------------------------------------------------------------------------
	//		ゲームオーバー時
	//----------------------------------------------------------------------------
	void PanelManager::_Over ()
	{
	}


	//----------------------------------------------------------------------------
	//	内部関数
	//----------------------------------------------------------------------------
	//入力
	void PanelManager::_PlayInput ()
	{
#if 0
		//配置リセット
		if ( GetAsyncKeyState ( 'R' ) & 0x0001 )
		{
			m_panelArray->SetGameStart ();
			TRACE_F ( _T("Reset\n") );
		}
#endif // 0

#if 0
		//DEBUG_FILE出力
		static bool b = true;
		if ( b && ::GetAsyncKeyState ( 'P' ) & 0x0001 )
		{
			DBGOUT_FL_F ( _T ( "PanelManager::Play()\n" ) );
			//			m_pEfGrp->Printf ();
			m_pnGrp->Printf ();
			b = false;	//一度のみ
		}
#endif // 0

		//カーソルで入替キーが押された
		if ( m_pCursor->CallSwap () )
		{
			//カーソルから位置取得
			POINT pt = m_pCursor->GetPoint ();

			//パネルの位置から条件確認後、左右交換移動
			m_operatePanel.MovePanel ( pt );
		}

		//せりあげキーが押されている状態のとき
		if ( m_pCursor->CallUp () )
		{
			m_upVel = PANEL_UP_VEL_SPEED * 0.1f;	//全体上昇速度増加
		}
		else
		{
			m_upVel = PANEL_UP_VEL_NORMAL * 0.1f;	//通常全体上昇速度
		}

		//@debug
#if 0
		//======================
		//test
		//======================
		//スポイト
		if ( m_pCursor->CallDebugSpoit () )
		{
			//カーソルから位置取得して一時保存
			m_tempPt = m_pCursor->GetPoint ();
			m_tempPnOb->Copy ( ( * m_panelArray->GetpPnOb ( m_tempPt ) ) );
			m_bGrap = true;
		}

		//貼付
		if ( m_pCursor->CallDebugPaste () )
		{
			POINT pt = m_pCursor->GetPoint ();
			SP_PnOb pob = m_panelArray->GetpPnOb ( pt );
			pob->Copy ( ( * m_tempPnOb ) );
			m_bGrap = false;
		}

		//======================
#endif // 1


		//おじゃま
#if 0
		//おじゃま行指定
		if ( DxInput::instance ()->PushOneKeyboard ( DIK_A ) )
		{
			if ( m_hamperNum < HAMPER_MAX ) { m_hamperNum++; }
		}
		if ( DxInput::instance ()->PushOneKeyboard ( DIK_S ) )
		{
			if ( m_hamperNum > 1 ) { m_hamperNum--; }
		}
//		DebugOutGameText::instance()->DebugOutfN ( 2, TEXT("おじゃま：%d"), m_hamperNum );

		//おじゃま生成
		m_myChain = 0;
		if ( m_pCursor->CallFunc1 () )
		{
			m_myChain = m_hamperNum;
		}
		//		DebugOutGameText::instance()->DebugOutfN ( 0, TEXT("m_hamper.size () = %d"), m_hamper.size () );
#endif // 0
	}

	//相手の状況を反映
	void PanelManager::_PlayEnemy ()
	{
		//相手の連鎖を反映
#if 0
		if ( 0 < m_enemyChain )
		{
			//対象のパネル状態を列挙
			P_PnState state[STAGE_SIZE_X * HAMPER_MAX];
			for ( UINT i = 0; i < m_enemyChain; i++ )
			{
				for ( UINT j = 0; j < STAGE_SIZE_X; j++ )
				{
					UINT index = i * STAGE_SIZE_X + j;
					state[i * STAGE_SIZE_X + j] = m_vpPanelOb[index]->GetPanelState ();
				}
			}
//			m_hamper.Generate ( state, STAGE_SIZE_X * m_enemyChain );			//おじゃま監理に通達
			m_enemyChain = 0;
		}
#endif // 0
	}

	//落下
	void PanelManager::_PlayDrop ()
	{
		m_pDrop->Do ();
	}

	//消去
	void PanelManager::_PlayErase ()
	{
		//消去チェックと実行
		m_pErase->Do ();

		//同時数・連鎖数取得
		UINT sameNum = m_pErase->GetSameNum ();
		UINT chainNum = m_pErase->GetChainNum ();

		//表示
		if ( 0 < chainNum )
		{
			m_dispChain->On ( chainNum );
		}
		else
		{
			if ( m_dispChain->IsActive () )
			{
				m_dispChain->Off ();
			}
		}

		//表示
		if ( 0 < sameNum )
		{
			m_dispCount = 60;
//			DBGOUT_WND->SetPos ( 1, 0, 20 );
//			DBGOUT_WND_F ( 1, _T("sameNum = %d"), sameNum );
		}

		if ( 0 < chainNum )
		{
			m_dispCount = 60;
//			DBGOUT_WND->SetPos ( 2, 0, 40 );
//			DBGOUT_WND_F ( 2, _T("chainNum = %d"), chainNum );
		}

		if ( 0 < m_dispCount )
		{
//			DBGOUT_WND_F ( 0, _T ( "[%d]" ), -- m_dispCount );
		}

		//連鎖待機チェック
		m_pErase->CheckWaiting ();


		//おじゃま消去後処理
//		m_hamper.EraseAfter ();
	}

	//全体位置上昇
	void PanelManager::_PlayUp ()
	{
		//一つでもロック状態が存在したら上昇しない
		if ( m_panelArray->IsPanelLocked () ) { return; }

		//ピンチ状態
		if ( PLAYER_PROCEDURE_PINCH == m_playerProc )
		{
			//消去・落下により最上部にない場合
			if ( ! m_panelArray->IsTopPanel () )
			{
				m_pinchTimer = 0;	//タイマリセット
				m_playerProc = PLAYER_PROCEDURE_PLAY;	//通常状態
				m_operatePanel.StopVibration ();
			}
		}
		else
		{
			//位置補正計算
			m_upRevise -= m_upVel;
		}

		//１パネル分のとき
		if ( m_upRevise < -1.f * PANEL_UP_SIZE )
		{
			//最上位判定
#if	0
			//おじゃまが最上位置にある
			if ( m_hamper.GetValid () && m_hamper.GetMaxPanelPos () == 1 )
			{
				//１ゲーム終了処理
				m_gameStart = false;
				for ( UINT i = 0; i < STAGE_SIZE_Y; i++ )
				{
					for ( UINT j = 0; j < STAGE_SIZE_X; j++ )
					{
						UINT index = i * STAGE_SIZE_X + j;
						m_vpPanelOb[index]->SetGameOver ();
					}
				}
				m_procedure = GAME_PROCEDURE_OVER;
			}
#endif	//0
			//一旦ピンチ
			//一番上に接触(全体位置上昇が最大、かつ最上位置にパネルがある)
			if ( m_panelArray->IsTopPanel () )
			{
				//タイマ分岐
				if ( 0 == m_pinchTimer )
				{
					m_playerProc = PLAYER_PROCEDURE_PINCH;
					m_pinchTimer = 1;	//計測開始
					m_operatePanel.SetVibration ();
				}
				else if ( PINCH_TIME == m_pinchTimer )
				{
					//時間を過ぎたらオーバー
					_GameOver ();
				}
				++m_pinchTimer;
			}
			//通常時１マス上がる
			else
			{
				m_upRevise = 0;
				m_operatePanel.UpPanel ();
				m_pCursor->Up ();
			}
		}

		//位置補正反映
		m_operatePanel.SetUpRevise ( m_upRevise );
		m_pCursor->SetUpRevise ( m_upRevise );
	}

	void PanelManager::_Pinch ()
	{
	}

	void PanelManager::_GameOver ()
	{
		m_playerProc = PLAYER_PROCEDURE_OVER;

		//１ゲーム終了処理
		m_gameStart = false;
		m_operatePanel.StopVibration ();
		m_panelArray->SetOver ();
	}


}	//namespace GAME

