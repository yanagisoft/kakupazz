﻿//=================================================================================================
//
//	パネルオブジェクトの配列を操作する
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "OperatePanel.h"
#include "PanelArray.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	OperatePanel::OperatePanel ()
	{
	}

	OperatePanel::~OperatePanel ()
	{
	}

	//パネル配列設定
	void OperatePanel::Setp ( P_PanelArray pPnAry )
	{
		m_pPnAry = pPnAry;
	}

	//パネル即時交換
	void OperatePanel::SwapPanel ( POINT pt0, POINT pt1 )
	{
		SP_PnOb ob0 = m_pPnAry->GetpPnOb ( pt0.x, pt0.y );
		SP_PnOb ob1 = m_pPnAry->GetpPnOb ( pt1.x, pt1.y );
		P_PnState st0 = ob0->GetpPanelState ();
		P_PnState st1 = ob1->GetpPanelState ();
		ob0->SetpPanelState ( st1 );
		ob1->SetpPanelState ( st0 );
	}

	//パネル即時交換‗縦(対象と一つ下)
	void OperatePanel::SwapPanel_V ( LONG x, LONG y )
	{
		POINT pt0 = { x, y };
		POINT pt1 = { x, y + 1 };
		SwapPanel ( pt0, pt1 );
	}


	//位置補正反映
	void OperatePanel::SetUpRevise ( float upRevise )
	{
		SPVSP_PnOb pvpOb = m_pPnAry->GetpVP_PnOb ();
		for ( SP_PnOb pOb : (*pvpOb) )
		{
			pOb->SetUpRevise ( upRevise );
		}
	}

	//せりあげ
	void OperatePanel::UpPanel ()
	{
		//上から一つずつ交換し、最下段だけ上書き

		//最下段以外のすべてを走査
		for ( UINT y = 0; y < STAGE_SIZE_Y - 1; ++ y )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SwapPanel_V ( x, y );
			}
		}

		//最下段を上書初期化
		for ( UINT x = 0; x < STAGE_SIZE_X; ++x )
		{
			m_pPnAry->GetpPnOb ( x, STAGE_SIZE_Y - 1 )->SetStart ();
		}
	}

	//条件確認後、パネル左右交換移動
	void OperatePanel::MovePanel ( POINT pt )
	{
		//カーソル位置はその形状(-> □□)から必ず右隣はindex+1
		SP_PnOb ob0 = m_pPnAry->GetpPnOb ( pt.x, pt.y );	// ->■□ 対象位置
		SP_PnOb ob1 = m_pPnAry->GetpPnOb ( pt.x + 1, pt.y );	// ->□■ 右隣
		P_PnState st0 = ob0->GetpPanelState ();
		P_PnState st1 = ob1->GetpPanelState ();

		//どちらかが移動不可能であれば何もしない
		if ( ! ob0->CanMove () || ! ob1->CanMove () ) { return; }

		//両方とも空白のときは何もしない (片方空白の場合は移動可能)
		if ( ob0->IsBlank () && ob1->IsBlank () ) { return; }

		//対象パネルオブジェクトの状態を移動入替設定
		float fx = PANEL_SIZE_X;
		UINT SP = PANEL_MOVE_SPEED;
		ob0->StartMove ( st1, VEC2 ( +fx, 0 ), SP );
		ob1->StartMove ( st0, VEC2 ( -fx, 0 ), SP );
		
		//SE
		SOUND->Play ( SE_PANEL_MOVE );
	}

#if 0


	//パネル落下
	void OperatePanel::DropPanel ( LONG x, LONG y )
	{
		SP_PnOb ob0 = m_pPnAry->GetpPnOb ( x, y );	//上
		SP_PnOb ob1 = m_pPnAry->GetpPnOb ( x, y + 1 );	//下
		P_PnState st0 = ob0->GetpPanelState ();
		P_PnState st1 = ob1->GetpPanelState ();

		float fy = PANEL_SIZE_Y;
		UINT SP = PANEL_DROP_SPEED;
		ob0->StartDrop ( st1, VEC2 ( 0, +fy ), SP );
		ob1->StartDrop ( st0, VEC2 ( 0, -fy ), SP );
	}
#endif // 0

	//振動設定
	void OperatePanel::SetVibration ()
	{
		SPVSP_PnOb spvspPnOb = m_pPnAry->GetpVP_PnOb ();
		for ( SP_PnOb pOb : (*spvspPnOb) )
		{
			pOb->SetVibration ();
		}
	}

	//振動停止
	void OperatePanel::StopVibration ()
	{
		SPVSP_PnOb spvspPnOb = m_pPnAry->GetpVP_PnOb ();
		for ( SP_PnOb pOb : (*spvspPnOb) )
		{
			pOb->StopVibration ();
		}
	}

}	//namespace GAME

