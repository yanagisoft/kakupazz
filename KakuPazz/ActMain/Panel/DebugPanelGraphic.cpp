//=================================================================================================
//
// PanelGraphic ソースファイル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "DebugPanelGraphic.h"


//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	DebugPanelGraphic::DebugPanelGraphic ()
	{
		//テクスチャの設定
		AddTexture ( _T( "panel1.png" ) );
		AddTexture ( _T( "panel2.png" ) );
		AddTexture ( _T( "panel3.png" ) );
		AddTexture ( _T( "panel4.png" ) );
		AddTexture ( _T( "panel5.png" ) );
		AddTexture ( _T( "panel6.png" ) );

		AddTexture ( _T( "panel1_light.png" ) );
		AddTexture ( _T( "panel2_light.png" ) );
		AddTexture ( _T( "panel3_light.png" ) );
		AddTexture ( _T( "panel4_light.png" ) );
		AddTexture ( _T( "panel5_light.png" ) );
		AddTexture ( _T( "panel6_light.png" ) );

		AddTexture ( _T( "panel1_face.png" ) );
		AddTexture ( _T( "panel2_face.png" ) );
		AddTexture ( _T( "panel3_face.png" ) );
		AddTexture ( _T( "panel4_face.png" ) );
		AddTexture ( _T( "panel5_face.png" ) );
		AddTexture ( _T( "panel6_face.png" ) );

		AddTexture ( _T( "blank.png" ) );

		AddTexture ( _T( "hamperBlockC.png" ) );		//おじゃま中央
		AddTexture ( _T( "hamperBlockL.png" ) );		//おじゃま左
		AddTexture ( _T( "hamperBlockR.png" ) );		//おじゃま右
	}

	void DebugPanelGraphic::SetPnOb ( P_PanelArray panelArray )
	{
	}


}	//namespace GAME

