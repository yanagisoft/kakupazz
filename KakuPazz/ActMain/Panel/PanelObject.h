//=================================================================================================
//
// PanelObject ヘッダファイル
//	画像を表示する単位のオブジェクトにパネルの位置を記録するパラメータを追加
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "PanelCommon.h"
#include "PanelState.h"
#include "PanelGameState.h"
#include "../PanelSub/EfObject.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class PanelObject : public GameObject, public enable_shared_from_this < PanelObject >
	{
		P_PnState	m_state;		//状態(入替必須のフラグなど)
		P_EfOb		m_pEfObject;	//エフェクトオブジェクト

		//-------------------------------------------
		//パネルゲームステート(振舞を変えるもの)
		WP_PGS				mwp_pgs;	//操作用ポインタ
		//[各種状態]
		P_PGS_Default		m_pgs_Default;
		P_PGS_Moving		m_pgs_Moving;
		P_PGS_Drop			m_pgs_Drop;
		P_PGS_Getdown		m_pgs_Getdown;
		P_PGS_EraseWait		m_pgs_EraseWait;
		P_PGS_Erase			m_pgs_Erase;
		P_PGS_EraseHamper	m_pgs_EraseHamper;
		P_PGS_GameStart		m_pgs_GameStart;
		P_PGS_GameOver		m_pgs_GameOver;
		//-------------------------------------------

		//外部からの設定値
		const POINT	m_point;		//位置(パネル座標)
		VEC2		m_base;			//表示基準位置
		float		m_upRevise;		//せりあげ表示補正位置
		float		m_vibration;	//振動位置補正

	public:
		PanelObject () = delete;
		PanelObject ( LONG x, LONG y );		//ステージ上位置(x, y)
		PanelObject ( const PanelObject & rhs ) = delete;
		~PanelObject ();

		void Move ();

		//オブジェクト状態にthisを渡す
		void SetThis ();

		//----------------------------------------------------------------------
		//取得と設定

		//値のみコピー
		void Copy ( const PanelObject & rhs );

		//ステージ位置の取得
		const POINT GetPoint () const { return m_point; }

		//表示基準位置の設定
		void SetBase ( VEC2 & vec );

		//エフェクトオブジェクトのポインタ取得
		P_EfOb GetpEfObject () { return m_pEfObject; }

		//パネル状態
		void SetpPanelState ( P_PnState p ) { m_state = p; }
		P_PnState GetpPanelState () { return m_state; }
		const P_PnState GetpPanelState () const { return m_state; }

		//パネル種類
		PANEL_KIND GetKind () const { return m_state->GetKind (); }
		void SetKind ( PANEL_KIND kind ) { m_state->SetKind ( kind ); }
		void SetRandomKind () { m_state->SetRandomKind (); }
		//上と左と異なる種類を設定
		void SetOtherKind ( PANEL_KIND k0, PANEL_KIND k1 );

		//----------------------------------------------------------------------
		//操作
		void SetDefault ();		//基本
		void SetStart ();		//状態も初期化
		void SetUpRevise ( float upRevise ) { m_upRevise = upRevise; }		//全体上昇位置補正

		//----------------------------------------------------------------------
		//状態確認

		//空白
		void SetBlank () { SetKind ( PN_BLANK ); }
		bool IsBlank () const { return ( PN_BLANK == m_state->GetKind () ); }

		//ロック状態
		bool IsLocked () const { return mwp_pgs.lock()->IsLocked (); }

		//-------------------------------------------
		//移動
		//引数： 移動先の状態、移動距離、移動時間
		bool CanMove () const;
		void StartMove ( P_PnState pTarget, VEC2 distance, UINT frame );
		bool IsMove () const { return ( mwp_pgs.lock() == m_pgs_Moving ); }

		//-------------------------------------------
		//落下
		void SetDropStart ( bool b ) { m_state->SetDropStart ( b ); }
		bool GetDropStart () const { return m_state->GetDropStart ();  }
		void SetDropAble ( bool b ) { m_state->SetDropAble ( b ); }
		bool GetDropAble () const { return m_state->GetDropAble ();  }
		void SetDrop ( bool b ) { m_state->SetDrop ( b ); }
		bool GetDrop () const { return m_state->GetDrop ();  }
		void SetDropCheck ( bool b ) { m_state->SetDropCheck ( b ); }
		bool GetDropCheck () const { return m_state->GetDropCheck ();  }

		bool CanDrop () const;		//自身が落下可能か
		bool CanBeDropped () const;		//一つ上が落下可能か
		bool CanBeGetdown () const;		//一つ上が着地可能か

		//落下開始
		void StartDrop ( P_PnState pTarget, VEC2 distance );
		bool IsDrop () const { return ( mwp_pgs.lock() == m_pgs_Drop ); }
		void ContinueDrop ();
		void EndDrop ();

		//-------------------------------------------
		//着地
		void SetGetdown ( bool b ) { m_state->SetGetdown ( b ); }
		bool GetGetdown () const { return m_state->GetGetdown ();  }
		void StartGetdown ();
		bool IsGetdown () const { return ( mwp_pgs.lock() == m_pgs_Getdown); }

		//-------------------------------------------
		//消去
		bool CanErase () const;
		void SetEraseFlag ( bool b ) { m_state->SetErase ( b ); }
		bool GetEraseFlag () const { return m_state->GetErase (); }
		void EraseWait ();
		void StartErase ();
		bool IsErase () const { return ( mwp_pgs.lock() == m_pgs_Erase ); }
		void StartEffect ();		//エフェクトの開始

		//連鎖フラグ
		void SetChain ( bool b ) { m_state->SetChain ( b ); }
		bool GetChain () const { return m_state->GetChain (); }

		//-------------------------------------------
		//おじゃま
		void SetHamper ( HAMPER hmp );
		bool IsHamper () const { return m_state->IsHamper (); }
		void SetHamperID ( UINT id ) { m_state->SetHamperID ( id ); }
		UINT GetHamperID () const { return m_state->GetHamperID (); }
		void StartEraseHamper ();

		//-------------------------------------------
		//ゲームスタート
		void SetGameStart () { mwp_pgs = m_pgs_GameStart; }

		//ゲームオーバー
		void SetGameOver () { mwp_pgs = m_pgs_GameOver; }

		//-------------------------------------------

		//振動
		void SetVibration () { m_vibration = 1.f; }
		void StopVibration () { m_vibration = 0.f; }

	private:
		//内部関数
		void _Position ();	//位置計算
		void _SelectTexture ();	//テクスチャ選択
		void _SelectColor ();	//カラー選択
	};

	using SP_PnOb = shared_ptr < PanelObject >;
	using VSP_PnOb = vector < SP_PnOb >;
	using SPVSP_PnOb = shared_ptr < VSP_PnOb >;


}	//namespace GAME


