﻿//=================================================================================================
//
//	パネル管理
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/SceneCommon.h"
#include "PanelCommon.h"
#include "PanelArray.h"
#include "PanelGraphic.h"
#include "OperatePanel.h"
#include "../PanelSub/EfGraphic.h"
#include "../Cursor.h"
#include "../PanelSub/Erase.h"
#include "../PanelSub/Drop.h"
#include "../PanelSub/HamperManager.h"
#include "../DispChain.h"

#include "DebugPanelGraphic.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//パネル管理
	class PanelManager : public TASK_VEC
	{
		//パラメータ
		GAME_MODE			m_mode;
		PLAYER_NUM			m_playerNum;
		
		P_Game_Proc			m_procedure;		//ゲーム進行
		PLAYER_PROCEDURE	m_playerProc;		//プレイヤ進行

		//パネル
		P_PanelArray		m_panelArray;	//パネル配列
		P_PnGraphic			m_pnGrp;		//パネル表示
		P_EfGraphic			m_pEfGrp;		//エフェクト表示
		OperatePanel		m_operatePanel;		//パネル操作

		unique_ptr < Erase >	m_pErase;		//消去監理
		unique_ptr < Drop >		m_pDrop;		//落下監理
		P_HmpMngr			m_hmpMngr;		//おじゃまパネルマネージャ


		shared_ptr < Cursor >	m_pCursor;		//カーソル

		//test
		POINT m_tempPt;		//交換位置
		bool m_bGrap;		//掴み
		P_DbgPnGrp m_dbgPnGrp;
		SP_PnOb	m_tempPnOb;

		UINT m_dispCount;
		
		P_DispChain		m_dispChain;	//連鎖表示

		UINT		m_pinchTimer;	//ピンチタイマ
		float		m_vibelation;	//振動
		float		m_upRevise;		//全体位置上昇補正
		float		m_upVel;		//全体位置上昇速度
		bool		m_gameStart;	//ゲームスタート(初期化)フラグ

		//1人用、2人用(1p,2p)の基準位置
		VEC2					m_base;
		const static POINT		m_basePoint;
		const static POINT		m_basePointDouble[_PLAYER_NUM];

		//相互
		UINT			m_hamperNum;	//おじゃま行指定
		UINT			m_enemyChain;	//相手の連鎖
		UINT			m_myChain;		//自分の連鎖

	public:
		PanelManager () = delete;
		PanelManager ( P_Game_Proc procedure );
		PanelManager ( const PanelManager& rhs ) = delete;
		~PanelManager ();

		void ParamInit ();
		void Init();
		void Move();

		//ゲームモード設定
		void SetMode ( GAME_MODE mode ) { m_mode = mode; }
		void SetPlayerNum ( PLAYER_NUM playerNUm ) { m_playerNum = playerNUm; }

		//ゲーム進行
		PLAYER_PROCEDURE GetProcedure () const { return m_playerProc; }

		//対戦相手通信
		void SetHamper ( LONG chain ) { m_enemyChain = chain; }		//おじゃまの設定
		LONG GetChain () { return m_myChain; }		//連鎖の取得

	private:
		//----------
		//	内部関数
		//----------

		//ゲーム初期化
		void _GameInit ();

		//位置
		void _SetSingleBase ();
		void _SetDoubleBase ();
		
		//Move場合分け
		void _Start ();
		void _Play ();
		void _Over ();

		//Play場合分け
		void _PlayInput ();	//入力
		void _PlayEnemy ();	//相手通信
		void _PlayDrop ();	//落下
		void _PlayErase ();	//消去
		void _PlayUp ();	//全体位置上昇

		void _Pinch ();
		void _GameOver ();
	};

	using P_PanelManager = shared_ptr < PanelManager >;


}	//namespace GAME

