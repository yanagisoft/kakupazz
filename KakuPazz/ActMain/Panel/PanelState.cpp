﻿//=================================================================================================
//
//	パネル状態
//		入替時に保持するパラメータ
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "PanelState.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	PanelState::PanelState()
		: m_kind ( PN_BLANK )
		, m_dropStart ( false ), m_dropAble ( false ), m_drop ( false )
		, m_dropCheck ( false )
		, m_getdown ( false )
		, m_erase ( false ), m_chain ( false )
		, m_hamper ( HAMPER_NUM ), m_hamperID ( 0 )
	{
		Start();
	}

	PanelState::PanelState ( const PanelState & rhs )
	{
		m_kind = rhs.m_kind;
		m_dropStart = rhs.m_dropStart;
		m_dropAble = rhs.m_dropAble;
		m_drop = rhs.m_drop;
		m_dropCheck = rhs.m_dropCheck;
		m_getdown = rhs.m_getdown;
		m_erase = rhs.m_erase;
		m_chain = rhs.m_chain;
		m_hamper = rhs.m_hamper;
		m_hamperID = rhs.m_hamperID;
	}

	PanelState::~PanelState()
	{
	}

	//ランダム種類指定
	void PanelState::SetRandomKind()
	{
		switch ( rand() % PN_BLANK )
		{
		case GREEN:		m_kind = GREEN;  break;
		case ORENGE:	m_kind = ORENGE; break;
		case BLUE:		m_kind = BLUE;	 break;
		case RED:		m_kind = RED;	 break;
		case WATER:		m_kind = WATER;	 break;
		case PURPLE:	m_kind = PURPLE; break;
		}
	}

	//上と左と異なるランダム種類指定
	void PanelState::SetOtherKind ( PANEL_KIND k0, PANEL_KIND k1 )
	{
		//初期値はランダム
		SetRandomKind ();

		//上,左とも空白なら終了
		if ( PN_BLANK == k0 && PN_BLANK == k1 ) { return; }

		//初期値からパネル種類の数だけ回す
		int i = (int) m_kind;
		for ( int d = 0; d < PN_BLANK; ++ d )
		{
			int k = ( i + d ) % PN_BLANK;	//端数
			bool b0 = ( k0 != (PANEL_KIND)k );
			bool b1 = ( k1 != (PANEL_KIND)k );

			//２つとも同じでない種類
			if ( b0 && b1 )
			{
				m_kind = (PANEL_KIND)k;
				break;
			}
		}
	}

	//種類も含めた初期化
	void PanelState::Start()
	{
		SetRandomKind ();
		m_hamper = HAMPER_NUM;
		m_hamperID = 0;
		SetDefault ();
	}

	//種類以外の初期化
	void PanelState::SetDefault ()
	{
		m_dropStart = false;
		m_dropAble = false;
		m_drop = false;
		m_dropCheck = false;
		m_getdown = false;
		m_erase = false;
		m_chain = false;
	}



}	//namespace GAME

