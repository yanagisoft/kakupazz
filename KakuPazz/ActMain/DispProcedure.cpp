﻿//=================================================================================================
//
// ゲーム進行表示
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "DispProcedure.h"
#include "../GameMain/Sound.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	enum DISP_PROCEDURE_PARAM
	{
		DP_COUNTDOWN_X = 290,
		DP_COUNTDOWN_Y = 180,
		DP_GAMEOVER_X = 200,
		DP_GAMEOVER_Y = 180,
		DP_WIN_X = 200,
		DP_WIN_Y = 180,
		DP_COUNT_TIME = 60,
		DP_DISP_TIME = 240,
	};

	DispProcedure::DispProcedure ( P_Game_Proc procedure )
		: m_procedure ( procedure )
	{
		//開始カウントダウン
		m_countDown = make_shared < GrpAcv > ();
		m_countDown->AddTexture ( _T("digit1_0.png") );
		m_countDown->AddTexture ( _T("digit1_1.png") );
		m_countDown->AddTexture ( _T("digit1_2.png") );
		m_countDown->AddTexture ( _T("digit1_3.png") );
		m_countDown->SetScalingCenter ( VEC2 ( 32, 32 ) );
		m_scale = 2.f;
		m_countDown->SetPos ( DP_COUNTDOWN_X , DP_COUNTDOWN_Y );
		m_countDown->SetIndexTexture ( 3 );
		m_countDown->SetZ ( 0.2f );
		m_countDown->SetValid ( false );
		AddTask ( m_countDown );

		//ゲームオーバー
		m_pGameOver = make_shared < GrpAcv > ();
		m_pGameOver->AddTexture ( _T("gameover.png") );
		m_pGameOver->SetPos ( DP_GAMEOVER_X , DP_GAMEOVER_Y );
		m_pGameOver->SetValid ( false );
		m_pGameOver->SetZ ( 0.2f );
		AddTask ( m_pGameOver );

		//1p勝
		m_grp1pWin = make_shared < GrpAcv > ();
		m_grp1pWin->AddTexture ( _T("1pWin.png") );
		m_grp1pWin->SetPos ( DP_WIN_X , DP_WIN_Y );
		m_grp1pWin->SetValid ( false );
		m_grp1pWin->SetZ ( 0.2f );
		AddTask ( m_grp1pWin );

		//2p勝
		m_grp2pWin = make_shared < GrpAcv > ();
		m_grp2pWin->AddTexture ( _T("2pWin.png") );
		m_grp2pWin->SetPos ( DP_WIN_X , DP_WIN_Y );
		m_grp2pWin->SetValid ( false );
		m_grp2pWin->SetZ ( 0.2f );
		AddTask ( m_grp2pWin );

		//フェード
		m_fade = make_shared < Fade > ();
		m_fade->SetZ ( 0.1f );
		m_fade->SetDarkIn ( 16 );
		AddTask ( m_fade );
	}

	DispProcedure::~DispProcedure ()
	{
	}

	void DispProcedure::Init()
	{
		m_timer = 0;
		m_index = 0;
		m_vel.x = 0;
		m_vel.y = 1;

//		* m_procedure = GAME_PROCEDURE_START;

		TASK_VEC::Init();
	}

	void DispProcedure::Move()
	{
		switch ( * m_procedure )
		{
		case GAME_PROCEDURE_START: Start (); break;
		case GAME_PROCEDURE_COUNT: Count (); break;
		case GAME_PROCEDURE_PLAY: Play (); break;
		case GAME_PROCEDURE_OVER: Over (); break;
		case GAME_PROCEDURE_1P_WIN: Over (); break;
		case GAME_PROCEDURE_2P_WIN: Over (); break;
		default: break;
		}
		TASK_VEC::Move();
	}

	void DispProcedure::Start ()
	{
		if ( ! m_fade->IsActive () )
		{
			* m_procedure = GAME_PROCEDURE_COUNT;
			SOUND->Play ( SE_COUNT );
			m_countDown->SetValid ( true );
		}
	}

	void DispProcedure::Count ()
	{
		if ( m_timer++ >= DP_COUNT_TIME ) 
		{
			if ( ++m_index > 2 )
			{
				SOUND->Play ( SE_START );
				m_index = 0;
				m_countDown->SetValid ( false );
				* m_procedure = GAME_PROCEDURE_PLAY;
			}
			else
			{
				SOUND->Play ( SE_COUNT );
			}
			m_countDown->SetIndexTexture ( 3 - m_index );
			m_timer = 0;
			m_scale = 2.f;
		}
		m_scale -= 0.01f;
		m_countDown->SetScaling ( m_scale, m_scale );
	}

	void DispProcedure::Play ()
	{
	}

	void DispProcedure::Over ()
	{
		//表示時間
		if ( m_timer == 0 )
		{
			m_pGameOver->SetValid ( true );
		}
		else if ( m_timer >= DP_DISP_TIME )
		{
			m_pGameOver->SetValid ( false );
			*m_procedure = GAME_PROCEDURE_TITLE;
		}
		m_timer++;

		//表示位置
		VEC2 pos = m_pGameOver->GetPos();
		if ( pos.y > 150 && m_vel.y > 0 ) { m_vel.y = -1; }
		if ( pos.y < 100 && m_vel.y < 0 ) { m_vel.y = 1; }
		m_pGameOver->SetPos ( pos + m_vel );
	}

	void DispProcedure::Win1p ()
	{
	}

	void DispProcedure::Win2p ()
	{
	}


}	//namespace GAME

