﻿//=================================================================================================
//
// おじゃまブロック
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
//#include "StdAfx.h"
#include "HamperBlock.h"
#include "PanelManager.h"


namespace GAME
{

//-------------------------------------------------------------------------------------------------
	HamperBlock::HamperBlock ()
	{
		m_bottom = 0;
		m_erasing = false;
	}

	HamperBlock::~HamperBlock ()
	{
		m_list.Clear ();
	}

	//パネル状態セット
	void HamperBlock::SetPanelState ( PanelState* stateArray[], UINT num )
	{
		LONG bottom = 0;
		for ( UINT i = 0; i < num; i++ )
		{
			LONG y = stateArray[i]->GetPoint ().y;
			if ( bottom < y ) { bottom = y; }

			stateArray[i]->SetHamper ( true );
			m_list.Add ( stateArray[i] );
		}
		m_bottom = bottom;	//最下段の記録
	}

	//最下段の更新
	void HamperBlock::RefreshBottom ()
	{
		LONG bottom = 0;
		TList < PanelState* >::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			LONG y = (*it)->GetPoint ().y;
			if ( bottom < y ) { bottom = y; }
		}
		m_bottom = bottom;	//最下段の記録
	}

	//落下チェック
	void HamperBlock::CheckDrop ()
	{
		RefreshBottom ();

		bool drop = true;	//初期値は落下可能

		TList < PanelState* >::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			//ひとつ下が落下可能かどうか
			POINT pt = (*it)->GetPoint();
			if ( pt.y == m_bottom )		//最下段のみ判定
			{
				if ( ! m_pPanelManager->CanDropColumn ( pt ) )
				{
					drop = false;	//ひとつでも落下不可ならすべて落下不可
					break;
				}
			}
		}

		//すべてに落下フラグ付け
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			(*it)->SetDrop ( drop );
		}
	}


	//消去
	bool HamperBlock::Erase ( PanelState* state )
	{
		if ( m_list.IsData ( state ) )
		{
			RefreshBottom ();

			TList < PanelState* >::iterator it = m_list.begin ();
			while ( it != m_list.end () )
			{
				if ( (*it)->GetPoint ().y == m_bottom )		//最下段のみ
				{
					(*it)->SetRandomKind ();
					(*it)->SetHamper ( false );
					it = m_list.Erase ( it );	//次のイテレータが返る
				}
				else
				{
					++it;	//消去しないときは手動で一つ進める
				}
			}
			//すべて消えたときのみtrueを返す
			if ( m_list.size () == 0 )
			{
				m_list.Clear ();
				return true;
			}
		}
		return false;
	}


	//消去チェック
	bool HamperBlock::EraseCheck ( POINT& pt )
	{
		RefreshBottom ();

		//消去対象の高さを確認
		if ( m_bottom + 1 >= STAGE_SIZE_Y ) { return false; }

		//消去するパネルの高さが同じだった場合
		if ( pt.y == m_bottom + 1 )
		{
			//一つでも対象が当てはまれば、そのおじゃまブロックは消去開始
			m_erasing = true;

			//ブロック内のおじゃまパネルに消去フラグ付け
			TList < PanelState* >::iterator it;
			POINT ptErase;
			for ( it = m_list.begin(); it != m_list.end(); )
			{
				ptErase = (*it)->GetPoint();
				(*m_eraseFlag)[ptErase.y][ptErase.x] = true;

				//最下段はリストから外す
				if ( m_bottom == ptErase.y )
				{
					//パネルの初期化
					(*it)->SetHamper ( false );
					(*it)->SetToNormal ( true );
					(*it)->SetRandomKind ();
					it = m_list.Erase ( it );
				}
				else
				{
					++it;
				}
			}
			return true;
		}
		return false;
	}

	//消去後処理
	void HamperBlock::EraseAfter ()
	{
		m_erasing = false;	//消去セットを終了
	}


//-------------------------------------------------------------------------------------------------
	HamperManager::HamperManager ()
	{
	}

	HamperManager::~HamperManager ()
	{
		//おじゃまブロックの解放
		TList < HamperBlock* > ::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			delete (*it);
		}

		//リストの解放
		m_list.Clear ();
	}


	//おじゃま生成
	//引数 PanelState* stateArray	：パネル状態の配列
	//引数 UINT num					：受け取るパネル状態の個数
	void HamperManager::Generate( PanelState* stateArray[], UINT num )
	{
		//おじゃまブロックの生成
		HamperBlock* hamperBlock = new HamperBlock;
		hamperBlock->SetPanelManager ( m_pPanelManager );	//パネルマネージャを設定
		hamperBlock->SetEraseFlag ( m_eraseFlag );			//消去フラグを設定
		hamperBlock->SetPanelState ( stateArray, num );		//パネル状態を指定

		//リストに登録
		m_list.Add ( hamperBlock );
	}

	//落下チェック
	void HamperManager::CheckDrop ()
	{
		//すべてのおじゃまブロックに対して
		TList < HamperBlock* > ::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			(*it)->CheckDrop ();
		}
	}

	//消去
	void HamperManager::Erase ( PanelState* state )
	{
		//おじゃまでなかったら何もしない
		if ( ! state->GetHamper() ) { return; }

		//すべてのおじゃまブロックに対して
		TList < HamperBlock* > ::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			//対象パネル状態を各ブロックで探して消去
			if ( (*it)->Erase ( state ) )	//パネル状態を消去したブロックはtrueを返す
			{
				delete (*it);			//該当ブロック自体を解放
				m_list.Erase ( it );	//該当ブロックをリンクから消去
				break;	//消去したらループを終了するのでwhileでなくてよい
			}
		}
	}

	//消去チェック
	void HamperManager::EraseCheck ( POINT& pt )
	{
		//すべてのおじゃまブロックに対して
		TList < HamperBlock* > ::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); )
		{
			//おじゃまブロックが消去中でなければ
			if ( ! (*it)->IsErasing () )
			{
				//消去チェック
				if ( (*it)->EraseCheck ( pt ) )
				{
					//そのおじゃまブロックが無くなったとき
					if ( (*it)->size () == 0 )
					{
						delete (*it);
						it = m_list.Erase ( it );	//リストから外す
						continue;
					}
				}
			}
			++it;
		}
	}

	//消去後処理
	void HamperManager::EraseAfter ()
	{
		//すべてのおじゃまブロックに対して
		TList < HamperBlock* > ::iterator it;
		for ( it = m_list.begin(); it != m_list.end(); ++it )
		{
			(*it)->EraseAfter ();
		}
	}


}	//namespace GAME

