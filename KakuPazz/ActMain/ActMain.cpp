﻿//=================================================================================================
//
// アクト　メイン　ヘッダ
//		ゲームにおける動作の主要部分
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "ActMain.h"

//Scene遷移先
#include "../Title/Title.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	ActMain::ActMain ()
		: m_frame ( 0 )
	{
		//ネットワーク関係の初期化
		Network::Create ();
		NetInput::Create ();
		Server::Create ();
		Client::Create ();
	}

	ActMain::~ActMain ()
	{
	}

	void ActMain::ParamInit ()
	{
		//入力にモードを設定
		NET_INPUT->SetMode ( GetpParam ()->GetMode() );

		//スイッチの生成
		m_networkSwitch = make_shared < NetworkSwitch > ();
		m_networkSwitch->ParamInit ( GetpParam () );
		AddTask ( m_networkSwitch );
	}

	void ActMain::Move ()
	{
		//ゲームキーを記録(ローカルでは互いに取得、ネットワークでは送信と受信)
		NET_INPUT->Store ( m_frame );

		Scene::Move ();

		//フレームカウント
		++ m_frame;
	}


	//状態遷移
	P_GameScene ActMain::Transit ()
	{ 
		//ESCでタイトルに移行
		if ( ::GetAsyncKeyState ( VK_ESCAPE ) & 0x0001 ) 
		{ 
			SOUND->Stop ( BGM_SETTINGS );
			SOUND->Stop ( BGM_MAIN );
			return make_shared < Title > ();
		}

		//ネットワークモード時
		//ボタン入力によりタイトルに移行
		bool b1 = ( NETWORK_SERVER_1P == GetpParam ()->GetMode () );
		bool b2 = ( NETWORK_CLIENT_2P == GetpParam ()->GetMode () );
		if ( b1 || b2 )
		{
			if ( m_networkSwitch->Title () )
			{
				SOUND->Stop ( BGM_SETTINGS );
				SOUND->Stop ( BGM_MAIN );
				return make_shared < Title > ();
			}
		}

		//ゲーム内の状態遷移によりタイトルに移行
		if ( GAME_PROCEDURE_TITLE == m_networkSwitch->GetProcedure () )
		{
			SOUND->Stop ( BGM_SETTINGS );
			SOUND->Stop ( BGM_MAIN );
			return make_shared < Title > ();
		}

		//通常時はThisを返して続行
		return shared_from_this ();
	}

}	//namespace GAME

