﻿//=================================================================================================
//
// キャラ ヘッダ
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/GameCommon.h"
#include "../GameMain/SceneCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	class Chara : public TASK_VEC
	{
		PLAYER_NUM	m_playerNum;
		CHARA_NAME	m_name;
		int			m_life;
		int			attack;
		enum CHARA_STATE
		{
			CHARA_STATE_STAND,
			CHARA_STATE_ATTACK,
			CHARA_STATE_DAMAGE,
			CHARA_STATE_WIN,
			CHARA_STATE_LOSE,
		};
		CHARA_STATE	m_state;

		P_GrpAcv	m_grpMain;
		UINT		m_timer;
		UINT		m_index;

	public:
		Chara ();
		Chara ( const Chara & rhs ) = delete;	//コピー禁止
		~Chara ();

		void ParamInit ();
		void Move ();

		void SetPlayerNum ( PLAYER_NUM playerNum ) { m_playerNum = playerNum; }
		void SetName ( CHARA_NAME name ) { m_name = name; }
		void SetPos ( VEC2 v ) { m_grpMain->SetPos ( v ); }

		void SetPosSingle ();
		void SetPosDouble ();
	};

	using P_Chara = shared_ptr < Chara >;


}	//namespace GAME

