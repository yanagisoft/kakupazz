﻿//=================================================================================================
//
// Stage ヘッダファイル
//		アクトにおける「場」を定義する
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/SceneCommon.h"
#include "Panel/PanelCommon.h"
#include "DispProcedure.h"
#include "Panel/PanelManager.h"
#include "DispSummary.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//========================================
	//Stage共通
	//========================================
	class Stage	: public GameTaskVector
	{
		P_Param				m_param;		//ゲームパラメータ
		P_Game_Proc			m_procedure;	//ゲーム進行状態
		P_DispProcedure		m_dispProc;		//ゲーム進行表示
		P_GrpAcv			m_pBg;			//背景
		P_GrpAcv			m_logo;			//ロゴ

	protected:
		bool				m_start;		//スタートフラグ

	public:
		Stage ();
		Stage ( const Stage & rhs ) = delete;
		~Stage ();

		//パラメータを用いた初期化
		virtual void ParamInit ( P_Param p ) { m_param = p; }

		//ゲーム進行
		GAME_PROCEDURE GetProcedure () const { return *m_procedure; }
		P_Game_Proc GetpProcedure () const { return m_procedure; }
		void SetProcedure ( GAME_PROCEDURE proc ) { *m_procedure = proc; }

		//スタート
		void SetStart ( bool b ) { m_start = b; }
		bool GetStart () const { return m_start; }
	};

//-----------------------------------------------------------------------------
	//========================================
	//シングル(1p)用
	//========================================
	class Stage1p	: public Stage
	{
		P_GrpAcv	m_pPanelBg;			//パネル背景
		P_GrpAcv	m_pBgBottom;		//パネル下部隠し1p用
		P_PanelManager 	m_panelManager;		//パネル管理
		P_DispSummary		m_dispSummary;	//総合表示

	public:
		Stage1p ();
		Stage1p ( const Stage1p & rhs ) = delete;
		~Stage1p ();

		void ParamInit ( P_Param p );
		void Move ();
		void Play ();
	};


//-----------------------------------------------------------------------------


	//========================================
	//ダブル(2p)用
	//========================================
	class Stage2p : public Stage
	{
		P_GrpAcv	m_panelBg1p;		//パネル背景
		P_GrpAcv	m_panelBg2p;		//パネル背景
		P_GrpAcv	m_bgBottomDOUBLE;		//パネル下部隠しDOUBLE用

		P_PanelManager		m_panelManager1p;		//パネル管理
		P_PanelManager		m_panelManager2p;		//パネル管理

		P_DispSummary		m_dispSummary1p;	//キャラ総合表示1p
		P_DispSummary		m_dispSummary2p;	//キャラ総合表示2p

	public:
		Stage2p ();
		Stage2p ( const Stage2p & rhs ) = delete;
		~Stage2p ();

		void ParamInit ( P_Param p );
		void Move ();
		void Play ();
	};


}	//namespace GAME

