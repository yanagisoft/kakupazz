﻿//=================================================================================================
//
// おじゃまブロック
//
//=================================================================================================


#ifndef		__HAMPER_BLOCK_HEADER_INCLUDE__
#define		__HAMPER_BLOCK_HEADER_INCLUDE__


//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "../GameMain/Game.h"

#include "PanelCommon.h"
//#include "PanelArray.h"
#include "PanelState.h"
//#include "TemplateList.h"


//おじゃまの定義

//パネルマネージャの仕様を変更する
	//ステージサイズ
	//パネル表示位置関連
	//パネル状態
	//落下チェック
	//落下実行
	//通常パネルとおじゃまパネルの処理分け

//Hamperクラスの移植


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//クラス宣言
	class PanelManager;


	//ブロック：複数パネルによるおじゃまの単位
	class HamperBlock
	{
		TList < PanelState* >		m_list;
		const PanelManager*			m_pPanelManager;
		LONG						m_bottom;			//最下段
		PanelStageArray < bool > *		m_eraseFlag;		//消去フラグポインタ
		bool						m_erasing;
		
	public:
		HamperBlock ();
		~HamperBlock ();

		//パネルマネージャ参照
		void SetPanelManager ( const PanelManager* p ) { m_pPanelManager = p; }

		//消去フラグポインタ
		void SetEraseFlag ( PanelStageArray < bool > * p ) { m_eraseFlag = p; }

		//パネル状態セット
		void SetPanelState ( PanelState* stateArray[], UINT num );

		//個数
		UINT size () const { return m_list.size (); }

		//最下段の更新
		void RefreshBottom ();

		//落下チェック
		void CheckDrop ();
		
		//消去
		//戻値：該当するパネル状態を消去したらtrue, そのパネルが存在しなければfalse
		bool Erase ( PanelState* state );

		//消去中かどうか
		bool IsErasing () const { return m_erasing; }

		//消去チェック
		bool EraseCheck ( POINT& pt );

		//消去後処理
		void EraseAfter ();
	};


	//おじゃまの監理
	class HamperManager
	{
		TList < HamperBlock* >		m_list;
		HamperBlock*				m_hamperBlock;
		const PanelManager*			m_pPanelManager;
		PanelStageArray < bool > *		m_eraseFlag;		//消去フラグポインタ
		
	public:
		HamperManager ();
		~HamperManager ();

		//パネルマネージャ参照
		void SetPanelManager ( const PanelManager* p ) { m_pPanelManager = p; }

		//消去フラグポインタ
		void SetEraseFlag ( PanelStageArray < bool > * p ) { m_eraseFlag = p; }

		//おじゃま生成
		//引数 PanelState* stateArray	：パネル状態の配列
		//引数 UINT num					：受け取るパネル状態の個数
		void Generate ( PanelState* stateArray[], UINT num );

		//落下チェック
		void CheckDrop ();

		//消去
		void Erase ( PanelState* state );

		//消去チェック
		void EraseCheck ( POINT& pt );

		//消去後処理
		void EraseAfter ();

		//個数
		UINT size () const { return m_list.size (); }
	};


}	//namespace GAME


#endif		//__HAMPER_BLOCK_HEADER_INCLUDE__

