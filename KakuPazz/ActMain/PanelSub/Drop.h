﻿//=================================================================================================
//
// 落下監理
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"
#include "HamperManager.h"
#include "DropSequence.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//-------------------------------------------------------------------------------------------------
	//落下監理
	class Drop
	{
		P_PanelArray	m_pPnAry;		//パネルポインタの配列参照
		P_HmpMngr		m_hmpMngr;		//おじゃま監理
		list < P_DropSeq >	m_listDrop;	//落下手順ポインタのリスト

	public:
		Drop () = delete;
		Drop ( P_PanelArray pPanelArray );
		Drop ( const Drop & rhs ) = delete;
		~Drop ();

		void SetHmpMngr ( P_HmpMngr p ) { m_hmpMngr = p; }
		void Do ();

	private:	//内部関数
		//落下判定
		void _CheckDropStart ();
		void _CheckDropAble ();
		void _CheckDrop ();
		void _CheckDropRotate ();

		//落下対象の位置から一連の落下個数
		UINT nDrop ( LONG x, LONG y );

		void _DoDrop ();	//落下実行
	};


}	//namespace GAME

