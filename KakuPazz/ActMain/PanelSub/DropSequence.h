﻿//=================================================================================================
//
// 落下順序処理
//	同時タイミングに落下確定したパネルを記録し、
//	その中で順次落下していく
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"
#include "HamperManager.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//パネル縦一連
	struct PANEL_SPAN
	{
		POINT	pt;		//ステージ位置
		UINT	n;		//連続した個数
	public:
		PANEL_SPAN () { pt.x = 0; pt.y = 0; n = 0; }
	};
	using V_PN_SPAN = vector < PANEL_SPAN >;

	//落下手順監理
	class DropSequence
	{
		P_PanelArray	m_pPnAry;	//パネル配列ポインタ
		P_HmpMngr		m_hmpMngr;		//おじゃま監理

		V_PN_SPAN		m_vDropPt;		//落下場所記憶の配列(最大値で確保)
		bool			m_end;			//終了フラグ
		UINT			m_dropTimer;	//落下時間カウント
		UINT			m_dropSpan;		//落下マスカウント

	public:
		DropSequence () = delete;
		DropSequence ( P_PanelArray pPanelArray );
		DropSequence ( const DropSequence & rhs ) = delete;
		~DropSequence ();

		void SetHmpMngr ( P_HmpMngr p ) { m_hmpMngr = p; }

		//落下位置配列を設定
		void SetrvDrop ( UINT n, V_PN_SPAN & rvDropPt );

		//通常落下手順
		void Do ();
		bool IsEnd () { return m_end; }

	private:
		SP_PnOb PsToPob ( PANEL_SPAN ps );

		//落下一連開始
		void RotatePanel ();

		//落下一連の入替対象を設定
		void SetDropTarget ( PANEL_SPAN ps );
		
		//着地
		void _CheckGetdown ();

		//落下一連続行
		void ContinueDropPanel ( PANEL_SPAN ps );
		
		//落下一連着地
		void GetdownPanel ( PANEL_SPAN ps );
	};

	using P_DropSeq = unique_ptr < DropSequence >;


}	//namespace GAME

