﻿//=================================================================================================
//
// エフェクト
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "EfObject.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	EfObject::EfObject ( LONG x, LONG y )
		: m_base ( VEC2 ( 0, 0 ) )
	{
		m_pt.x = x;
		m_pt.y = y;
		m_timer = 0;
		m_index = 0;
		SetValid ( false );
	}

	EfObject::~EfObject ()
	{
	}

	void EfObject::Move()
	{
		//稼動開始
		if ( ! GetValid() ) { return; }

		if ( m_timer ++ > 4 )	//インターバルタイム
		{
			if ( ++ m_index > 5 )	//アニメーション終了時
			{
				m_index = 0;
				SetValid ( false );
			}
			SetIndexTexture ( m_index );
			m_timer = 0;
		}
		GameObject::Move ();
	}

	void EfObject::Position ( VEC2 moving, float upRevise )
	{
		//エフェクトの位置計算
		VEC2 pos = m_base;
		pos.x += (float)(m_pt.x * PANEL_SIZE_X) - EF_SIZE_X;
		pos.y += (float)(m_pt.y * PANEL_SIZE_Y) - EF_SIZE_Y + upRevise;
		pos += moving;
		SetPos ( pos );
	}


}	//namespace GAME

