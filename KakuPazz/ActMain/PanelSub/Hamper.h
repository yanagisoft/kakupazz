//=================================================================================================
//
// おじゃまパネル
//		・位置をブロックごとに記録
//		・テクスチャの指定	
//		・落下時ブロック判定
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//おじゃまパネル
	struct HAMPER_PANEL
	{
		POINT pt;	//位置
		bool btm;	//ブロックの底かどうか
	};

	//おじゃまパネル１ブロック
	using V_HMP = vector < HAMPER_PANEL >;

	//おじゃまパネルブロック
	class HamperBlock
	{
		P_PanelArray	m_pPnAry;
		V_HMP			mv_hmp;

	public:
		HamperBlock () = delete;
		HamperBlock ( P_PanelArray pPnAry );
		HamperBlock ( const HamperBlock & rhs );	//コピー可能
		~HamperBlock ();

		//IDを取得
		UINT GetID ();

		//落下
		void CheckDropStart ();
		void CheckDropAble ();
		void CheckDrop ();
		void CheckGetdown ();
		void push ( HAMPER_PANEL hmp );

		//消去
		void CheckErase ( LONG x, LONG y );

	private:
		//位置を１つ下げる
		void _Down ();

		void _SetAllDropStart ( bool b );
		void _SetAllDropAble ( bool b );
		void _SetAllDrop ( bool b );
		void _SetAllGetDown ( bool b );
	};


}	//namespace GAME

