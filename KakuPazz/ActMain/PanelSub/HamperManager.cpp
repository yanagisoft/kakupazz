//=================================================================================================
//
// おじゃまパネル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "HamperManager.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	HamperManager::HamperManager ()
		: m_id ( 0 )
	{
	}

	HamperManager::~HamperManager ()
	{
	}

	//パネル配列の記録
	void HamperManager::SetpPnAry ( P_PanelArray pPnAry )
	{
		m_pPnAry = pPnAry;
	}

	//生成
	void HamperManager::GenerateHamper ( UINT size )
	{
		//大きさが０のときは何もしない
		if ( 0 == size ) { return; }

		//ブロック生成
		HamperBlock hmpBlk ( m_pPnAry );
		SPVSP_PnOb pvpPnOb = m_pPnAry->GetpVP_PnOb ();
		for ( auto pOb : (*pvpPnOb) )
		{
			POINT pt = pOb->GetPoint ();
			UINT y = (UINT)pt.y;
			//定位置に生成
			if ( 12 - size < y && y <= 12 )
			{
				HAMPER_PANEL hmp = { pt, false };
				if ( 12 == y ) { hmp.btm = true; }
				hmpBlk.push ( hmp );
				pOb->SetHamper ( HAMPER_C );	//おじゃま種類
				pOb->SetHamperID ( m_id );	//IDを記録
			}
		}

		//IDをインクリメント
		++ m_id;

		ml_hmpBlk.push_back ( hmpBlk );
	}

	//IDから１ブロックが落下するかどうかチェックする
	void HamperManager::CheckDropFromID ( UINT id )
	{
		//おじゃまパネルブロックの各連でチェック
		for ( HamperBlock hmp : ml_hmpBlk )
		{
			//必ずIDを持つブロックが存在する
			if ( hmp.GetID () == id )
			{
				hmp.CheckDropStart ();
				return;
			}
		}
		assert ( false );
	}

	//落下起点かフラグ付けをする
	void HamperManager::CheckDropStart ()
	{
		//おじゃまパネルブロックの各連でチェック
		for ( HamperBlock hmp : ml_hmpBlk )
		{
			hmp.CheckDropStart ();
		}
	}

	//落下可能かフラグ付けをする
	void HamperManager::CheckDropAble ()
	{
		//おじゃまパネルブロックの各連でチェック
		for ( HamperBlock hmp : ml_hmpBlk )
		{
			hmp.CheckDropAble ();
		}
	}

	//落下状態かフラグ付けをする
	void HamperManager::CheckDrop ()
	{
		//おじゃまパネルブロックの各連でチェック
		for ( HamperBlock hmp : ml_hmpBlk )
		{
			hmp.CheckDrop ();
		}
	}


	//----------------------------------------------------
	void HamperManager::CheckGetdownFromID ( UINT id )
	{
		//おじゃまパネルブロックの各連でチェック 
		auto it = begin ( ml_hmpBlk );
		while ( it != end ( ml_hmpBlk ) )
		{
			//必ずIDを持つブロックが存在する
			if ( it->GetID () == id )
			{
				it->CheckGetdown ();
				return;
			}
		}
		assert ( false );
	}

	//着地可能かフラグ付けをする
	void HamperManager::CheckGetdown ()
	{
		//おじゃまパネルブロックの各連でチェック
		for ( HamperBlock hmp : ml_hmpBlk )
		{
			hmp.CheckGetdown ();
		}
	}

	void HamperManager::CheckErase ( LONG x, LONG y )
	{
		auto it = begin ( ml_hmpBlk );
		while ( it != end ( ml_hmpBlk ) )
		{
			it->CheckErase ( x, y );
			++ it;
		}
	}



}	//namespace GAME

