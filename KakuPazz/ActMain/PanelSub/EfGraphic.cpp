﻿//=================================================================================================
//
// エフェクト
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "EfGraphic.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	EfGraphic::EfGraphic ( P_PanelArray & panelArray )
	{
		//テクスチャの設定
		AddTexture ( _T("erace_ef0.png") );
		AddTexture ( _T("erace_ef1.png") );
		AddTexture ( _T("erace_ef2.png") );
		AddTexture ( _T("erace_ef3.png") );
		AddTexture ( _T("erace_ef4.png") );
		AddTexture ( _T("erace_ef5.png") );

		SetPanelArray ( panelArray );
		
	}
	EfGraphic::~EfGraphic ()
	{
	}

	void EfGraphic::Draw ()
	{
		GrpAcv::Draw ();
	}

	void EfGraphic::SetPanelArray ( P_PanelArray & panelArray )
	{
		ClearObject ();
		VSP_PnOb & vp_pnOb = panelArray->GetrVP_PnObject ();
		for ( auto p : vp_pnOb )
		{
			AddpObject ( p->GetpEfObject () );
		}
	}

	void EfGraphic::Printf ()
	{
		DBGOUT_FL_F ( _T("EfGraphic\n") );
		int index = 0;
		for ( auto p : (*GetpvpObject()) )
		{
			if ( 0 == index % STAGE_SIZE_X )
			{
				DBGOUT_FL_F ( _T("\n") );
			}
			VEC2 v = p->GetPos ();
			bool b = p->GetValid ();
			DBGOUT_FL_F ( _T(" [%d](%d, %d), %d "), index, (int)v.x, (int)v.y, b ? 1: 0 );
			++index;
		}
		DBGOUT_FL_F ( _T(" \n") );
	}

}	//namespace GAME

