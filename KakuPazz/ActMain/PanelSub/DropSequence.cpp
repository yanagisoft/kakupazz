﻿//=================================================================================================
//
// 落下序処理
//	同時タイミングに落下確定したパネルを記録し、
//	その中で順次落下していく
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "DropSequence.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//-------------------------------------------------------------------------------------------------
	DropSequence::DropSequence ( P_PanelArray pPnAry )
		: m_pPnAry ( pPnAry )
		, m_end ( false )
		, m_dropTimer ( 0 )
		, m_dropSpan ( 0 )
	{
	}
	
	DropSequence::~DropSequence ()
	{
	}

	//落下位置配列を設定
	void DropSequence::SetrvDrop ( UINT n, V_PN_SPAN & rvDropPt )
	{
		m_vDropPt.resize ( n );
		
		for ( UINT i = 0; i < n; ++ i )
		{
			m_vDropPt [ i ] = rvDropPt [ i ];
		}
	}


	//通常落下手順
	void DropSequence::Do ()
	{
		//初回
		if ( 0 == m_dropTimer )
		{
			RotatePanel ();	//一連落下開始
		}

		//落下時間計測
		++ m_dropTimer;

		//落下時間満了時
		if ( DROP_TIME <= m_dropTimer )
		{
			m_dropTimer = 0;
			++ m_dropSpan;

			//着地判定
			_CheckGetdown ();

			//すべての落下が終了したらSequenceも終了
			if ( 0 == m_vDropPt.size () )
			{
				m_end = true;
			}
		}
	}

	SP_PnOb DropSequence::PsToPob ( PANEL_SPAN ps )
	{
		return m_pPnAry->GetpPnOb ( ps.pt );
	}

	//一連落下開始
	//落下するパネルから上のパネルを同時に落下させる(Rotate)
	void DropSequence::RotatePanel ()
	{
		for ( PANEL_SPAN ps : m_vDropPt )
		{
			//落下一連に対して入替先を設定する
			SetDropTarget ( ps );
		}
	}

	//落下一連の入替対象を設定
	void DropSequence::SetDropTarget ( PANEL_SPAN ps )
	{
		const UINT PY = PANEL_SIZE_Y;
		POINT pt = ps.pt;	//対象
		
		//対象位置の一つ下(空白)を保存
		SP_PnOb pnObBottom = m_pPnAry->GetpPnOb ( pt.x, pt.y + 1 );
		P_PnState pnStBlank = pnObBottom->GetpPanelState ();

		//１つ下に自身を設定し、落下スタート
		SP_PnOb ob = m_pPnAry->GetpPnOb ( pt.x, pt.y );
		P_PnState st = ob->GetpPanelState ();
		pnObBottom->StartDrop ( st, VEC2(0,+PY) );

		//追加で一連の落下個数分を繰返
		for ( UINT i = 1; i <= ps.n; ++ i )
		{
			//対象の状態を取得
			SP_PnOb ob0 = m_pPnAry->GetpPnOb ( pt.x, pt.y - i );
			P_PnState st0 = ob0->GetpPanelState ();

			//１つ下に設定し、落下スタート
			SP_PnOb ob1 = m_pPnAry->GetpPnOb ( pt.x, pt.y - i + 1 );
			ob1->StartDrop ( st0, VEC2(0,+PY) );
		}
			
		//落下一連の最上位に空白を設定して落下スタート
		SP_PnOb obTop = m_pPnAry->GetpPnOb ( pt.x, pt.y - ps.n );
		obTop->StartDrop ( pnStBlank, VEC2(0,+PY) );
	}

	//着地判定
	//落下直後、次も落下可能か判定する
	void DropSequence::_CheckGetdown ()
	{
		//チェック済フラグを初期化
		for ( PANEL_SPAN ps : m_vDropPt )
		{
			PsToPob ( ps )->SetDropCheck ( false );

			//上方向の一連も同様にする
			for ( UINT dy = 1; dy <= ps.n; ++ dy ) 
			{
				SP_PnOb pObUp = m_pPnAry->GetpPnOb ( ps.pt.x, ps.pt.y - dy );
				pObUp->SetDropCheck ( false );
			}
		}

		//落下位置配列を走査
		for ( PANEL_SPAN ps : m_vDropPt )
		{
			//一時変数
			POINT pt = ps.pt;
			UINT n = ps.n;
			SP_PnOb pOb = PsToPob ( ps );

			//チェック済の場合は飛ばす
			if ( pOb->GetDropCheck () ) { continue; }

			//おじゃまの場合、IDから指定して１ブロックをチェック
			if ( pOb->IsHamper () )
			{
				//着地判定
				UINT id = pOb->GetHamperID ();
				m_hmpMngr->CheckGetdownFromID ( id );
				continue;
			}
			
			//対象から２つ下が着地可能か（１つ目は今回、２つ目は次回）　	
			bool bGetdown = m_pPnAry->CanBeGetdown ( pt.x, pt.y + 2 );
			pOb->SetGetdown ( bGetdown );
			pOb->SetDropCheck ( true );

			//上方向の一連も同様にする
			for ( UINT dy = 1; dy <= ps.n; ++ dy ) 
			{
				SP_PnOb pObUp = m_pPnAry->GetpPnOb ( pt.x, pt.y - dy );

				//チェック済の場合は飛ばす
				if ( pObUp->GetDropCheck () ) { continue; }

				//おじゃまの場合、IDから指定して１ブロックをチェック
				if ( pObUp->IsHamper () ) 
				{
					UINT id = pObUp->GetHamperID ();
					m_hmpMngr->CheckGetdownFromID ( id );
					continue; 
				}
				pObUp->SetGetdown ( true );
				pObUp->SetDropCheck ( true );
			}
		}

		//チェック済フラグを確認
		for ( PANEL_SPAN ps : m_vDropPt )
		{
			assert ( PsToPob ( ps )->GetDropCheck () );
			//上方向の一連も同様にする
			for ( UINT dy = 1; dy <= ps.n; ++ dy ) 
			{
				SP_PnOb pObUp = m_pPnAry->GetpPnOb ( ps.pt.x, ps.pt.y - dy );
				assert ( pObUp->GetDropCheck () );
			}
		}



		//フラグに基づいて処理
		int index = 0;
		auto it = begin ( m_vDropPt );
		//落下の底のループ
		while ( it != end ( m_vDropPt ) )
		{
			PANEL_SPAN ps = m_vDropPt [ index ];
			POINT pt = ps.pt;
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( pt.x, pt.y );
			bool bGetdown = pOb->GetGetdown ();

			//着地
			if ( bGetdown )
			{
				GetdownPanel ( ps );

				//配列から削除(イテレータは次へ)
				it = m_vDropPt.erase ( it );

				//空のとき終了
				if ( 0 == m_vDropPt.size () ) { break; }

				continue;
			}
			//落下可能なら落下続行
			else
			{
				ContinueDropPanel ( ps );

				//位置を１つ下にして落下続行
				++(it->pt.y);
			}

#if 0


			//上方向の一連も同様にする
			for ( UINT dy = 1; dy <= ps.n; ++ dy ) 
			{
				SP_PnOb pObUp = m_pPnAry->GetpPnOb ( pt.x, pt.y - dy );
				bool bGetdown = pObUp->GetGetdown ();

				//着地
				if ( bGetdown )
				{
					GetdownPanel ( ps );
					continue;
				}
				//落下可能なら落下続行
				else
				{
					ContinueDropPanel ( ps );
				}

				//@todo 落下開始位置＋上方向ｎで記録していると
				//途中のおじゃまが着地したときに個数が変更になる
			}
#endif // 0

			++ index;
			++ it;
		}
	}

	void DropSequence::ContinueDropPanel ( PANEL_SPAN ps )
	{
		POINT pt = ps.pt;
		UINT n = ps.n;

		//対象位置の一つ下(空白)から
		SP_PnOb pnObBottom = m_pPnAry->GetpPnOb ( pt.x, pt.y + 1 );
		pnObBottom->ContinueDrop ();

		//落下一連の繰返
		for ( UINT i = 0; i < n; ++ i )
		{
			//落下状態続行
			SP_PnOb pob = m_pPnAry->GetpPnOb ( pt.x, pt.y - i );
			pob->ContinueDrop ();
		}

		//一連の最上位は透明かつ通常状態
		SP_PnOb pob = m_pPnAry->GetpPnOb ( pt.x, pt.y - n );
		pob->EndDrop ();
	}

	void DropSequence::GetdownPanel ( PANEL_SPAN ps )
	{
		POINT pt = ps.pt;
		UINT n = ps.n;

		//対象位置の一つ下(空白)から
		SP_PnOb pnObBlank = m_pPnAry->GetpPnOb ( pt.x, pt.y + 1 );
		pnObBlank->StartGetdown ();

		//一連
		for ( UINT i = 0; i < n; ++ i )
		{
			//落下状態ストップしてから着地状態を設定
			SP_PnOb pob = m_pPnAry->GetpPnOb ( pt.x, pt.y - i );
			pob->StartGetdown ();
		}

		//一連の最上位は透明かつ通常状態
		SP_PnOb pob = m_pPnAry->GetpPnOb ( pt.x, pt.y - n );
		pob->EndDrop ();
	}

}	//namespace GAME

