﻿//=================================================================================================
//
// 消去順序処理
//	同時タイミングに消去確定したパネルを記録し、
//	その中で順次消去していく
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "EraseSequence.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//-------------------------------------------------------------------------------------------------
	//調整パラメータ
	enum ERASE_SEQUENCE_PARAM
	{
		ERASE_TIME = 25,
	};

	//-------------------------------------------------------------------------------------------------
	EraseSequence::EraseSequence ( P_PanelArray pPanelArray )
		: m_pPnAry ( pPanelArray ), m_vErasePt ( STAGE_PANEL_NUM )
		, m_end ( false )
		, m_index ( 0 )
		, m_indexSize ( 0 )
		, m_eraseTimer ( 0 )
	{
		_Start ();
	}

	//消去開始のとき
	void EraseSequence::_Start ()
	{
		//消去フラグから消去するパネルの位置を記録
		const UINT y = STAGE_SIZE_Y - 2;
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				if ( pOb->GetEraseFlag () )
				{
					//消去待機状態
					pOb->EraseWait ();
					//配列に追加
					m_vErasePt [ m_index ++ ] = pOb->GetPoint ();
				}
			}
		}
		m_indexSize = m_index;	//初期スタックポインタサイズを記録
		m_index = 0;
	}

	EraseSequence::~EraseSequence ()
	{
		m_vErasePt.clear ();
	}


	void EraseSequence::Do ()
	{
		//通常消去手順
		_Do ();
	}

	//通常消去手順
	void EraseSequence::_Do ()
	{
		//消去時間カウント
		if ( ERASE_TIME > ++ m_eraseTimer ) { return; }

		//カウント完了時
		m_eraseTimer = 0;	//カウントリセット

		//消去スタック
		if ( m_indexSize > m_index )
		{
			//記録した位置から１つずつ消去
			POINT pt = m_vErasePt [ m_index ++ ];
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( pt );
			if ( pOb->IsHamper () )
			{
				pOb->StartEraseHamper ();
			}
			else
			{
				pOb->StartErase ();		//消去開始
			}
		}
		else
		{
			//最終処理


			//@todo おじゃまの消去フラグ初期化を調査


			//今回消去したパネルすべてに対し
			for ( UINT i = 0; i < m_indexSize; ++ i )
			{
				POINT pt = m_vErasePt [ i ];
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( pt );

				//連鎖フラグをORでチェック (１つでも存在すればtrueを返す)
				m_bChain |= pOb->GetChain ();

				//自身を通常状態にする
				pOb->SetDefault();

				//今回消えたパネルの上にあるすべてのパネルに連鎖フラグをセット
				SetChainFlag ( pt );
			}

			//消去シークエンス終了
			m_index = 0;
			m_end = true;
		}
	}

	//上のパネルに連鎖フラグを設定
	void EraseSequence::SetChainFlag ( POINT pt )
	{
		//対象から上に走査
		for ( UINT uy = pt.y - 1; 0 != uy ; -- uy )
		{
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( pt.x, uy );
			pOb->SetChain ( true );
		}
	}


}	//namespace GAME

