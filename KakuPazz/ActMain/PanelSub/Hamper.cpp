//=================================================================================================
//
// おじゃまパネル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Hamper.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	HamperBlock::HamperBlock ( P_PanelArray pPnAry )
		: m_pPnAry ( pPnAry )
	{
	}
	
	HamperBlock::HamperBlock ( const HamperBlock & rhs )
	{
		mv_hmp = rhs.mv_hmp;
		m_pPnAry = rhs.m_pPnAry;
	}

	HamperBlock::~HamperBlock ()
	{
	}

	UINT HamperBlock::GetID ()
	{
		//先頭[0]は必ず存在する(一連の値はすべて同じ)
		return m_pPnAry->GetpPnOb ( mv_hmp[0].pt )->GetHamperID ();
	}

	//ブロックが落下開始か
	void HamperBlock::CheckDropStart ()
	{
		//ブロックの底のいずれかが落下開始不能の場合、ブロックすべて落下開始不能
		//一連をチェック
		bool b = true;	//すべて落下可能の場合true
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			LONG x = hmp.pt.x; LONG y = hmp.pt.y;

			//底かどうかチェック
			if ( ! hmp.btm ) { continue; }

			//落下開始かどうか(底の１つでも落下開始不能なら一連が落下開始不能)
			if ( ! m_pPnAry->IsDropStart ( x, y ) ) { b = false; break; }
		}

		//一連にフラグを設定
		_SetAllDropStart ( b );
	}

	//ブロックが落下可能か
	void HamperBlock::CheckDropAble ()
	{
		//先頭を確認して消去や落下状態でなければ(他はすべて同じ)
		bool b = ! ( m_pPnAry->GetpPnOb ( mv_hmp[0].pt )->IsLocked () );

		//一連にフラグを設定
		_SetAllDropAble ( b );
	}

	//ブロックが落下か
	void HamperBlock::CheckDrop ()
	{
		//一連をチェック
		bool bDrop = true;
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			POINT pt = hmp.pt;
			LONG x = pt.x; LONG y = pt.y;

			//底をチェック
			if ( ! hmp.btm ) { continue; }

			//自身が落下可能か
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y );
			bool b0 = pOb->CanDrop ();

			//底の１つ下が落下状態もしくは被落下可能か
			SP_PnOb pObBottom = m_pPnAry->GetpPnOb ( x, y + 1 );
			bool b1 = pObBottom->GetDrop ();
			bool b2 = pObBottom->CanBeDropped ();
			bool bBottom = b1 || b2;

			//落下かどうか(底の１つでも落下不能なら一連が落下不能)
			if ( ! ( b0 && bBottom ) )
			{
				bDrop = false;
				break;
			}
		}

		//一連にフラグを設定
		_SetAllDrop ( bDrop );
	}

	//ブロックが着地か
	void HamperBlock::CheckGetdown ()
	{
		//一連をチェック
		bool bGetdown = false;
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			//底をチェック
			if ( ! hmp.btm ) { continue; }

			//着地かどうか(底の１つでも着地なら一連が着地)
			//対象から２つ下が被落下可能か（１つ目は今回、２つ目は次回）　	
			if ( ! m_pPnAry->CanBeDropped ( hmp.pt.x, hmp.pt.y + 2 ) )
			{
				bGetdown = true;
				break;
			}
		}

		//一連にフラグを設定
		_SetAllGetDown ( bGetdown );

		//着地もしくは落下続行
		//どちらの場合も１つ落下したため、おじゃま位置の更新
		_Down ();
	}

	void HamperBlock::push ( HAMPER_PANEL hmp )
	{
		mv_hmp.push_back ( hmp );

		//底判定
#if 0
		UINT btm_y [ STAGE_SIZE_X ] = { 0 };
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			hmp.btm = false;
			if ( hmp.pt.y > btm_y[hmp.pt.x] )
			{
				btm_y [hmp.pt.x] = hmp.pt.y;
			}
		}
#endif // 0
	}

	void HamperBlock::CheckErase ( LONG x, LONG y )
	{
		//いずれかがヒットするかどうか
		bool bErase = false;
		for ( auto hmp : mv_hmp )
		{
			if ( (x == hmp.pt.x) && (y == hmp.pt.y) )
			{
				bErase = true;
				break;
			}
		}

		//最初からフラグ付け
		for ( auto hmp : mv_hmp )
		{
			m_pPnAry->GetpPnOb ( hmp.pt )->SetEraseFlag ( true );
		}
	}

	void HamperBlock::_Down ()
	{
		for ( UINT i = 0; i < mv_hmp.size (); ++ i )
		{
			++ ( mv_hmp [ i ].pt.y );
		}
	}

	void HamperBlock::_SetAllDropStart ( bool b )
	{
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( hmp.pt );
			pOb->SetDropStart ( b );	//落下開始
			pOb->SetDropCheck ( true );	//確定
		}
	}

	void HamperBlock::_SetAllDrop ( bool b )
	{
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( hmp.pt );
			pOb->SetDrop ( b );
			pOb->SetDropCheck ( true );	//確定
		}
	}

	void HamperBlock::_SetAllDropAble ( bool b )
	{
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			m_pPnAry->GetpPnOb ( hmp.pt )->SetDropAble ( b );
		}
	}

	void HamperBlock::_SetAllGetDown ( bool b )
	{
		for ( HAMPER_PANEL hmp : mv_hmp )
		{
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( hmp.pt );
			pOb->SetGetdown ( b );	//着地開始
			pOb->SetDropCheck ( true );	//確定
		}
	}


}	//namespace GAME

