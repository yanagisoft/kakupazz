﻿//=================================================================================================
//
// 消去監理
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"
#include "EraseSequence.h"
//#include "Chain.h"
#include "HamperManager.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//-------------------------------------------------------------------------------------------------
	//消去監理
	class Erase
	{
		P_PanelArray	m_pPnlAry;		//パネル配列
		P_HmpMngr		m_hmpMngr;		//おじゃま監理
		list < P_EraseSeq >	m_listErase;	//消去手順ポインタのリスト

		UINT		m_sameNum;		//同時数
		UINT		m_chainNum;		//連鎖数
		bool		m_bKeepChain;	//連鎖持続フラグ

	public:
		Erase () = delete;
		Erase ( P_PanelArray pPanelArray );
		Erase ( const Erase & rhs ) = delete;
		~Erase ();

		void SetHmpMngr ( P_HmpMngr p ) { m_hmpMngr = p; }
		void Do ();

		//連鎖待機かどうか
		bool IsWaiting () const;

		//連鎖待機チェック
		void CheckWaiting ();

		//同時数・連鎖数の取得
		UINT GetSameNum () { return m_sameNum; }
		UINT GetChainNum () { return m_chainNum; }

	private:
		void _Generate ();
		void _DoSeq ();

		bool _CheckErase ();
		bool _CheckErase ( POINT pt0 , POINT pt1, POINT pt2 );

		bool _CheckEraseAroundHamper ( LONG x, LONG y );
	};


}	//namespace GAME

