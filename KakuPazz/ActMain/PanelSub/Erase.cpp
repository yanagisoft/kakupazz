﻿//=================================================================================================
//
// 消去順序処理
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Erase.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

//---------------------------------------------------------------------------------------------
	Erase::Erase ( P_PanelArray pPanelArray )
		: m_pPnlAry ( pPanelArray )
	{
	}

	Erase::~Erase ()
	{
		m_listErase.clear ();
	}

	void Erase::Do ()
	{
		//消去チェック
		if ( _CheckErase () )
		{
			//消去開始があったら消去手順を生成
			_Generate ();
		}

		//消去リスト処理
		_DoSeq ();
	}


	//連鎖待機かどうか
	bool Erase::IsWaiting () const
	{
		return m_bKeepChain;
	}

	//連鎖待機チェック
	void Erase::CheckWaiting ()
	{
		//他に消去シークエンスがなければ
		if ( 0 == m_listErase.size () )
		{
			m_bKeepChain = false;			//連鎖終了
		}
	}

	//新規消去手順作成	
	void Erase::_Generate ()
	{
		P_EraseSeq p = make_unique < EraseSequence > ( m_pPnlAry );
		m_listErase.push_back ( ::move ( p ) );	//リストに追加
	}

	void Erase::_DoSeq ()
	{
		m_sameNum = 0;
		bool bChain = false;

		list < P_EraseSeq > ::iterator it = m_listErase.begin ();
		while ( it != m_listErase.end () )
		{
			//消去手順実行	
			(*it)->Do ();

			//消去終了時	
			if ( (*it)->IsEnd () )
			{
				//同時数・連鎖数取得
				m_sameNum = (*it)->GetSameNum ();
				bChain = (*it)->GetbChain ();

				//※同時タイミングで起きた連鎖は１連鎖でカウント
				//※別シークエンスの２連鎖以上は時間差連鎖として＋１
				//※別シークエンスでも手動の１回消(１連鎖)はカウントしない

				//リストから外す(イテレータは次へ。unique_ptrは自動解放)
				it = m_listErase.erase ( it );
			}
			else
			{
				++it;
			}
		}

		//----------------------------------------------------------------
		//連鎖回数チェック
		if ( bChain )
		{
			++ m_chainNum;
			m_bKeepChain = true;
		}
		else
		{
			if ( 0 != m_chainNum )
			{
				//連鎖  状態でなければ
				if ( ! m_pPnlAry->IsChain () )
				{
					m_chainNum = 0;
				}
			}
		}

		//----------------------------------------------------------------
	}
	
	//消去チェック
	bool Erase::_CheckErase ()
	{
		//消去開始フラグ(１つでも存在したらOR(|=)で上書)
		bool eraseFlag = false;

		//消去チェック：横
		for ( UINT y = 0; y < STAGE_SIZE_Y - 1; ++y )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X - 2; ++x )
			{
				POINT pt0 = { (LONG)x + 0, (LONG)y };
				POINT pt1 = { (LONG)x + 1, (LONG)y };
				POINT pt2 = { (LONG)x + 2, (LONG)y };
				eraseFlag |= _CheckErase ( pt0, pt1, pt2 );
			}
		}
		//消去チェック：縦
		for ( UINT y = 0; y < STAGE_SIZE_Y - 3; ++y )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++x )
			{
				POINT pt0 = { (LONG)x, (LONG)y + 0 };
				POINT pt1 = { (LONG)x, (LONG)y + 1 };
				POINT pt2 = { (LONG)x, (LONG)y + 2 };
				eraseFlag |= _CheckErase ( pt0, pt1, pt2 );
			}
		}

		//消去チェック：おじゃま
		for ( UINT y = 0; y < STAGE_SIZE_Y - 1; ++ y )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				//今回消えるパネル
				SP_PnOb pOb = m_pPnlAry->GetpPnOb ( x, y );
				if ( pOb->GetEraseFlag () )
				{
					//周囲をチェック
					_CheckEraseAroundHamper ( x, y );

					//IDからブロックに通達
				}
			}
		}

		return eraseFlag;
	}

	//消去チェック
	bool Erase::_CheckErase ( POINT pt0, POINT pt1, POINT pt2 )
	{
		//対象を取得
		SP_PnOb ob0 = m_pPnlAry->GetpPnOb ( pt0 );
		SP_PnOb ob1 = m_pPnlAry->GetpPnOb ( pt1 );
		SP_PnOb ob2 = m_pPnlAry->GetpPnOb ( pt2 );

		//いずれかが消去不可能なら飛ばす
		if (   ! ob0->CanErase ()
			|| ! ob1->CanErase ()
			|| ! ob2->CanErase () )
		{
			return false;
		}

		//並んだ３つが同じ種類のとき消える
		PANEL_KIND kind0 = ob0->GetKind ();
		PANEL_KIND kind1 = ob1->GetKind ();
		PANEL_KIND kind2 = ob2->GetKind ();

		if ( ( kind1 == kind0 ) && ( kind2 == kind0 ) )
		{
			ob0->SetEraseFlag ( true );
			ob1->SetEraseFlag ( true );
			ob2->SetEraseFlag ( true );
			return true;
		}
		return false;
	}

	bool Erase::_CheckEraseAroundHamper ( LONG x, LONG y )
	{
		//左
		if ( 0 < x )
		{
			m_hmpMngr->CheckErase ( x - 1, y );
		}
		//右
		if ( x < STAGE_SIZE_X - 1 )
		{
			m_hmpMngr->CheckErase ( x + 1, y );
		}
		//上
		if ( 0 < y )
		{
			m_hmpMngr->CheckErase ( x, y - 1 );
		}
		//下
		if ( x < STAGE_SIZE_Y - 2 )
		{
			m_hmpMngr->CheckErase ( x, y + 1 );
		}

		return false;
	}


}	//namespace GAME

