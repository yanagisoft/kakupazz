﻿//=================================================================================================
//
// エフェクト
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../Panel/PanelCommon.h"
//#include "../Panel/PanelArray.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//エフェクトのオブジェクト
	class EfObject : public GameObject
	{
		UINT		m_timer;
		UINT		m_index;	//テクスチャインデックス
		VEC2		m_base;		//表示基準
		POINT		m_pt;	//位置xy

		void SetPoint ( POINT point ) { m_pt = point; }
	public:
		EfObject () = delete;
		EfObject ( LONG x, LONG y );
		EfObject ( const EfObject & rhs ) = delete;
		~EfObject ();

		void Move();

		void SetValid ( bool b )
		{ GameObject::SetValid ( b ); }
		void SetBase ( VEC2 & base ) { m_base = base; }
		void Position ( VEC2 moving, float upRevise );
	};

	using P_EfOb = shared_ptr < EfObject >;


}	//namespace GAME

