﻿//=================================================================================================
//
// 消去順序処理
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "../Panel/PanelObject.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"
//#include "Chain.h"
//#include "HamperBlock.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//消去手順監理
	class EraseSequence
	{
		P_PanelArray 	m_pPnAry;	//パネルポインタ配列参照
		vector < POINT > m_vErasePt;	//消去場所記憶の可変配列

		UINT			m_index;		//スタックポインタ
		UINT			m_indexSize;	//スタックポインタサイズ
		UINT			m_eraseTimer;	//消去にかける時間
		bool			m_end;			//終了フラグ
		bool			m_bChain;		//連鎖フラグ

	public:
		EraseSequence () = delete;
		EraseSequence ( P_PanelArray pPanelArray );
		EraseSequence ( const EraseSequence & rhs ) = delete;
		~EraseSequence ();

		void Do ();
		bool IsEnd () const { return m_end; }

		//同時数・連鎖数の取得
		UINT GetSameNum () { return m_indexSize; }
		bool GetbChain () { return m_bChain; }

	private:
		void _Start ();	//落下開始時
		void _Do ();	//通常消去手順

		void SetChainFlag ( POINT pt );	//上のパネルに連鎖フラグを設定
	};

	using P_EraseSeq = unique_ptr < EraseSequence >;


}	//namespace GAME

