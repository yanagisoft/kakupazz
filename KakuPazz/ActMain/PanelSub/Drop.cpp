﻿//=================================================================================================
//
// 落下処理
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Drop.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//コンストラクタ
	Drop::Drop ( P_PanelArray pPnAry )
		: m_pPnAry ( pPnAry )
	{
	}

	//デストラクタ
	Drop::~Drop ()
	{
		m_listDrop.clear ();
	}

	//毎Fの動作
	void Drop::Do ()
	{
		_CheckDropStart ();	//落下起点判定
		_CheckDropAble ();	//落下可能判定
		_CheckDrop ();		//全体落下判定
		_CheckDropRotate ();//落下一連判定
		_DoDrop ();			//落下実行
	}


	//落下起点のチェック
	void Drop::_CheckDropStart ()
	{
		//---------------------------------------
		//※おじゃまの形が矩形のみなら、
		//下から順に走査したとき、おじゃまの下は確定パネルのみ

		//---------------------------------------
		//繰返
			//通常パネルを走査
				//おじゃまのとき監理側からチェック
			//次のパネルからチェック済は飛ばす
		//すべて確定したら終了
		//---------------------------------------
		
		//チェック済フラグを初期化
		const UINT y = STAGE_SIZE_Y - 2;
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				pOb->SetDropCheck ( false );
			}
		}

		//落下開始チェック
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );

				//チェック済の場合は飛ばす
				if ( pOb->GetDropCheck () ) { continue; }

				//おじゃまの場合、IDから指定して１ブロックをチェック
				if ( pOb->IsHamper () )
				{
					UINT id = pOb->GetHamperID ();
					m_hmpMngr->CheckDropFromID ( id );
					continue;
				}

				//通常の場合、個別に落下起点判定を記録
				bool b = m_pPnAry->IsDropStart ( x, y - dy );
				if ( b )
				{
					pOb->SetDropStart ( b );
				}
				pOb->SetDropCheck ( true );
			}
		}

#if 1
		//すべてチェックしているかどうか
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				assert ( pOb->GetDropCheck () );
			}
		}
	}
#endif // 1


	//落下可能判定
	void Drop::_CheckDropAble ()
	{
		//おじゃま落下可能判定
		m_hmpMngr->CheckDropAble ();

		//最下段を除く全体を走査
		SPVSP_PnOb pvpOb = m_pPnAry->GetpVP_PnOb ();
		for ( auto pOb : (*pvpOb) )
		{
			POINT pt = pOb->GetPoint ();
			LONG x = pt.x; LONG y = pt.y;

			//最下段は飛ばす
			if ( y == STAGE_SIZE_Y - 1 ) { continue; }

			//おじゃまは飛ばす
			if ( pOb->IsHamper () ) { continue; }
			
			//個別に落下判定を記録
			pOb->SetDropAble ( pOb->CanDrop () );
		}
	}

	//全体落下判定
	void Drop::_CheckDrop ()
	{		
		//チェック済フラグを初期化
		const UINT y = STAGE_SIZE_Y - 2;
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				pOb->SetDropCheck ( false );
			}
		}

		//おじゃまと通常パネルを同時にチェック
		//おじゃまは底すべてが落下起点のとき、ブロックすべてに落下起点フラグがついている
		
		//[1](落下起点)か
		//もしくは、[2](自身が落下可能状態かつ１つ下が落下状態)かどうか
		//で落下状態にする

		//最下段より１つ上から、上方向に走査
		for ( UINT dy = 0; dy <= y; ++ dy )
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				//チェック済
				if ( pOb->GetDropCheck () ) { continue; }

				//[1]
				if ( pOb->GetDropStart () )
				{
					pOb->SetDrop ( true );
					pOb->SetDropCheck ( true );
					continue; 
				}

				//[2]
				if ( pOb->IsHamper () )
				{
					m_hmpMngr->CheckDrop ();
					continue;
				}

				bool b0 = pOb->CanDrop ();
				SP_PnOb pObBottom = m_pPnAry->GetpPnOb ( x, y - dy + 1 );
				bool b1 = pObBottom->GetDrop ();
				if ( b0 && b1 )
				{
					pOb->SetDrop ( true ); 
					pOb->SetDropCheck ( true ); 
					continue;
				}

				//いずれでもない
				pOb->SetDrop ( false ); 
				pOb->SetDropCheck ( true ); 
			}
		}

#define TEST_DEBUGOUT_FILE 0
#if TEST_DEBUGOUT_FILE
		//test
		{
			DBGOUT_FL_F ( _T("//------------------------------\n") );
			SPVSP_PnOb pvpOb = m_pPnAry->GetpVP_PnOb ();
			for ( auto pOb : (*pvpOb) )
			{
				DBGOUT_FL_F ( _T("%d "), pOb->GetDropCheck () );
				//改行
				POINT pt = pOb->GetPoint ();
				if ( pt.x + 1 == STAGE_SIZE_X )
				{
					DBGOUT_FL_F ( _T("\n") );
				}
			}
		}
#endif // TEST_DEBUGOUT_FILE
		//すべてチェックしているかどうか
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				assert ( pOb->GetDropCheck () );
			}
		}

	}

	void Drop::_CheckDropRotate ()
	{
		//チェック済フラグを初期化
		const UINT y = STAGE_SIZE_Y - 2;
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				pOb->SetDropCheck ( false );
			}
		}

		//----------------------------------------------
		//落下フラグに基づいて、同時に落下する位置を記録
		UINT dropNum = 0;	//今回落下する個数
		vector < PANEL_SPAN > vDropPt ( STAGE_PANEL_NUM );	//落下の位置を一時保存

#if 0
		SPVSP_PnOb pvpOb = m_pPnAry->GetpVP_PnOb ();
		for ( auto pOb : (*pvpOb) )
		{
			//落下時
			if ( pOb->GetDrop () ) 
			{
				PANEL_SPAN ps;
				//位置を記録
				ps.pt = pOb->GetPoint ();
				//上方同時落下個数を記録
				ps.n = nDrop ( ps.pt.x, ps.pt.y );
				//一時配列に保存
				vDropPt [ dropNum ++ ] = ps;
			}
		}
#endif // 0

		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				if ( pOb->GetDrop () )		//落下時
				{
					//下から走査して上方向のチェックをするため、
					//垂直方向で最下段の位置だけ保存される

					//チェック済は飛ばす
					if ( pOb->GetDropCheck () ) { continue; }

					//位置と上方同時落下個数を記録
					PANEL_SPAN ps;
					ps.pt = pOb->GetPoint ();
					ps.n = nDrop ( ps.pt.x, ps.pt.y );

					//一時配列に保存
					vDropPt [ dropNum ++ ] = ps;

					pOb->SetDropCheck ( true );
				}
				else
				{
					pOb->SetDropCheck ( true );
				}
			}
		}

#if 1
		//すべてチェックしているかどうか
		for ( UINT dy = 0; dy <= y; ++ dy )	//下から上に走査
		{
			for ( UINT x = 0; x < STAGE_SIZE_X; ++ x )
			{
				SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - dy );
				assert ( pOb->GetDropCheck () );
			}
		}
#endif // 0

		//----------------------------------------------
		//今回１つでも落下するなら落下手順生成
		if ( 0 < dropNum )
		{
			//新規落下手順を作成
 			P_DropSeq p = make_unique < DropSequence > ( m_pPnAry );
			p->SetHmpMngr ( m_hmpMngr ); //おじゃま監理を渡す
			p->SetrvDrop ( dropNum, vDropPt );	//落下位置配列を渡す

			//リストに移譲
			m_listDrop.push_back ( ::move ( p ) );
		}
	}

	//上方向の同時に落下する個数(自身を含まない)
	UINT GAME::Drop::nDrop ( LONG x, LONG y )
	{
		//最上段はチェックしない
		if ( 0 == y ) { return 0; }

		//１つ上からスタート
		LONG num = 1;
		while ( num <= y )	//yから０に向けて走査
		{
			//対象を取得して、落下しない場合に数上げ終了
			SP_PnOb pOb = m_pPnAry->GetpPnOb ( x, y - num );

			if ( ! pOb->CanDrop () ) { break; }

			//落下する場合はチェックをつける
			pOb->SetDropCheck ( true );

			++ num;	//一つ上に移行
		}
		return num - 1;
	}

	//落下実行
	void Drop::_DoDrop ()
	{
		auto it = begin ( m_listDrop );
		while ( it != end ( m_listDrop ) )
		{
			//落下手順実行	
			(*it)->Do ();

			//落下終了時	
			if ( (*it)->IsEnd () )
			{
				//リストから外す(イテレータは次へ)
				//unique_ptrは自動解放
				it = m_listDrop.erase ( it );
			}
			else
			{
				++it;
			}
		}
	}

#if 0


	//自身の状態と下のパネル状態から、落下可能かどうか
	bool Drop::CanDrop ( LONG x, LONG y ) const
	{
		//自身の状態
		if ( ! m_pPnAry->CanDrop ( x, y ) ) { return false; }

		//↓以下、自身は落下可能の場合

		//下のパネル
		SP_PnOb pPnObBottom = m_pPnAry->GetpPnOb ( x, y + 1 );
		return pPnObBottom->CanBeDropped ();
	}
#endif // 0


}	//namespace GAME

