//=================================================================================================
//
// おじゃまパネル監理
//		・位置をブロックごとに記録
//		・落下時ブロック判定
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../../GameMain/GameCommon.h"
#include "Hamper.h"
#include "../Panel/PanelCommon.h"
#include "../Panel/PanelArray.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//ブロック監理
	using L_HamperBlock = list < HamperBlock >;

	//おじゃまパネル監理
	class HamperManager
	{
		P_PanelArray		m_pPnAry;
		L_HamperBlock		ml_hmpBlk;
		UINT		m_id;	//ID

	public:
		HamperManager ();
		HamperManager ( const HamperManager & rhs ) = delete;
		~HamperManager ();

		//パネル配列の設定
		void SetpPnAry ( P_PanelArray pPnAry );

		//生成
		void GenerateHamper ( UINT size );

		//----------------------------------------------------
		//IDから１ブロックが落下するかどうかチェックする
		void CheckDropFromID ( UINT id );

		//落下起点かどうかフラグ付けをする
		void CheckDropStart ();

		//落下可能かどうかフラグ付けをする
		void CheckDropAble ();

		//落下状態かフラグ付けをする
		void CheckDrop ();

		//----------------------------------------------------
		//IDから１ブロックが落下するかどうかチェックする
		void CheckGetdownFromID ( UINT id );

		//着地可能か
		void CheckGetdown ();
		//----------------------------------------------------
		//消去
		//引数：消えた通常パネルの周囲(上下左右判定は呼出し元で行う)
		void CheckErase ( LONG x, LONG y );
	};

	using P_HmpMngr = shared_ptr < HamperManager >;


}	//namespace GAME

