﻿//=================================================================================================
//
// Stage ソースファイル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Stage.h"
#include "../Title/Title.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	Stage::Stage ()
		: m_start ( false )
	{
		//背景
		m_pBg = make_shared < GrpAcv > ();
		m_pBg->AddTexture ( _T("bg.png") );
		AddTask ( m_pBg );

		//ゲーム中ロゴ
		m_logo = make_shared < GrpAcv > ();
		m_logo->AddTexture ( _T("act_logo.png") );
		m_logo->SetPos ( -7, -5 );
		AddTask ( m_logo );

		//ゲーム進行
		m_procedure = make_shared < GAME_PROCEDURE > ();
		* m_procedure = GAME_PROCEDURE_START;
//@temp		* m_procedure = GAME_PROCEDURE_PLAY;

		//ゲーム進行表示
		m_dispProc = make_shared < DispProcedure > ( m_procedure );
		AddTask ( m_dispProc );
	}

	Stage::~Stage ()
	{
	}

//----------------------------------------------------------------------------
	Stage1p::Stage1p ()
	{
		//パネル背景
		m_pPanelBg = make_shared < GrpAcv > ();
		m_pPanelBg->AddTexture ( TEXT("panel_bg.png") );
		m_pPanelBg->SetPos ( 192.f, -16.f );
		AddTask ( m_pPanelBg );

		//パネル全般
		m_panelManager = make_shared < PanelManager > ( GetpProcedure () );
		m_panelManager->SetMode ( SINGLE );
		m_panelManager->SetPlayerNum ( PLAYER1 );
		AddTask ( m_panelManager );

		//ステージ下部分のパネル隠し背景
		m_pBgBottom = make_shared < GrpAcv > ();
		m_pBgBottom->AddTexture ( TEXT("bg_bottom1p.png") );
		m_pBgBottom->SetPos ( VEC2( -1, 384 ) );
		AddTask ( m_pBgBottom );

		//キャラ総合表示
		m_dispSummary = make_shared < DispSummary > ();
		AddTask ( m_dispSummary );
	}

	Stage1p::~Stage1p ()
	{
	}

	void Stage1p::ParamInit ( P_Param p )
	{
		//パラメータに関する初期化

		//パネル
		m_panelManager->SetMode ( SINGLE );
		m_panelManager->SetPlayerNum ( PLAYER1 );
		m_panelManager->ParamInit ();

		//キャラ総合表示
		m_dispSummary->SetPlayerNum ( PLAYER1 );
		m_dispSummary->ParamInit ( p );
	}

	void Stage1p::Move()
	{
		//プロシージャによって分岐
		switch ( GetProcedure () )
		{
		case GAME_PROCEDURE_START: break;
		case GAME_PROCEDURE_PLAY: Play (); break;
		case GAME_PROCEDURE_OVER: break;
		case GAME_PROCEDURE_TITLE: break;
		default: break;
		}

		Stage::Move();
	}

	void Stage1p::Play ()
	{
		//開始
		if ( ! m_start )
		{
			SOUND->PlayLoop ( BGM_MAIN );
			m_start = true;
		}

		//終了条件チェック
		PLAYER_PROCEDURE proc = m_panelManager->GetProcedure ();
		if ( PLAYER_PROCEDURE_OVER == proc )
		{
			SetProcedure ( GAME_PROCEDURE_OVER );
		}
	}
//----------------------------------------------------------------------------

	Stage2p::Stage2p()
	{
		//パネル背景
		m_panelBg1p = make_shared < GrpAcv > ();
		m_panelBg1p->AddTexture ( _T("panel_bg.png") );
		m_panelBg1p->SetPos ( PANEL_BASE_1P_X - 32 , -16.f );
		m_panelBg2p = make_shared < GrpAcv > ();
		m_panelBg2p->AddTexture ( _T("panel_bg.png") );
		m_panelBg2p->SetPos ( PANEL_BASE_2P_X - 32 , -16.f );

		//パネル
		m_panelManager1p = make_shared < PanelManager > ( GetpProcedure () );
		m_panelManager2p = make_shared < PanelManager > ( GetpProcedure () );

		//ステージ下部分のパネル隠し背景
		m_bgBottomDOUBLE = make_shared < GrpAcv > ();
		m_bgBottomDOUBLE->AddTexture ( _T("bg_bottom2p.png") );
		m_bgBottomDOUBLE->SetPos ( VEC2( 0, PANEL_BOTTOM ) );

		//キャラ
		m_dispSummary1p = make_shared < DispSummary > ();
		m_dispSummary2p = make_shared < DispSummary > ();

		//タスク設定
		AddTask ( m_panelBg1p );
		AddTask ( m_panelBg2p );
		AddTask ( m_panelManager1p );
		AddTask ( m_panelManager2p );
		AddTask ( m_bgBottomDOUBLE );
		AddTask ( m_dispSummary1p );
		AddTask ( m_dispSummary2p );
	}

	Stage2p::~Stage2p ()
	{
	}


	void Stage2p::ParamInit ( P_Param p )
	{
		//パラメータに関する初期化
		m_panelManager1p->SetMode ( p->GetMode () );
		m_panelManager2p->SetMode ( p->GetMode () );
		m_panelManager1p->SetPlayerNum ( PLAYER1 );
		m_panelManager2p->SetPlayerNum ( PLAYER2 );
		m_panelManager1p->ParamInit ();
		m_panelManager2p->ParamInit ();

		m_dispSummary1p->SetPlayerNum ( PLAYER1 );
		m_dispSummary2p->SetPlayerNum ( PLAYER2 );
		m_dispSummary1p->ParamInit ( p );
		m_dispSummary2p->ParamInit ( p );
	}


	void Stage2p::Move()
	{
		//プロシージャによって分岐
		switch ( GetProcedure () )
		{
		case GAME_PROCEDURE_START: break;
		case GAME_PROCEDURE_PLAY: Play (); break;
		case GAME_PROCEDURE_OVER: break;
		case GAME_PROCEDURE_TITLE: break;
		default: break;
		}

		Stage::Move();
	}

	void Stage2p::Play ()
	{
		//開始
		if ( ! m_start )
		{
			SOUND->PlayLoop ( BGM_MAIN );
			m_start = true;
		}
#if 0
		//------------------------------
		//両者チェック

		//1p->2p
		UINT chain1p =  m_panelManager1p->GetChain ();
		m_panelManager2p->SetHamper ( chain1p );

		//2p->1p
		UINT chain2p =  m_panelManager2p->GetChain ();
		m_panelManager1p->SetHamper ( chain2p );

//		DebugOutGameText::instance () -> DebugOutfN ( 3, TEXT("%d, %d"), chain1p, chain2p );
#endif // 0
		
		//終了条件チェック
		PLAYER_PROCEDURE proc1 = m_panelManager1p->GetProcedure ();
		PLAYER_PROCEDURE proc2 = m_panelManager2p->GetProcedure ();
		
		//同時：引分
		if ( proc1 == PLAYER_PROCEDURE_OVER && proc2 == PLAYER_PROCEDURE_OVER )
		{
			SetProcedure ( GAME_PROCEDURE_OVER );
		}
		//1P負：2P勝
		if ( proc1 == PLAYER_PROCEDURE_OVER )
		{
			SetProcedure ( GAME_PROCEDURE_2P_WIN );
		}
		//2P負：1P勝
		if ( proc2 == PLAYER_PROCEDURE_OVER )
		{
			SetProcedure ( GAME_PROCEDURE_1P_WIN );
		}
	}


}	//namespace GAME

