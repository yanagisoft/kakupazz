﻿//=================================================================================================
//
// カーソル　ヘッダ
//					パネル操作
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/SceneCommon.h"
#include "Panel/PanelCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//方向別押しっぱなし判定補助クラス
	class CursorInput
	{
		KEY_NAME	m_targetKey;
		shared_ptr < POINT >	m_pPt;
		UINT		m_inputTimer;

	public:
		CursorInput () : m_inputTimer ( 0 ) {}
		CursorInput ( const CursorInput& rhs ) = delete;
		~CursorInput () {}
		
		void SetTargetKey ( KEY_NAME key ) { m_targetKey = key; }
		void SetpPoint ( shared_ptr < POINT > pPt ) { m_pPt = pPt; }
		shared_ptr < POINT > GetpPt () { return m_pPt; }
		POINT GetPt () const { return * m_pPt; }

		void Input ();
		void DoMove ();

		virtual bool Condition () = 0;
		virtual void Do () = 0;
	};
	//-------------------------------------------------------------------------------------------------

	class CursorInputUp : public CursorInput { public: bool Condition (); void Do (); };
	class CursorInputDown : public CursorInput { public: bool Condition (); void Do (); };
	class CursorInputLeft : public CursorInput { public: bool Condition (); void Do (); };
	class CursorInputRight : public CursorInput { public: bool Condition (); void Do (); };

	//-------------------------------------------------------------------------------------------------
	class Cursor	: public GrpAcv
	{
		const static KEY_NAME	m_input[_PLAYER_NUM][INPUT_NUM];		//入力の1p2p分け

		shared_ptr < POINT >	m_point;		//ゲーム座標
		float				m_upRevise;		//上昇位置補正
		UINT				m_pulseTimer;	//鼓動タイマー
		bool				m_pulse;		//鼓動フラグ

		//押しっぱなしタイマー
		CursorInputUp		m_inputUp;
		CursorInputDown		m_inputDown;
		CursorInputLeft		m_inputLeft;
		CursorInputRight	m_inputRight;

		//1人用、2人用(1p,2p)の基準位置
		VEC2			m_base;
		GAME_MODE		m_mode;
		PLAYER_NUM		m_playerNum;

	public:
		Cursor ();
		Cursor ( const Cursor& rhs ) = delete;
		~Cursor ();

		void Init ();
		void Move ();

		bool CallSwap ();
		bool CallUp ();

		bool CallDebugSpoit ();
		bool CallDebugPaste ();

		POINT GetPoint() { return  * m_point; }

		void SetUpRevise ( float f ) { m_upRevise = f; }

		void Down();
		void Up();

		//基準位置設定
		void SetBase( VEC2& vec ) { m_base = vec; }

		//ゲームプレイヤ(表示と入力)設定
		void SetPlayerNum ( PLAYER_NUM playerNum )
		{
			m_playerNum = playerNum; 

			//カーソル入力補助に対象キーを設定
			m_inputUp.SetTargetKey ( m_input[playerNum][UP] );
			m_inputDown.SetTargetKey ( m_input[playerNum][DOWN] );
			m_inputLeft.SetTargetKey ( m_input[playerNum][LEFT] );
			m_inputRight.SetTargetKey ( m_input[playerNum][RIGHT] );
		}
	};


}	//namespace GAME

