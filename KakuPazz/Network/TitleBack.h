//=================================================================================================
//
// TitleBack ヘッダファイル
//	タイトルの背景動作
//
//=================================================================================================

#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
//#include "Const.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//斜め移動背景
	class TitleBack : public GameTaskVector
	{
		shared_ptr < GrpAcv >	m_pBack;

		static const UINT BACK_W;
		static const UINT BACK_H;
		static const UINT NUM_W;
		static const UINT NUM_H;
		static const UINT OBJECT_SIZE;

		VEC2 m_vec;

	public:
		TitleBack ();
		TitleBack ( const TitleBack& rhs ) = delete;
		virtual ~TitleBack ();

		void Init ();
		void Move ();

		void SetColor ( D3DXCOLOR color );
	};

	using P_TitleBack = shared_ptr < TitleBack >;

}	//namespace GAME


