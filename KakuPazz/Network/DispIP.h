﻿//=================================================================================================
//
//	IP表示　ヘッダ
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/SceneCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class DispIP : public GameTaskVector
	{
		P_GrpAcv		m_grpIP;
		string			m_ip;
		int				m_port;
		static const int	TX_DOT;
		static const int	TX_COLON;
		static const int	DISP_IP_X;
		static const int	DISP_IP_Y;
		static const int	DISP_IP_P;

	public:
		DispIP ();
		DispIP ( const DispIP & rhs ) = delete;	//コピー禁止
		~DispIP ();

		void SetNetworkSetting ( string& ip, int port );
		void Move ();
	};

}	//namespace GAME

