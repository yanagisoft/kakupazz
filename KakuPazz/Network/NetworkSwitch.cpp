﻿//=================================================================================================
//
//	ネットワーク　スイッチ　ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "NetworkSwitch.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{


	NetworkSwitch::NetworkSwitch ()
		: m_start ( false ), m_frame ( 0 )
	{
	}

	NetworkSwitch::~NetworkSwitch ()
	{
	}

	void NetworkSwitch::ParamInit ( P_Param param )
	{
		//パラメータの保存
		m_param = param;

		//ゲームモードで分岐
		switch ( m_param->GetMode() )
		{
		case SINGLE:
			m_stage = make_shared < Stage1p > ();
			LoacalStart (); 
		break;
		case DOUBLE_1P_CPU: 
			m_stage = make_shared < Stage2p > ();
			LoacalStart (); 
		break;
		case DOUBLE_1P_2P: 
			m_stage = make_shared < Stage2p > ();
			LoacalStart (); 
		break;
		case DOUBLE_CPU_CPU: 
			m_stage = make_shared < Stage2p > ();
			LoacalStart (); 
		break;
		case NETWORK_SERVER_1P:
			AddNetwork ();
		break;
		case NETWORK_CLIENT_2P:
			AddNetwork ();
		break;
		default: break;
		}
	}

	void NetworkSwitch::LoacalStart ()
	{
//		SOUND->PlayLoop ( BGM_MAIN );

		//ステージを追加
//		m_stage->SetMode ( m_param->GetMode () );
		m_stage->ParamInit ( m_param );
		AddTask ( m_stage );

		//開始フラグ
		m_start = true;
	}

	void NetworkSwitch::AddNetwork ()
	{
		//ステージも生成はしておく
		m_stage = make_shared < Stage2p > ();
//		m_stage->SetMode ( m_param->GetMode () );
		m_stage->ParamInit ( m_param );
		m_stage->Load ();
		m_stage->Init ();

		//ネット待機の生成と追加
		m_netWaiting = make_shared < NetWaiting > ( m_param->GetMode () );
		m_netWaiting->SetNetworkSetting ( m_param->GetIp (), m_param->GetNetPort () );
		AddTask ( m_netWaiting );

		//BGM
		SOUND->PlayLoop ( BGM_SETTINGS );
	}

	void NetworkSwitch::Move ()
	{
		//モードによる分岐
		switch ( m_param->GetMode () )
		{
		case SINGLE: break;
		case DOUBLE_1P_CPU: break;
		case DOUBLE_1P_2P: break;
		case DOUBLE_CPU_CPU: break;
		case NETWORK_SERVER_1P: 
			StateCheck ( Server::GetState () ); 
		break;
		case NETWORK_CLIENT_2P: 
			StateCheck ( Client::GetState () ); 
		break;
		default: break;
		}

		GameTaskVector::Move ();
	}

	void NetworkSwitch::StateCheck ( NETWORK_STATE nstate )
	{
		//接続状態で分岐
		switch ( nstate )
		{
		case NS_START:	return;		//開始状態
		case NS_WAIT:	return;		//待機状態
		case NS_CONNECT: return;		//接続時	
		case NS_DISCONNECT: return;		//非接続時	
		case NS_ACT: Act (); break;		//動作
		default:	break;
		}
	}

	void NetworkSwitch::Act ()
	{
		//初回に待機を解除、以下続行
		if ( ! m_start )
		{
			NetworkStart ();
		}

		// ※ゲームモードと接続状態の両方を用いる
		//サーバ・クライアント同期
		switch ( m_param->GetMode () )
		{
		case SINGLE: break;
		case DOUBLE_1P_CPU: break;
		case DOUBLE_1P_2P: break;
		case DOUBLE_CPU_CPU: break;
		case NETWORK_SERVER_1P: 
//			Server::FrameAsyncWait ( m_frame );
		break;
		case NETWORK_CLIENT_2P: 
//			Client::FrameAsyncWait ( m_frame );
		break;
		default: break;
		}
	}
	
	void NetworkSwitch::NetworkStart ()
	{
		//ネット待機を取外
		EraseTask ( m_netWaiting );
		SOUND->Stop ( BGM_SETTINGS );

		//ステージを追加
		m_stage->ParamInit ( m_param );
		AddTask ( m_stage );

		//開始フラグ
		m_start = true;
	}

	bool NetworkSwitch::Title () const
	{
		if ( m_netWaiting->Title () ) { return true; }
		return false;
	}

}	//namespace GAME

