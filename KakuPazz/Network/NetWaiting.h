﻿//=================================================================================================
//
//	ネットワークウェイティング　ヘッダ
//		接続待機中の表示
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/GameCommon.h"
#include "../GameMain/SceneCommon.h"
#include "TitleBack.h"
#include "DispIP.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class NetWaiting : public GameTaskVector
	{
		P_GrpAcv		m_loading;
		float			m_rad;
		GAME_MODE		m_gameMode;
		string			m_ip;
		int				m_port;
		bool			m_title;	//タイトルへ戻るフラグ

		P_TitleBack		m_pBack;	//背景
		P_GrpAcv		m_networkMode;	//モード表示
		shared_ptr < DispIP >	m_dispIP;	//IP表示
		P_GrpAcv		m_explain;		//説明
		
	public:
		NetWaiting ( GAME_MODE mode );
		NetWaiting ( const NetWaiting & rhs ) = delete;	//コピー禁止
		~NetWaiting ();

		void SetNetworkSetting ( string& ip, int port );

		void Init ();
		void Move ();

		bool Title () const { return m_title; }

	private:
		void Start ();
		void Wait ();
		void Connect ();
		void Disconnect ();
		void Act ();
	};

}	//namespace GAME

