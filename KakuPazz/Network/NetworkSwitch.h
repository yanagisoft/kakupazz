﻿//=================================================================================================
//
//	ネットワーク　スイッチ　ヘッダ
//		ネットワーク接続中とメインのステージを
//		ネットワークの状態によって切り替える
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/GameCommon.h"
#include "NetWaiting.h"
#include "../ActMain/Panel/PanelCommon.h"
#include "../ActMain/Stage.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class NetworkSwitch : public GameTaskVector
	{
		bool		m_start;	//開始フラグ
		UINT		m_frame;	//同期のための自フレーム保存
		P_Param		m_param;	//パラメータ
//		GAME_MODE	m_mode;		//ゲームモード

		shared_ptr < NetWaiting >	m_netWaiting;	//接続待機表示
		shared_ptr < Stage >	m_stage;	//ゲームステージ

	public:
		NetworkSwitch ();
		NetworkSwitch ( const NetworkSwitch & rhs ) = delete;	//コピー禁止
		~NetworkSwitch ();

		void ParamInit ( P_Param param );
		void Move ();

		//ネットワーク待機を用いないで開始
		void LoacalStart ();
		void AddNetwork ();

		bool Title () const;

		void StateCheck ( NETWORK_STATE nstate );
		void Act ();

		//ネットワーク状態により開始(切替)
		void NetworkStart ();


		//ゲーム進行
		GAME_PROCEDURE GetProcedure () const 
		{
			return m_stage->GetProcedure (); 
		}

		//同期のための自フレーム保存
		void SetFrame ( UINT frame ) { m_frame = frame; }
	};

}	//namespace GAME

