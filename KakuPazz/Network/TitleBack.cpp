//=================================================================================================
//
// TitleBack ソースファイル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "TitleBack.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	//定数
	const UINT TitleBack::BACK_W = 64;
	const UINT TitleBack::BACK_H = 64;
	const UINT TitleBack::NUM_W = WINDOW_WIDTH / BACK_W + 2;
	const UINT TitleBack::NUM_H = WINDOW_HEIGHT / BACK_H + 2;	//画面サイズ/テクスチャサイズ＋両端の２つ
	const UINT TitleBack::OBJECT_SIZE = NUM_W * NUM_H;


	TitleBack::TitleBack ()
		: m_vec ( 0, 0 )
	{
		m_pBack = make_shared < GameGraphicFromArchive > ();

		//テクスチャの読込
		m_pBack->AddTexture ( TEXT ( "back.jpg" ) );

		//オブジェクトの追加
		m_pBack->ClearObject ();
		m_pBack->AddObject ( OBJECT_SIZE );

		//オブジェクトから位置の設定
		for ( UINT i = 0; i < OBJECT_SIZE; ++i )
		{
			float x = 1.f * BACK_W * (i % NUM_W);
			float y = 1.f * NUM_H * (i / NUM_W);
			m_pBack->SetiPos ( i, VEC2 ( x, y ) );
		}

		//タスクを追加
		AddTask ( m_pBack );
	}
	
	TitleBack::~TitleBack ()
	{
	}
	
	void TitleBack::Init ()
	{
		GameTaskVector::Init ();
	}

	void TitleBack::Move ()
	{
		//位置の計算
		m_vec += VEC2 ( -0.8f, -0.8f );
		if ( m_vec.x < -1.f * BACK_W ) 
		{
			m_vec.x = 0;
			m_vec.y = 0;
		}

		for ( UINT i = 0; i < OBJECT_SIZE; ++i )
		{
			VEC2 v = m_vec + VEC2 ( 1.f * BACK_W * ( i % NUM_W ), 1.f * BACK_H * ( i / NUM_W ) );
			m_pBack->SetiPos ( i, v );
		}
		GameTaskVector::Move ();
	}

	void TitleBack::SetColor ( D3DXCOLOR color )
	{
		for ( UINT i = 0; i < NUM_W; i++ )
		{
			for ( UINT j = 0; j < NUM_H; j++ )
			{
				m_pBack->SetAllColor ( color );
			}
		}
	}


}	//namespace GAME

