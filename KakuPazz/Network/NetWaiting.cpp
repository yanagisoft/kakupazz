﻿//=================================================================================================
//
//	ネットワークウェイティング　ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "NetWaiting.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	NetWaiting::NetWaiting ( GAME_MODE mode )
		: m_rad ( 0 ), m_gameMode ( mode ), m_title ( false )
	{
		//移動背景
		m_pBack = make_shared < TitleBack > ();
		m_pBack->SetColor ( 0xffd0d0e8 );
		AddTask ( m_pBack );

		//モード表示
		m_networkMode = make_shared < GrpAcv > ();
		m_networkMode->AddTexture ( _T ( "networkServer.png" ) );
		m_networkMode->AddTexture ( _T ( "networkClient.png" ) );
		m_networkMode->SetPos ( VEC2 ( 180, 50 ) );
		AddTask ( m_networkMode );

		//説明
		m_explain = make_shared < GrpAcv > ();
		m_explain->AddTexture ( _T ( "Ex_net.png" ) );
		m_explain->SetPos ( VEC2 ( 180, 350 ) );
		AddTask ( m_explain );

		//IP表示
		m_dispIP = make_shared < DispIP > ();
		AddTask ( m_dispIP );

		//IP,Port 初期値
		m_ip = "127.0.0.1";
		m_port = 12345;
		m_dispIP->SetNetworkSetting ( m_ip, m_port );

		//回転
		m_loading = make_shared < GrpAcv > ();
		m_loading->AddTexture ( _T("erace_ef0.png") );
		m_loading->SetPos ( 50, 100 );
		m_loading->SetRotationCenter ( VEC2 ( 64, 64 ) );
		AddTask ( m_loading );
	}

	NetWaiting::~NetWaiting ()
	{
	}

	void NetWaiting::SetNetworkSetting ( string& ip, int port )
	{
		m_ip = ip;
		m_port = port;
		m_dispIP->SetNetworkSetting ( ip, port );
	}

	void NetWaiting::Init ()
	{
		if ( NETWORK_SERVER_1P == m_gameMode )
		{
			m_networkMode->SetIndexTexture ( 0 );
		}
		else
		{
			m_networkMode->SetIndexTexture ( 1 );
		}
		GameTaskVector::Init ();
	}

	void NetWaiting::Move ()
	{
		NETWORK_STATE ns;

		if ( NETWORK_SERVER_1P == m_gameMode )
		{
			ns = Server::GetState ();
		}
		else
		{
			ns = Client::GetState ();
		}

		//状態を取得して分岐
		switch ( ns )
		{
		case NS_START:	Start ();	break;		//開始状態
		case NS_WAIT:	Wait ();	break;		//待機状態
		case NS_CONNECT: Connect ();	break;		//接続時	
		case NS_DISCONNECT: Disconnect ();	break;		//非接続時	
		case NS_ACT: Act ();	break;		//動作
		default:	break;
		}

		GameTaskVector::Move ();
	}

	void NetWaiting::Start ()
	{
		//ボタン１でスタート
		if ( PUSH_KEY ( P1_BUTTON1 ) )
		{
			if ( NETWORK_SERVER_1P == m_gameMode )
			{
				Server::instance ()->Start ();
			}
			else
			{
				Client::instance()->Start ( m_ip.c_str(), m_port );
			}
		}

		//ボタン２でタイトルに戻る
		if ( PUSH_KEY ( P1_BUTTON2 ) )
		{
			m_title = true;
		}
	}

	void NetWaiting::Wait ()
	{
		//ボタン１でキャンセル
		if ( PUSH_KEY ( P1_BUTTON1 ) )
		{
			if ( NETWORK_SERVER_1P == m_gameMode )
			{
				SERVER->End ();
			}
			else
			{
				CLIENT->End ();
			}
		}

		//動作
		m_rad += D3DX_2PI / 120;
		if ( m_rad > D3DX_2PI ) { m_rad = 0; }
		m_loading->SetRadian ( m_rad );
	}

	void NetWaiting::Connect ()
	{
	}

	void NetWaiting::Disconnect ()
	{
		if ( NETWORK_SERVER_1P == m_gameMode )
		{
			SERVER->End ();
		}
		else
		{
			CLIENT->End ();
		}
	}

	void NetWaiting::Act ()
	{
	}


}	//namespace GAME

