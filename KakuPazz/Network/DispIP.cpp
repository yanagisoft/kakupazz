﻿//=================================================================================================
//
//	ネットワークウェイティング　ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "DispIP.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	const int DispIP::TX_DOT = 10;
	const int DispIP::TX_COLON = 11;
	const int DispIP::DISP_IP_X = 150;
	const int DispIP::DISP_IP_Y = 150;
	const int DispIP::DISP_IP_P = 20;

	DispIP::DispIP ()
	{
		m_ip = "127.0.0.1";
		m_port = 12345;

		m_grpIP = make_shared < GrpAcv > ();
		m_grpIP->AddTexture ( _T("chain0.png") );
		m_grpIP->AddTexture ( _T("chain1.png") );
		m_grpIP->AddTexture ( _T("chain2.png") );
		m_grpIP->AddTexture ( _T("chain3.png") );
		m_grpIP->AddTexture ( _T("chain4.png") );
		m_grpIP->AddTexture ( _T("chain5.png") );
		m_grpIP->AddTexture ( _T("chain6.png") );
		m_grpIP->AddTexture ( _T("chain7.png") );
		m_grpIP->AddTexture ( _T("chain8.png") );
		m_grpIP->AddTexture ( _T("chain9.png") );
		m_grpIP->AddTexture ( _T("dot.png") );
		m_grpIP->AddTexture ( _T("colon.png") );
		AddTask ( m_grpIP );
	}

	DispIP::~DispIP ()
	{
	}

	void DispIP::SetNetworkSetting ( string& ip, int port )
	{
		m_ip = ip;
		m_port = port;

		//Grp初期化
		m_grpIP->ClearObject ();
		PVP_Object pvpob = m_grpIP->GetpvpObject();
		int obIndex = 0;

		//ipを分割
		string tempIp = m_ip;
		int sizeIp = tempIp.size ();
		int n = 0;
		vector < int > vIndex;

		while ( true )
		{
			string str = tempIp.substr ( 0, 1 );
			
			if ( '\0' == str[0] ) { break; }
			else if ( '.' == str[0] )
			{
				vIndex.push_back ( TX_DOT );
			}
			else
			{
				vIndex.push_back ( stoi ( str ) );
			}

			tempIp.erase ( 0, 1 );
		}

		//Grpに反映
		float x = 0.f;
		m_grpIP->AddObject ( sizeIp );
		for ( int i = 0; i < sizeIp; ++i )
		{
			(*pvpob)[i]->SetIndexTexture ( vIndex [ i ] );
			x = 0.f + DISP_IP_X + DISP_IP_P * i;
			(*pvpob)[i]->SetPos ( x, DISP_IP_Y );
		}
		obIndex = sizeIp;

		//-----------------------------------------------------
		//区切り(":")
		m_grpIP->AddObject ();
		(*pvpob)[obIndex]->SetIndexTexture ( TX_COLON );
		x += DISP_IP_P;
		(*pvpob)[obIndex]->SetPos ( x, DISP_IP_Y );
		++obIndex;
		
		//-----------------------------------------------------
		//portの各桁を保存
		int tempPort = m_port;
		vector < int > num;
		while ( tempPort >= 10 )
		{
			num.push_back ( tempPort % 10 );
			tempPort /= 10;
		}
		num.push_back ( tempPort % 10 );

		//Grpに反映
		int size = num.size();
		m_grpIP->AddObject ( size );
		for ( int i = 0; i < size; ++i )
		{
			(*pvpob)[obIndex + i]->SetIndexTexture ( num [ i ] );
			(*pvpob)[obIndex + i]->SetPos ( x + DISP_IP_P * ( 1 + size - i ), DISP_IP_Y );
		}
		//-----------------------------------------------------
		for ( auto p : (*pvpob) )
		{
			p->SetScaling ( VEC2 ( 0.5f, 0.5f ) );
		}
	}

	void DispIP::Move ()
	{
		GameTaskVector::Move ();
	}


}	//namespace GAME

