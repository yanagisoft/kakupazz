﻿//=================================================================================================
//
//	ネットセッティングファイル　ソース
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "NetSettingFile.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	NetSettingFile::NetSettingFile ()
	{
	}

	NetSettingFile::~NetSettingFile ()
	{
	}

	Param NetSettingFile::FileToParam ()
	{
		Param param;
		
		//設定ファイルの読込
		ifstream ifStrm;
		ios_base::iostate exceptionMask = ifStrm.exceptions () | ios::failbit;
		ifStrm.exceptions ( exceptionMask );
		string str;
		int port;

		try
		{
			ifStrm.open ( _T("network.txt") );
			if ( ! ifStrm ) { throw; }
		
			::getline ( ifStrm, str );

			ifStrm >> port;
			ifStrm.close ();
		}
		catch ( ios_base::failure & fail )
		{
			TRACE_CHF ( fail.what () );

			//エラーのときに新規作成
			ofstream ofStrm ( _T("network.txt"), ios::trunc );
			ofStrm << DEFAULT_IP << '\n' << DEFAULT_PORT;
			ofStrm.close ();

			str.assign ( DEFAULT_IP );
			port = DEFAULT_PORT;
		}
		param.SetIp ( str );
		param.SetNetPort ( port );

		return param;
	}


}	//namespace GAME

