//=================================================================================================
//
// パラメータ
//		シーン間をまたぐ値
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Param.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	Param::Param ()
		: m_gameMode ( SINGLE ), m_playerNum ( PLAYER1 )
		, m_charaName1p ( CN_VERNA ), m_charaName2p ( CN_ESTA )
		, m_demo ( false )
		, m_ip ( "127.0.0.1" ), m_port ( 0 )
	{
	}

	Param::Param ( const Param & rhs )
	{
		m_gameMode	= rhs.m_gameMode;
		m_playerNum	= rhs.m_playerNum;
		m_charaName1p = rhs.m_charaName1p;
		m_charaName2p = rhs.m_charaName2p;
		m_demo = rhs.m_demo;
		m_ip		= rhs.m_ip;
		m_port		= rhs.m_port;
	}

	Param::~Param ()
	{
	}

	void Param::SetRandomChara ()
	{
		m_charaName1p = (CHARA_NAME)(rand () % CHARA_NAME_NUM );
		m_charaName2p = (CHARA_NAME)(rand () % CHARA_NAME_NUM );
	}

	void Param::SetRandomChara2p ()
	{
		m_charaName2p = (CHARA_NAME)(rand () % CHARA_NAME_NUM );
	}


}	//namespace GAME

