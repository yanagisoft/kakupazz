//=================================================================================================
//
// シーン
//		解放と確保を伴う状態遷移
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "Scene.h"
#include "../Network/NetSettingFile.h"

//状態遷移先
#include "../Title/Title.h"
#include "../CharaSele/CharaSele.h"
#include "../ActMain/ActMain.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	SceneManager::SceneManager ()
	{
		//パラメータの設定
		P_Param m_pParam = make_shared < Param > ();

		//ファイルからの読込(エラーのときは新規作成)
		NetSettingFile nsf;
		(*m_pParam) = nsf.FileToParam ();

		//------------------------------------------------------
		//最初のシーン
//@temp
		m_pScene = make_shared < Title > ();
//		m_pScene = make_shared < CharaSele > ();
//		m_pScene = make_shared < ActMain > ();

		m_pScene->SetpParam ( m_pParam );
		m_pScene->ParamInit ();
		Load ();
		Init ();

		//Sceneの設定
		SetScene( m_pScene );
	}

	SceneManager::~SceneManager()
	{
	}


}	//namespace GAME

