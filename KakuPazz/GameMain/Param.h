//=================================================================================================
//
// パラメータヘッダ
//		ゲーム内でシーン間をまたぐ値
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "SceneCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class Param	: public GameParam
	{
		GAME_MODE		m_gameMode;		//ゲームモード
		PLAYER_NUM		m_playerNum;	//プレイヤ番号
		CHARA_NAME		m_charaName1p;	//１Pキャラ名
		CHARA_NAME		m_charaName2p;	//２Pキャラ名
		bool			m_demo;			//デモモードかどうか
		string			m_ip;			//ネットワーク：接続先IPアドレス
		int				m_port;			//ネットワーク：接続先ポート

	public:
		Param ();
		Param ( const Param & rhs );	//コピー可能
		~Param ();

		void SetMode ( GAME_MODE mode ) { m_gameMode = mode; }
		GAME_MODE GetMode () const { return m_gameMode; }

		void SetPlayerNum ( PLAYER_NUM playerNum ) { m_playerNum = playerNum; }
		PLAYER_NUM GetPlayerNum () const { return m_playerNum; }

		void SetRandomChara ();
		void SetRandomChara2p ();

		void SetCharaName1p ( CHARA_NAME name ) { m_charaName1p = name; }
		CHARA_NAME GetCharaName1p () const { return m_charaName1p; }

		void SetCharaName2p ( CHARA_NAME name ) { m_charaName2p = name; }
		CHARA_NAME GetCharaName2p () const { return m_charaName2p; }

		void SetDemo ( bool b ) { m_demo = b; }
		bool GetDemo () const { return m_demo; }

		void SetIp ( string & str ) { m_ip.assign ( str.c_str () ); }
		string & GetIp () { return m_ip; }

		void SetNetPort ( int port ) { m_port = port; }
		int GetNetPort () const { return m_port; }
	};

	using P_Param = shared_ptr < Param >;


}	//namespace GAME

