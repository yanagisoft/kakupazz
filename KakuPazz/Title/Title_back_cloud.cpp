//=================================================================================================
//
// タイトル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "Title_back_cloud.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	enum TITLE_POS
	{
		TP_CLOUD_N = 10,
		TP_CLOUD_X = 0,
		TP_CLOUD_Y = 0,
	};

	Title_back_cloud::Title_back_cloud ()
	{
		//テクスチャファイル読込
		GrpAcv::AddTexture ( _T ( "cloud.png" ) );

		//速度
		m_vVel.resize ( TP_CLOUD_N );
		for ( int i = 0; i < TP_CLOUD_N; ++i )
		{
			m_vVel [ i ] = -1.f - 0.1f *( rand () % 100 );
		}
	
		//グラフィックオブジェクト
		GrpAcv::ResetObjectNum ( TP_CLOUD_N );
		PVP_Object pvpOb = GetpvpObject ();

		int i = 0;
		for ( auto p : (*pvpOb) )
		{
			p->SetPos ( VEC2 ( i * 70.f, -240 + 1.f * ( rand() % 960 ) ) );
			++i;
		}
	}

	Title_back_cloud::~Title_back_cloud()
	{
	}

	void Title_back_cloud::Move()
	{
		PVP_Object pvpOb = GetpvpObject ();
		int i = 0;
		for ( auto p : (*pvpOb) )
		{
			p->AddPosY ( m_vVel [ i ] );
			if ( 0 - 64 > p->GetPos ().y )
			{
				p->SetPosY ( 480 + 64 );
			}
			++i;
		}
		GrpAcv::Move();
	}



}	//namespace GAME

