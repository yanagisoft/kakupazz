//=================================================================================================
//
// タイトル
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/Scene.h"
#include "../GameMain/SceneCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class Title_back_cloud	: public GrpAcv
	{
		vector < float >		m_vVel;

	public:
		Title_back_cloud();
		Title_back_cloud ( const Title_back_cloud& rhs ) = delete;
		~Title_back_cloud();

		void Move();
	};


}	//namespace GAME

