.//=================================================================================================
//
// タイトル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "Title.h"
#include "../GameMain/Sound.h"

//遷移先Sceneのヘッダ
#include "../CharaSele/CharaSele.h"
#include "../ActMain/ActMain.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	enum TITLE_POS
	{
		TP_CAPTION_X = 0,
		TP_CAPTION_Y = 0,

		TP_CIRCLE_X = 640 - 256 + 20,
		TP_CIRCLE_Y = 0,

		TP_LOGO_BASE_X = -40,
		TP_LOGO_BASE_Y = 15,

		TP_LOGO_BACK_X = 50,
		TP_LOGO_BACK_Y = 20,

		TP_CHARA_BASE_X = 280,
		TP_CHARA_BASE_Y = 60,

		TP_MODE_BASE_X = 70,
		TP_MODE_BASE_Y = 280,
		TP_MODE_BASE_P = 50,

		TP_CURSOR_BASE_X = TP_MODE_BASE_X - 50,
		TP_CURSOR_BASE_Y = TP_MODE_BASE_Y - 0,
		TP_CURSOR_BASE_P = 45,

		DEMO_TITLE_WAIT = 180,
	};

	Title::Title() : m_decide ( false ), m_bFade ( false )
	{
		//タイトル背景
		m_title_back = make_shared < GrpAcv > ();
		m_title_back->AddTexture ( _T ( "title_back.png" ) );
		AddTask ( m_title_back );

		//タイトル背景：雲
		m_cloud = make_shared < Title_back_cloud > ();
		AddTask ( m_cloud );

		//キャプション("対戦格闘パズル")
		m_title_caption = make_shared < GrpAcv > ();
		m_title_caption->AddTexture ( _T ( "caption.png" ) );
		m_title_caption->SetPos ( VEC2 ( TP_CAPTION_X, TP_CAPTION_Y ) );
		AddTask ( m_title_caption );

		//サークル("2018 やなぎソフト/あきはらソフト")
		m_title_circle = make_shared < GrpAcv > ();
		m_title_circle->AddTexture ( _T ( "circle.png" ) );
		m_title_circle->SetPos ( VEC2 ( TP_CIRCLE_X, TP_CIRCLE_Y ) );
		AddTask ( m_title_circle );

		//タイトルロゴ背景
		m_title_logo_back = make_shared < GrpAcv > ();
		m_title_logo_back->AddTexture ( _T ( "title_logo_back.png" ) );
		m_title_logo_back->SetPos ( VEC2 ( TP_LOGO_BACK_X, TP_LOGO_BACK_Y ) );
		AddTask ( m_title_logo_back );

		//タイトルキャラ
		m_title_chara = make_shared < GrpAcv > ();
		m_title_chara->AddTexture ( _T ( "title_ver.png" ) );
		m_title_chara->SetPos ( VEC2 ( TP_CHARA_BASE_X, TP_CHARA_BASE_Y ) );
		AddTask ( m_title_chara );

		m_posyChara = 0.f;
		m_velyChara = 0.f;
		m_accyChara = 0.04f;

		//タイトルロゴ
		m_title_logo = make_shared < GrpAcv > ();
		m_title_logo->AddTexture ( _T ( "title_logo.png" ) );
		m_title_logo->SetPos ( VEC2 ( TP_LOGO_BASE_X, TP_LOGO_BASE_Y ) );
		AddTask ( m_title_logo );

		m_posyLogo = 0.f;
		m_velyLogo = 0.1f;

		//モード表示
		m_grpMode = make_shared < GrpAcv > ();
		m_grpMode->AddTexture ( _T ( "mode.png" ) );
		m_grpMode->SetPos ( VEC2 ( TP_MODE_BASE_X, TP_MODE_BASE_Y ) );
		AddTask ( m_grpMode );
	
		//カーソル
		m_pCursor = make_shared < GrpAcv > ();
		m_pCursor->AddTexture ( _T ("titleCursor.png") );
		m_pCursor->SetPos ( VEC2( TP_CURSOR_BASE_X, TP_CURSOR_BASE_Y ) );
		m_pCursor->SetScalingCenter ( VEC2 ( 16, 16 ) );
		m_cursorScale = 1.f;
		m_cursorDirection = 1.f;
		AddTask ( m_pCursor );

		//フェードアウト
		m_fade = make_shared < Fade > ();
		m_fade->SetZ ( 0.1f );
		m_fade->SetDarkIn ( 16 );
		AddTask ( m_fade );

		//デモ
		m_demoTimer = 0;
		m_grpDemo = make_shared < GrpAcv > ();
		m_grpDemo->AddTexture ( _T ( "demo_mode.png" ) );
		m_grpDemo->SetPos ( VEC2 ( TP_MODE_BASE_X, TP_MODE_BASE_Y + 100) );
		m_grpDemo->SetValid ( false );
		AddTask ( m_grpDemo );

		//モード
		m_mode = SINGLE;
	}

	Title::~Title()
	{
	}

	void Title::ParamInit ()
	{
		if ( GetpParam()->GetDemo () )	//デモON
		{
			//デモモード表示:true
			m_grpDemo->SetValid ( true );
			m_demoTimer = 0;
			//通常メニュー:false
			m_pCursor->SetValid ( false );
			m_grpMode->SetValid ( false );
		}
		else	//デモOFF
		{
			//デモモード表示:false
			m_grpDemo->SetValid ( false );
			//通常メニュー:true
			m_pCursor->SetValid ( true );
			m_grpMode->SetValid ( true );
		}
	}

	void Title::Load ()
	{
		//BGM
		SoundArchiver::instance()->PlayLoop ( BGM_TITLE );

		Scene::Load ();
	}

	void Title::Move()
	{
		//デモ
		if ( GetpParam()->GetDemo () )
		{
			if ( DEMO_TITLE_WAIT == ++ m_demoTimer )
			{
				m_demoTimer = 0;
				SOUND->Play ( SE_DECIDE );
				GetpParam ()->SetMode ( m_mode );
				m_fade->SetDarkOut ( 16 );
				m_decide = true;
			}
		}

		//F1でデモモードを切り替え
		if ( ::GetAsyncKeyState ( VK_F1 ) & 0x0001 ) 
		{ 
			if ( GetpParam()->GetDemo () )	//デモOFF
			{
				//デモモード表示:false
				m_grpDemo->SetValid ( false );
				GetpParam()->SetDemo ( false );
				//通常メニュー:true
				m_pCursor->SetValid ( true );
				m_grpMode->SetValid ( true );
			}
			else	//デモON
			{
				//デモモード表示:true
				m_grpDemo->SetValid ( true );
				GetpParam()->SetDemo ( true );
				m_demoTimer = 0;
				//通常メニュー:false
				m_pCursor->SetValid ( false );
				m_grpMode->SetValid ( false );
			}
		}

		//決定済のときは入力動作無でフェード終了待機
		if ( m_decide )
		{
			m_bFade = ! m_fade->IsActive ();
		}
		else
		{
			//決定
			if ( PUSH_KEY ( P1_BUTTON1 ) || PUSH_KEY ( P2_BUTTON1 ) )
			{
				SOUND->Play ( SE_DECIDE );
				m_decide = true;

				m_fade->SetDarkOut ( 16 );
				m_bFade = false;
			}

			//上下で選択
#if 0
			switch ( m_mode )
			{
			case SINGLE:
				if ( _PushUp ()		) { m_mode = NETWORK_CLIENT_2P; }
				if ( _PushDown ()	) { m_mode = DOUBLE; }
			break;
			case DOUBLE:
				if ( _PushUp ()		) { m_mode = SINGLE; }
				if ( _PushDown ()	) { m_mode = NETWORK_SERVER_1P; }
			break;
			case NETWORK_SERVER_1P:
				if ( _PushUp ()		) { m_mode = DOUBLE; }
				if ( _PushDown ()	) { m_mode = NETWORK_CLIENT_2P; }
			break;
			case NETWORK_CLIENT_2P:
				if ( _PushUp ()		) { m_mode = NETWORK_SERVER_1P; }
				if ( _PushDown ()	) { m_mode = SINGLE; }
			break;
			default: break;
			}
#endif // 0
			switch ( m_mode )
			{
			case SINGLE:
				if ( _PushUp ()		) { m_mode = DOUBLE_CPU_CPU; }
				if ( _PushDown ()	) { m_mode = DOUBLE_1P_CPU; }
			break;
			case DOUBLE_1P_CPU:
				if ( _PushUp ()		) { m_mode = SINGLE; }
				if ( _PushDown ()	) { m_mode = DOUBLE_1P_2P; }
			break;
			case DOUBLE_1P_2P:
				if ( _PushUp ()		) { m_mode = DOUBLE_1P_CPU; }
				if ( _PushDown ()	) { m_mode = DOUBLE_CPU_CPU; }
			break;
			case DOUBLE_CPU_CPU:
				if ( _PushUp ()		) { m_mode = DOUBLE_1P_2P; }
				if ( _PushDown ()	) { m_mode = SINGLE; }
			break;
			default: break;
			}

			//カーソル表示位置設定
			float x = TP_CURSOR_BASE_X;
			float y = TP_CURSOR_BASE_Y;
			float p = TP_CURSOR_BASE_P;
			float cy = 0;
			switch ( m_mode )
			{
			case SINGLE:			cy = y + p * 0; break;
			case DOUBLE_1P_CPU:		cy = y + p * 1; break;
			case DOUBLE_1P_2P:		cy = y + p * 2; break;
			case DOUBLE_CPU_CPU:	cy = y + p * 3; break;
//			case NETWORK_SERVER_1P:	cy = y + p * 2; break;
//			case NETWORK_CLIENT_2P:	cy = y + p * 3; break;
			default: break;
			}
			m_pCursor->SetPos ( VEC2 ( x, cy ) );
		}

		//アニメーション
		//キャラ
		m_velyChara += m_accyChara;
		if (  1.f < m_velyChara ) { m_accyChara *= -1.f; }
		if ( -1.f > m_velyChara ) { m_accyChara *= -1.f; }
		m_posyChara += m_velyChara;
		m_title_chara->SetPos ( TP_CHARA_BASE_X, TP_CHARA_BASE_Y + m_posyChara );

		//ロゴ
		m_posyLogo += m_velyLogo;
		if (  10.f < m_posyLogo ) { m_velyLogo *= -1.f; }
		if ( -10.f > m_posyLogo ) { m_velyLogo *= -1.f; }
		m_title_logo->SetPos ( VEC2 ( TP_LOGO_BASE_X, TP_LOGO_BASE_Y + m_posyLogo  ) );

		//カーソル
		m_cursorDirection += 0.1f;
		if ( m_cursorDirection >= D3DX_PI * 2 ) { m_cursorDirection = 0; }
		m_cursorScale = cos ( m_cursorDirection );
		m_pCursor->SetScaling ( 1.f, m_cursorScale );

		Scene::Move();
	}

	P_GameScene Title::Transit()
	{
		//ESCで終了
		if ( ::GetAsyncKeyState ( VK_ESCAPE ) & 0x0001 ) 
		{ 
			::PostQuitMessage( 0 );
		}

		//モード決定時に、次のオブジェクトを生成して返す
		if ( m_decide && m_bFade )
		{
			SOUND->Stop ( BGM_TITLE );

			//パラメータに保存
			GetpParam ()->SetMode ( m_mode );
			
			//デモ時
			if (  GetpParam()->GetDemo () )
			{
				GetpParam ()->SetPlayerNum ( PLAYER2 );
				GetpParam ()->SetRandomChara ();
				GetpParam ()->SetMode ( DOUBLE_CPU_CPU );
				return make_shared < ActMain > ();
			}

			//CPU観戦時はキャラランダム
			if ( DOUBLE_CPU_CPU == GetpParam ()->GetMode () )
			{
				GetpParam ()->SetRandomChara ();
				return make_shared < ActMain > ();	//ローカルとネットワークは共通
			}

			//通常時
//			return make_shared < ActMain > ();	//ローカルとネットワークは共通
			return make_shared < CharaSele > ();
		}

		//非決定時はthisを返して続行
		return shared_from_this ();
	}

	bool Title::_PushUp ()
	{
		if ( PUSH_KEY ( P1_UP ) || PUSH_KEY ( P2_UP ) )
		{
			SOUND->Play ( SE_SELECT );
			return true;
		}
		return false;
	}

	bool Title::_PushDown ()
	{
		if ( PUSH_KEY ( P1_DOWN ) || PUSH_KEY ( P2_DOWN ) )
		{
			SOUND->Play ( SE_SELECT );
			return true;
		}
		return false;
	}


}	//namespace GAME

