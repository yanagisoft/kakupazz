//=================================================================================================
//
// タイトル
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/Scene.h"
#include "../GameMain/SceneCommon.h"
#include "Title_back_cloud.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class Title	: public Scene
	{
		P_GrpAcv		m_title_back;		//タイトル背景
		shared_ptr < Title_back_cloud >		m_cloud;		//タイトル背景：雲
		P_GrpAcv		m_title_caption;	//タイトルキャプション("対戦格闘パズル")
		P_GrpAcv		m_title_circle;		//タイトルサークル("2018 やなぎソフト/あきはらソフト")
		P_GrpAcv		m_title_chara;		//タイトルキャラ
		P_GrpAcv		m_title_logo;		//タイトルロゴ
		P_GrpAcv		m_title_logo_back;	//タイトルロゴ背景
		P_GrpAcv		m_grpMode;		//モード表示

		//キャラ
		float			m_posyChara;
		float			m_velyChara;
		float			m_accyChara;

		//ロゴ
		float			m_posyLogo;
		float			m_velyLogo;

		//カーソル
		P_GrpAcv		m_pCursor;
		float			m_cursorScale;
		float			m_cursorDirection;

		GAME_MODE		m_mode;		//ゲームモード
		bool			m_decide;	//決定

		shared_ptr < Fade >			m_fade;
		bool			m_bFade;	//フェードアウト待ち

		bool	_PushUp ();
		bool	_PushDown ();

		//デモモード
		P_GrpAcv	m_grpDemo;		//モード表示
		UINT		m_demoTimer;

	public:
		Title();
		Title ( const Title& rhs ) = delete;
		~Title();

		void ParamInit ();
		void Load ();
		void Move();

		//GameSceneManagerで扱うため、P_SceneではなくP_GameScene
		P_GameScene		Transit();
	};


}	//namespace GAME

