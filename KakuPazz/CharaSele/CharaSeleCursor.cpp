//=================================================================================================
//
// キャラセレ カーソル
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "CharaSeleCursor.h"
#include "../GameMain/Sound.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//-------------------------------------------------------------
	CharaSeleCursor::CharaSeleCursor ( PLAYER_NUM playerNum )
		: m_playerNum ( playerNum ), m_charaName ( CN_VERNA )
		, m_decide ( false )
		, m_cursorTimer ( 0 ), m_pulse ( false )
	{
		if ( PLAYER1 == playerNum )
		{
			AddTexture ( _T ("charasele_cursor_1p.png") );
			SetPos ( VEC2( CHSLC_CSR_X_1P, CHSLC_CSR_Y_1P ) );
			m_charaName = CN_VERNA;
		}
		else
		{
			AddTexture ( _T ("charasele_cursor_2p.png") );
			SetPos ( VEC2( CHSLC_CSR_X_2P, CHSLC_CSR_Y_2P) );
			m_charaName = CN_HIEMS;
		}

		SetScalingCenter ( VEC2 ( 32, 32 ) );
		m_cursorScale = VEC2 { 1.f, 1.f };
	}

	CharaSeleCursor::~CharaSeleCursor()
	{
	}

	void CharaSeleCursor::Input ()
	{
		//移動
		if ( ! m_decide )
		{
			if ( PushLeft () ) { RotateL (); }
			if ( PushRight () ) { RotateR (); }
		}

		//決定
		if ( PushButton1 () ) 
		{
			m_decide = ! m_decide;
		}
	}

	void CharaSeleCursor::Move()
	{
		//位置
		SetPos ( _Position () );

		//振動
		if ( ! m_decide )
		{
			if ( ++ m_cursorTimer > CHSLC_CSR_TIME )
			{
				m_cursorTimer = 0;
				m_pulse = ! m_pulse;
				if ( m_pulse ) { SetScaling ( VEC2( 1.125f, 1.125f ) ); }
				else { SetScaling ( VEC2( 1, 1 ) ); }
			}
		}

		//毎フレーム動作
		GrpAcv::Move();
	}

	bool CharaSeleCursor::PushLeft ()
	{
		bool b = PUSH_KEY ( PL_IP_KEY[m_playerNum][LEFT] );
		if ( b ) { SOUND->Play ( SE_SELECT ); }
		return ( b );
	}

	bool CharaSeleCursor::PushRight ()
	{
		bool b = PUSH_KEY ( PL_IP_KEY[m_playerNum][RIGHT] );
		if ( b ) { SOUND->Play ( SE_SELECT ); }
		return ( b );
	}

	bool CharaSeleCursor::PushButton1 ()
	{
		bool b = PUSH_KEY ( PL_IP_KEY[m_playerNum][BUTTON1] );
		if ( b ) { SOUND->Play ( SE_DECIDE ); }
		return ( b );
	}

	void CharaSeleCursor::RotateL ()
	{
		switch ( m_charaName )
		{
		case CN_VERNA: m_charaName = CN_HIEMS; break;
		case CN_ESTA : m_charaName = CN_VERNA; break;
		case CN_AUTUM: m_charaName = CN_ESTA; break;
		case CN_HIEMS: m_charaName = CN_AUTUM; break;
		default: m_charaName = CN_VERNA; break;
		}
	}

	void CharaSeleCursor::RotateR ()
	{
		switch ( m_charaName )
		{
		case CN_VERNA: m_charaName = CN_ESTA; break;
		case CN_ESTA : m_charaName = CN_AUTUM; break;
		case CN_AUTUM: m_charaName = CN_HIEMS; break;
		case CN_HIEMS: m_charaName = CN_VERNA; break;
		default: m_charaName = CN_VERNA; break;
		}
	}

	VEC2 CharaSeleCursor::_Position ()
	{
		VEC2 ret;
		float x = CHSLC_CSR_BASE_X;
		float y = CHSLC_CSR_BASE_Y;
		float p = CHSLC_CHF_PX;
		switch ( m_charaName )
		{
		case CN_VERNA: ret = VEC2 ( x + p * 0, y ); break;
		case CN_ESTA : ret = VEC2 ( x + p * 1, y ); break;
		case CN_AUTUM: ret = VEC2 ( x + p * 2, y ); break;
		case CN_HIEMS: ret = VEC2 ( x + p * 3, y ); break;
		default		 : ret = VEC2 ( 0, 0 ); break;
		}
		return ret;
	}



}	//namespace GAME

