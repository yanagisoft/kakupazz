//=================================================================================================
//
// キャラセレ
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "stdAfx.h"
#include "CharaSele.h"
#include "../GameMain/Sound.h"

//遷移先Sceneのヘッダ
#include "../ActMain/ActMain.h"
#include "../Title/Title.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	CharaSele::CharaSele()
		:m_bDecide ( false ), m_bFinish ( false )
	{
		//移動背景
		m_titleBack = make_shared < TitleBack > ();
		AddTask ( m_titleBack );

		m_characterSelect = make_shared < GrpAcv > ();
		m_characterSelect->AddTexture ( _T("CharacterSelect.png") );
		m_characterSelect->SetPos ( VEC2 ( 64, 30 ) );
		AddTask ( m_characterSelect );
	
		//キャラ
		float x = CHSLC_CH0_X;
		float y = CHSLC_CH0_Y;
		float p = CHSLC_CHF_PX;

		m_chara0 = make_shared < Chara > ();
		m_chara0->SetName ( CN_VERNA );
		m_chara0->ParamInit ();
		m_chara0->SetPos ( VEC2 ( x + 0 * p, y ) );
		AddTask ( m_chara0 );

		m_chara1 = make_shared < Chara > ();
		m_chara1->SetName ( CN_ESTA );
		m_chara1->ParamInit ();
		m_chara1->SetPos ( VEC2 ( x + 1 * p, y ) );
		AddTask ( m_chara1 );

		m_chara2 = make_shared < Chara > ();
		m_chara2->SetName ( CN_AUTUM );
		m_chara2->ParamInit ();
		m_chara2->SetPos ( VEC2 ( x + 2 * p, y ) );
		AddTask ( m_chara2 );

		m_chara3 = make_shared < Chara > ();
		m_chara3->SetName ( CN_HIEMS );
		m_chara3->ParamInit ();
		m_chara3->SetPos ( VEC2 ( x + 3 * p, y ) );
		AddTask ( m_chara3 );


		//カーソル
		m_cursor1p = make_shared < CharaSeleCursor > ( PLAYER1 );
		AddTask ( m_cursor1p );

		m_cursor2p = make_shared < CharaSeleCursor > ( PLAYER2 );
		AddTask ( m_cursor2p );

		//フェードアウト
		m_fade = make_shared < Fade > ();
		m_fade->SetZ ( 0.1f );
		m_fade->SetDarkIn ( 16 );
		AddTask ( m_fade );
	}

	CharaSele::~CharaSele()
	{
	}

	void CharaSele::ParamInit ()
	{
		P_Param p = GetpParam ();

		//モードで分岐
		switch ( p->GetMode () )
		{
		case SINGLE: _Init1p (); break;
		case DOUBLE_1P_CPU: _Init1p ();
			p->SetRandomChara2p ();
			break;
		case DOUBLE_1P_2P: _InitDouble (); break;
		case DOUBLE_CPU_CPU: _InitDouble (); break;
		case NETWORK_SERVER_1P: _Init1p (); break;
		case NETWORK_SERVER_2P: _Init2p (); break;
		case NETWORK_CLIENT_1P: _Init1p (); break;
		case NETWORK_CLIENT_2P: _Init2p (); break;
		default: break;
		}
	}

	void CharaSele::_Init1p ()
	{
		m_cursor2p->SetValid ( false );
	}

	void CharaSele::_Init2p ()
	{
		m_cursor1p->SetValid ( false );
	}

	void CharaSele::_InitDouble ()
	{
	}

	void CharaSele::Load ()
	{
		//BGM
		SoundArchiver::instance()->PlayLoop ( BGM_SETTINGS );
		Scene::Load ();
	}

	void CharaSele::Move ()
	{
		//フェード時は入力をしないで表示のみ
		if ( m_bDecide )
		{
			//フェードが終了したとき、待機も完了してキャラセレを終了する
			m_bFinish = ! m_fade->IsActive ();
		}

		switch ( GetpParam()->GetMode () )
		{
		case SINGLE: _Move1p (); break;
		case DOUBLE_1P_CPU: _Move1p (); break;
		case DOUBLE_1P_2P: _MoveDouble (); break;
		case DOUBLE_CPU_CPU: _MoveDouble (); break;
		case NETWORK_SERVER_1P: _Move1p (); break;
		case NETWORK_SERVER_2P: _Move2p (); break;
		case NETWORK_CLIENT_1P: _Move1p (); break;
		case NETWORK_CLIENT_2P: _Move2p (); break;
		default: break;
		}

		//毎フレーム動作
		Scene::Move();
	}

	void CharaSele::_Move1p ()
	{
		//未決定時
		if ( ! m_bDecide )
		{
			//入力
			m_cursor1p->Input ();
			GetpParam ()->SetRandomChara2p ();
			m_cursor2p->SetCharaName ( GetpParam ()->GetCharaName2p () );
			m_cursor2p->SetDecide ( true );
			_Decide ();	//決定チェック
		}
	}

	void CharaSele::_Move2p ()
	{
		//未決定時
		if ( ! m_bDecide )
		{
			//入力
			m_cursor2p->Input ();
			_Decide ();	//決定チェック
		}
	}

	void CharaSele::_MoveDouble ()
	{
		//未決定時
		if ( ! m_bDecide )
		{
			//入力
			m_cursor1p->Input ();
			m_cursor2p->Input ();
			_Decide ();	//決定チェック
		}
	}

	void CharaSele::_Decide ()
	{
		//決定
		m_bDecide = m_cursor1p->IsDecide () && m_cursor2p->IsDecide ();
		
		if ( m_bDecide )
		{
			//パラメータに保存
			GetpParam ()->SetCharaName1p ( m_cursor1p->GetCharaName () );
			GetpParam ()->SetCharaName2p ( m_cursor2p->GetCharaName () );
			
			//フェードアウト開始
			m_fade->SetDarkOut ( 16 );
		}
	}

	//====================================================
	//状態遷移
	P_GameScene  CharaSele::Transit()
	{
		//ESCでタイトルに移行
		if ( ::GetAsyncKeyState ( VK_ESCAPE ) & 0x0001 ) 
		{ 
			SOUND->Stop ( BGM_SETTINGS );
			return make_shared < Title > ();
		}

		//モード決定時に、次のオブジェクトを生成して返す
		if ( m_bFinish )
		{
			SOUND->Stop ( BGM_SETTINGS );
			return make_shared < ActMain > ();	//ローカルとネットワークは共通
		}

		//非決定時はthisを返して続行
		return shared_from_this ();
	}



}	//namespace GAME

