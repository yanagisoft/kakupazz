//=================================================================================================
//
// キャラクタセレクト カーソル
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/SceneCommon.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	enum CHARASELE_POS
	{
		CHSLC_X = 0,
		CHSLC_Y = 0,
		CHSLC_CH0_X = 80,
		CHSLC_CH0_Y = 200,
		CHSLC_CHF_PX = 120,	//ピッチ
	};

	enum CHARASELE_CURSOR
	{
		CHSLC_CSR_BASE_X = CHSLC_CH0_X + 30,
		CHSLC_CSR_BASE_Y = CHSLC_CH0_Y + 20,
		CHSLC_CSR_X_1P = CHSLC_CSR_BASE_X,
		CHSLC_CSR_Y_1P = CHSLC_CSR_BASE_Y,
		CHSLC_CSR_X_2P = CHSLC_CSR_BASE_X + CHSLC_CHF_PX * 4,
		CHSLC_CSR_Y_2P = CHSLC_CSR_BASE_Y,
		CHSLC_CSR_TIME = 16,		//振動時間
	};


	class CharaSeleCursor	: public GrpAcv
	{
		PLAYER_NUM		m_playerNum;
		CHARA_NAME		m_charaName;
		bool			m_decide;	//決定

		UINT			m_cursorTimer;
		VEC2			m_cursorScale;
		bool			m_pulse;		//鼓動フラグ

	public:
		CharaSeleCursor () = delete;
		CharaSeleCursor ( PLAYER_NUM playerNum );
		CharaSeleCursor ( const CharaSeleCursor & rhs ) = delete;
		~CharaSeleCursor ();

		void Input ();
		void Move();

		void SetCharaName ( CHARA_NAME name ) { m_charaName = name; }
		CHARA_NAME GetCharaName () const { return m_charaName; }

		void SetDecide ( bool b ) { m_decide = b; }
		bool IsDecide () { return m_decide; }

		bool PushLeft ();
		bool PushRight ();
		bool PushButton1 ();

		void RotateL ();
		void RotateR ();

	private:
		VEC2 _Position ();
	};

	using P_ChSlctCsr = shared_ptr < CharaSeleCursor >;


}	//namespace GAME

