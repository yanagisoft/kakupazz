//=================================================================================================
//
// キャラクタセレクト
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Game.h"
#include "../GameMain/Scene.h"
#include "../GameMain/SceneCommon.h"
#include "CharaSeleCursor.h"
#include "../Network/TitleBack.h"
#include "../ActMain/Chara.h"

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class CharaSele	: public Scene
	{
		//カーソル
		P_ChSlctCsr		m_cursor1p;
		P_ChSlctCsr		m_cursor2p;

		//背景
		P_TitleBack		m_titleBack;
		P_GrpAcv		m_characterSelect;

		//キャラ
		P_Chara			m_chara0;
		P_Chara			m_chara1;
		P_Chara			m_chara2;
		P_Chara			m_chara3;

		//フェード
		P_Fade			m_fade;

		bool			m_bDecide;	//決定
		bool			m_bFinish;	//待機完了

	public:
		CharaSele();
		CharaSele ( const CharaSele & rhs ) = delete;
		~CharaSele();

		void ParamInit ();
		void Load ();
		void Move();

		//状態遷移
		//GameSceneManagerで扱うため、P_SceneではなくP_GameScene
		P_GameScene		Transit();

	private:
		void _Init1p ();
		void _Init2p ();
		void _InitDouble ();

		void _Move1p ();
		void _Move2p ();
		void _MoveDouble ();
		void _Decide ();	//決定チェック
	};


}	//namespace GAME

