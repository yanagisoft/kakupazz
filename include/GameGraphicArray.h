//=================================================================================================
//
// GameGraphicArray ヘッダファイル
//
//	Z値を用いてソートするGameGpraphicの配列
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
//#include "../Const.h"
#include "GameTask.h"
#include "GameGraphic.h"


//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME
{

	class GameGraphicArray : public GameTaskVector
	{
	public:
		GameGraphicArray () = default;
		GameGraphicArray ( const GameGraphicArray& rhs ) = delete;		//コピー禁止
		~GameGraphicArray () = default;

		//Z値で降順ソートされた位置に挿入
		void InsertByZ ( shared_ptr < GameGraphicBase > pTask );
	};


	//------------------------------------------
	//	型指定
	//------------------------------------------
	typedef GameGraphicArray GrpAry;
	typedef shared_ptr < GrpAry > P_GrpAry;

}	//namespace GAME


