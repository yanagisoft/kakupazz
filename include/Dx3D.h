﻿//=================================================================================================
//
// Direct3D(グラフィックス)の管理クラス
//
//=================================================================================================
#pragma once

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Directx_common.h"		//DirectX共通
#include "DxDefine.h"			//DirectX内外共通
#include <d3d9.h>				//Direct3D
#include <d3dx9.h>				//Direct3DX
#include <memory>
using namespace std;

#include "DebugLibrary.h"		//デバッグの利用
#include "HWnd.h"				//ウィンドウハンドルの取得
#include "GameLibConst.h"		//ゲームライブラリ共通

//-------------------------------------------------------------------------------------------------
// ライブラリのリンク
//-------------------------------------------------------------------------------------------------
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

//-------------------------------------------------------------------------------------------------
// 宣言
//-------------------------------------------------------------------------------------------------
namespace GAME 
{
	//------------------------------------------
	//Direct3D の管理クラス
	//------------------------------------------
	class Dx3D
	{
	//---------------------------------------------------------------------
	//シングルトンパターン
	private:
		using P_Dx3D = unique_ptr < Dx3D >;
		static P_Dx3D		m_inst;		//シングルトンインスタンス
		Dx3D();		//private コンストラクタで通常の実体化は禁止
	public:
		~Dx3D();		//デストラクタはunique_ptrのためpublic
		static void Create() { if ( ! m_inst ) { m_inst = P_Dx3D ( new Dx3D () ); } }
		static unique_ptr < Dx3D >& instance () { return m_inst; }	//インスタンス取得
	//---------------------------------------------------------------------

	private:
		LPDIRECT3D9				m_lpD3D;				//Direct3D　オブジェクト
		LPDIRECT3DDEVICE9		m_lpD3DDevice;			//Direct3D　デバイス
		LPD3DXSPRITE			m_lpSprite;				//スプライト

		LPDIRECT3DSURFACE9		m_lpBackBuffer;			//バックバッファサーフェス
		LPDIRECT3DTEXTURE9		m_lpTexture;			//サーフェス用のテクスチャ	
		LPDIRECT3DSURFACE9		m_lpTextureSurface;		//テクスチャとするサーフェスポインタ
		LPDIRECT3DSURFACE9		m_lpSurface;			//サーフェスポインタ

//		double					m_zoom;					//拡大値(0-320)
//		D3DXVECTOR2				m_focus;				//スクリーン表示位置の原点

	public:
		void Load();	//読込
		void Rele();	//解放
		void Reset();	//再設定

		//デバイスの取得
		LPDIRECT3DDEVICE9		GetDevice() { return m_lpD3DDevice; }
		//スプライトの取得
		LPD3DXSPRITE			GetSprite() { return m_lpSprite; }

		//描画開始
		void BeginScene();
		//描画終了
		void EndScene();

		//スプライト描画開始
		void BeginSprite();
		//スプライト描画終了
		void EndSprite();

		//スプライト描画
		void DrawSprite 
		(
			LPDIRECT3DTEXTURE9 lpTexture, const D3DXMATRIX* pMatrix, const RECT* rect, 
			const D3DXVECTOR3 *pCenter, const D3DXVECTOR3 *pPosition, D3DCOLOR Color 
		);

		//頂点描画
		void DrawVertex 
		(
			LPDIRECT3DTEXTURE9 lpTexture, 
			UINT streamNumber, LPDIRECT3DVERTEXBUFFER9 lpVertexBuffer, UINT offsetBytes, UINT stride, 
			DWORD FVF, D3DPRIMITIVETYPE primitiveType, UINT startVertex, UINT primitiveCount 
		);

		//稼動状態の確認
		bool	IsActive () { return ( m_lpD3D && m_lpD3DDevice && m_lpSprite ); }

		//フォーカスの取得
//		D3DXVECTOR2 GetFocus() { return m_focus; }
//		void SetFocus(D3DXVECTOR2 vec) { m_focus = vec; }

		//ズーム
//		void ZoomIn() { m_zoom += 10.0; if ( m_zoom > 319.9 ) m_zoom = 319.9; }
//		void ZoomOut() { m_zoom -= 10.0; if ( m_zoom < 0.0 ) m_zoom = 0.0; }

		//テクスチャの作成
		void CreateTextureFromMem ( LPCVOID pSrcData, UINT SrcDataSize, LPDIRECT3DTEXTURE9* ppTexture );
	};

	//シングルトンアクセス
	#define DX3D Dx3D::instance()

}	//namespace GAME

